//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.ContratoServicios;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData implements FieldProvider {
static Logger log4j = Logger.getLogger(ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String docstatus;
  public String docstatusr;
  public String adOrgId;
  public String adOrgIdr;
  public String doctypeServiciosId;
  public String doctypeServiciosIdr;
  public String cDoctypeId;
  public String documentno;
  public String proveedorId;
  public String proveedorIdr;
  public String fechaInicio;
  public String fechaFin;
  public String emNeAreaEmpresaId;
  public String emNeAreaEmpresaIdr;
  public String emAtnorhCargoId;
  public String emAtnorhCargoIdr;
  public String isactive;
  public String emNeVacacionProp;
  public String emNeVacacionTom;
  public String emNeVacacionRes;
  public String salario;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String emNeIsJornadaParcial;
  public String emNeNumHorasParciales;
  public String emNeSissalnet;
  public String emNeRegion;
  public String pagofondoreserva;
  public String aplicaUtilidad;
  public String emNeMotivoSalida;
  public String emNeMotivoSalidar;
  public String emNeObservaciones;
  public String liquidacionEmpleado;
  public String docactionno;
  public String docactionnoBtn;
  public String reenvioAzure;
  public String tipoContrato;
  public String finPaymentmethodId;
  public String finFinancialAccountId;
  public String noContratoEmpleadoId;
  public String adClientId;
  public String isImpuestoAsumido;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("doctype_servicios_id") || fieldName.equals("doctypeServiciosId"))
      return doctypeServiciosId;
    else if (fieldName.equalsIgnoreCase("doctype_servicios_idr") || fieldName.equals("doctypeServiciosIdr"))
      return doctypeServiciosIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("proveedor_id") || fieldName.equals("proveedorId"))
      return proveedorId;
    else if (fieldName.equalsIgnoreCase("proveedor_idr") || fieldName.equals("proveedorIdr"))
      return proveedorIdr;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("em_ne_area_empresa_id") || fieldName.equals("emNeAreaEmpresaId"))
      return emNeAreaEmpresaId;
    else if (fieldName.equalsIgnoreCase("em_ne_area_empresa_idr") || fieldName.equals("emNeAreaEmpresaIdr"))
      return emNeAreaEmpresaIdr;
    else if (fieldName.equalsIgnoreCase("em_atnorh_cargo_id") || fieldName.equals("emAtnorhCargoId"))
      return emAtnorhCargoId;
    else if (fieldName.equalsIgnoreCase("em_atnorh_cargo_idr") || fieldName.equals("emAtnorhCargoIdr"))
      return emAtnorhCargoIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("em_ne_vacacion_prop") || fieldName.equals("emNeVacacionProp"))
      return emNeVacacionProp;
    else if (fieldName.equalsIgnoreCase("em_ne_vacacion_tom") || fieldName.equals("emNeVacacionTom"))
      return emNeVacacionTom;
    else if (fieldName.equalsIgnoreCase("em_ne_vacacion_res") || fieldName.equals("emNeVacacionRes"))
      return emNeVacacionRes;
    else if (fieldName.equalsIgnoreCase("salario"))
      return salario;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("em_ne_is_jornada_parcial") || fieldName.equals("emNeIsJornadaParcial"))
      return emNeIsJornadaParcial;
    else if (fieldName.equalsIgnoreCase("em_ne_num_horas_parciales") || fieldName.equals("emNeNumHorasParciales"))
      return emNeNumHorasParciales;
    else if (fieldName.equalsIgnoreCase("em_ne_sissalnet") || fieldName.equals("emNeSissalnet"))
      return emNeSissalnet;
    else if (fieldName.equalsIgnoreCase("em_ne_region") || fieldName.equals("emNeRegion"))
      return emNeRegion;
    else if (fieldName.equalsIgnoreCase("pagofondoreserva"))
      return pagofondoreserva;
    else if (fieldName.equalsIgnoreCase("aplica_utilidad") || fieldName.equals("aplicaUtilidad"))
      return aplicaUtilidad;
    else if (fieldName.equalsIgnoreCase("em_ne_motivo_salida") || fieldName.equals("emNeMotivoSalida"))
      return emNeMotivoSalida;
    else if (fieldName.equalsIgnoreCase("em_ne_motivo_salidar") || fieldName.equals("emNeMotivoSalidar"))
      return emNeMotivoSalidar;
    else if (fieldName.equalsIgnoreCase("em_ne_observaciones") || fieldName.equals("emNeObservaciones"))
      return emNeObservaciones;
    else if (fieldName.equalsIgnoreCase("liquidacion_empleado") || fieldName.equals("liquidacionEmpleado"))
      return liquidacionEmpleado;
    else if (fieldName.equalsIgnoreCase("docactionno"))
      return docactionno;
    else if (fieldName.equalsIgnoreCase("docactionno_btn") || fieldName.equals("docactionnoBtn"))
      return docactionnoBtn;
    else if (fieldName.equalsIgnoreCase("reenvio_azure") || fieldName.equals("reenvioAzure"))
      return reenvioAzure;
    else if (fieldName.equalsIgnoreCase("tipo_contrato") || fieldName.equals("tipoContrato"))
      return tipoContrato;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("no_contrato_empleado_id") || fieldName.equals("noContratoEmpleadoId"))
      return noContratoEmpleadoId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("is_impuesto_asumido") || fieldName.equals("isImpuestoAsumido"))
      return isImpuestoAsumido;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_contrato_empleado.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_contrato_empleado.CreatedBy) as CreatedByR, " +
      "        to_char(no_contrato_empleado.Updated, ?) as updated, " +
      "        to_char(no_contrato_empleado.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_contrato_empleado.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_contrato_empleado.UpdatedBy) as UpdatedByR," +
      "        no_contrato_empleado.Docstatus, " +
      "(CASE WHEN no_contrato_empleado.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "no_contrato_empleado.AD_Org_ID, " +
      "(CASE WHEN no_contrato_empleado.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_contrato_empleado.Doctype_Servicios_ID, " +
      "(CASE WHEN no_contrato_empleado.Doctype_Servicios_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS Doctype_Servicios_IDR, " +
      "no_contrato_empleado.C_Doctype_ID, " +
      "no_contrato_empleado.Documentno, " +
      "no_contrato_empleado.Proveedor_ID, " +
      "(CASE WHEN no_contrato_empleado.Proveedor_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS Proveedor_IDR, " +
      "no_contrato_empleado.Fecha_Inicio, " +
      "no_contrato_empleado.Fecha_Fin, " +
      "no_contrato_empleado.em_ne_area_empresa_id, " +
      "(CASE WHEN no_contrato_empleado.em_ne_area_empresa_id IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Nombre), ''))),'') ) END) AS em_ne_area_empresa_idR, " +
      "no_contrato_empleado.EM_Atnorh_Cargo_ID, " +
      "(CASE WHEN no_contrato_empleado.EM_Atnorh_Cargo_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS EM_Atnorh_Cargo_IDR, " +
      "COALESCE(no_contrato_empleado.Isactive, 'N') AS Isactive, " +
      "no_contrato_empleado.em_ne_vacacion_prop, " +
      "no_contrato_empleado.em_ne_vacacion_tom, " +
      "no_contrato_empleado.em_ne_vacacion_res, " +
      "no_contrato_empleado.Salario, " +
      "no_contrato_empleado.C_Currency_ID, " +
      "(CASE WHEN no_contrato_empleado.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "COALESCE(no_contrato_empleado.em_ne_is_jornada_parcial, 'N') AS em_ne_is_jornada_parcial, " +
      "no_contrato_empleado.em_ne_num_horas_parciales, " +
      "no_contrato_empleado.em_ne_sissalnet, " +
      "no_contrato_empleado.em_ne_region, " +
      "COALESCE(no_contrato_empleado.Pagofondoreserva, 'N') AS Pagofondoreserva, " +
      "COALESCE(no_contrato_empleado.aplica_utilidad, 'N') AS aplica_utilidad, " +
      "no_contrato_empleado.em_ne_motivo_salida, " +
      "(CASE WHEN no_contrato_empleado.em_ne_motivo_salida IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS em_ne_motivo_salidaR, " +
      "no_contrato_empleado.em_ne_observaciones, " +
      "no_contrato_empleado.Liquidacion_Empleado, " +
      "no_contrato_empleado.Docactionno, " +
      "list3.name as Docactionno_BTN, " +
      "no_contrato_empleado.Reenvio_Azure, " +
      "no_contrato_empleado.Tipo_Contrato, " +
      "no_contrato_empleado.FIN_Paymentmethod_ID, " +
      "no_contrato_empleado.FIN_Financial_Account_ID, " +
      "no_contrato_empleado.NO_Contrato_Empleado_ID, " +
      "no_contrato_empleado.AD_Client_ID, " +
      "COALESCE(no_contrato_empleado.IS_Impuesto_Asumido, 'N') AS IS_Impuesto_Asumido, " +
      "        ? AS LANGUAGE " +
      "        FROM no_contrato_empleado left join ad_ref_list_v list1 on (no_contrato_empleado.Docstatus = list1.value and list1.ad_reference_id = 'E45117AAE15841AAB98FFF75970AA6E0' and list1.ad_language = ?)  left join (select AD_Org_ID, Name from AD_Org) table1 on (no_contrato_empleado.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (no_contrato_empleado.Doctype_Servicios_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table4 on (no_contrato_empleado.Proveedor_ID = table4.C_BPartner_ID) left join (select NO_Area_Empresa_ID, Nombre from no_area_empresa) table5 on (no_contrato_empleado.em_ne_area_empresa_id =  table5.NO_Area_Empresa_ID) left join (select Atnorh_Cargo_ID, Name from atnorh_cargo) table6 on (no_contrato_empleado.EM_Atnorh_Cargo_ID =  table6.Atnorh_Cargo_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table7 on (no_contrato_empleado.C_Currency_ID = table7.C_Currency_ID) left join ad_ref_list_v list2 on (no_contrato_empleado.em_ne_motivo_salida = list2.value and list2.ad_reference_id = 'B08E1E7FC07F46AEB9CF7301EE8ACD50' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (list3.ad_reference_id = 'FB9AC7F0B808481EBCFB59A37571C352' and list3.ad_language = ?  AND no_contrato_empleado.Docactionno = TO_CHAR(list3.value))" +
      "        WHERE 2=2 " +
      " AND No_Contrato_Empleado.C_Bpartner_ID is null" +
      "        AND 1=1 " +
      "        AND no_contrato_empleado.NO_Contrato_Empleado_ID = ? " +
      "        AND no_contrato_empleado.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_contrato_empleado.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData = new ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData();
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.created = UtilSql.getValue(result, "created");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.updated = UtilSql.getValue(result, "updated");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.updatedby = UtilSql.getValue(result, "updatedby");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.docstatus = UtilSql.getValue(result, "docstatus");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.doctypeServiciosId = UtilSql.getValue(result, "doctype_servicios_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.doctypeServiciosIdr = UtilSql.getValue(result, "doctype_servicios_idr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.documentno = UtilSql.getValue(result, "documentno");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.proveedorId = UtilSql.getValue(result, "proveedor_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.proveedorIdr = UtilSql.getValue(result, "proveedor_idr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeAreaEmpresaId = UtilSql.getValue(result, "em_ne_area_empresa_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeAreaEmpresaIdr = UtilSql.getValue(result, "em_ne_area_empresa_idr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emAtnorhCargoId = UtilSql.getValue(result, "em_atnorh_cargo_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emAtnorhCargoIdr = UtilSql.getValue(result, "em_atnorh_cargo_idr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.isactive = UtilSql.getValue(result, "isactive");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeVacacionProp = UtilSql.getValue(result, "em_ne_vacacion_prop");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeVacacionTom = UtilSql.getValue(result, "em_ne_vacacion_tom");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeVacacionRes = UtilSql.getValue(result, "em_ne_vacacion_res");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.salario = UtilSql.getValue(result, "salario");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeIsJornadaParcial = UtilSql.getValue(result, "em_ne_is_jornada_parcial");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeNumHorasParciales = UtilSql.getValue(result, "em_ne_num_horas_parciales");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeSissalnet = UtilSql.getValue(result, "em_ne_sissalnet");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeRegion = UtilSql.getValue(result, "em_ne_region");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.pagofondoreserva = UtilSql.getValue(result, "pagofondoreserva");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.aplicaUtilidad = UtilSql.getValue(result, "aplica_utilidad");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeMotivoSalida = UtilSql.getValue(result, "em_ne_motivo_salida");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeMotivoSalidar = UtilSql.getValue(result, "em_ne_motivo_salidar");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.emNeObservaciones = UtilSql.getValue(result, "em_ne_observaciones");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.liquidacionEmpleado = UtilSql.getValue(result, "liquidacion_empleado");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.docactionno = UtilSql.getValue(result, "docactionno");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.docactionnoBtn = UtilSql.getValue(result, "docactionno_btn");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.reenvioAzure = UtilSql.getValue(result, "reenvio_azure");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.tipoContrato = UtilSql.getValue(result, "tipo_contrato");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.noContratoEmpleadoId = UtilSql.getValue(result, "no_contrato_empleado_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.isImpuestoAsumido = UtilSql.getValue(result, "is_impuesto_asumido");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.language = UtilSql.getValue(result, "language");
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.adUserClient = "";
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.adOrgClient = "";
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.createdby = "";
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.trBgcolor = "";
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.totalCount = "";
        objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[] = new ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[vector.size()];
    vector.copyInto(objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData);
    return(objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData);
  }

/**
Create a registry
 */
  public static ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[] set(String emNeVacacionTom, String reenvioAzure, String docactionno, String docactionnoBtn, String emNeMotivoSalida, String isImpuestoAsumido, String salario, String aplicaUtilidad, String adClientId, String isactive, String emNeVacacionProp, String docstatus, String emNeIsJornadaParcial, String createdby, String createdbyr, String emNeAreaEmpresaId, String cCurrencyId, String cDoctypeId, String emNeObservaciones, String liquidacionEmpleado, String doctypeServiciosId, String emNeRegion, String emNeVacacionRes, String documentno, String fechaFin, String emAtnorhCargoId, String finPaymentmethodId, String pagofondoreserva, String updatedby, String updatedbyr, String fechaInicio, String noContratoEmpleadoId, String tipoContrato, String adOrgId, String proveedorId, String proveedorIdr, String finFinancialAccountId, String emNeNumHorasParciales, String emNeSissalnet)    throws ServletException {
    ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[] = new ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[1];
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0] = new ContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData();
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].created = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].createdbyr = createdbyr;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].updated = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].updatedTimeStamp = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].updatedby = updatedby;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].updatedbyr = updatedbyr;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].docstatus = docstatus;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].docstatusr = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].adOrgId = adOrgId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].adOrgIdr = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].doctypeServiciosId = doctypeServiciosId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].doctypeServiciosIdr = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].cDoctypeId = cDoctypeId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].documentno = documentno;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].proveedorId = proveedorId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].proveedorIdr = proveedorIdr;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].fechaInicio = fechaInicio;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].fechaFin = fechaFin;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeAreaEmpresaId = emNeAreaEmpresaId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeAreaEmpresaIdr = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emAtnorhCargoId = emAtnorhCargoId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emAtnorhCargoIdr = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].isactive = isactive;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeVacacionProp = emNeVacacionProp;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeVacacionTom = emNeVacacionTom;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeVacacionRes = emNeVacacionRes;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].salario = salario;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].cCurrencyId = cCurrencyId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].cCurrencyIdr = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeIsJornadaParcial = emNeIsJornadaParcial;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeNumHorasParciales = emNeNumHorasParciales;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeSissalnet = emNeSissalnet;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeRegion = emNeRegion;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].pagofondoreserva = pagofondoreserva;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].aplicaUtilidad = aplicaUtilidad;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeMotivoSalida = emNeMotivoSalida;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeMotivoSalidar = "";
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].emNeObservaciones = emNeObservaciones;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].liquidacionEmpleado = liquidacionEmpleado;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].docactionno = docactionno;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].docactionnoBtn = docactionnoBtn;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].reenvioAzure = reenvioAzure;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].tipoContrato = tipoContrato;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].finPaymentmethodId = finPaymentmethodId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].finFinancialAccountId = finFinancialAccountId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].noContratoEmpleadoId = noContratoEmpleadoId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].adClientId = adClientId;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].isImpuestoAsumido = isImpuestoAsumido;
    objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData[0].language = "";
    return objectContratoServiciosD4018D2407234FC892CB8DAF8AE5141EData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef51CD8EE238E246ABB405A694B4F2686B_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefCB5C3B91D6FD498F98EF2B355BE07E36_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE0E69B2F38744B50865DD11F03D7464F_2(ConnectionProvider connectionProvider, String Proveedor_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Proveedor_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Proveedor_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "proveedor_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_contrato_empleado" +
      "        SET Docstatus = (?) , AD_Org_ID = (?) , Doctype_Servicios_ID = (?) , C_Doctype_ID = (?) , Documentno = (?) , Proveedor_ID = (?) , Fecha_Inicio = TO_DATE(?) , Fecha_Fin = TO_DATE(?) , em_ne_area_empresa_id = (?) , EM_Atnorh_Cargo_ID = (?) , Isactive = (?) , em_ne_vacacion_prop = TO_NUMBER(?) , em_ne_vacacion_tom = TO_NUMBER(?) , em_ne_vacacion_res = TO_NUMBER(?) , Salario = TO_NUMBER(?) , C_Currency_ID = (?) , em_ne_is_jornada_parcial = (?) , em_ne_num_horas_parciales = TO_NUMBER(?) , em_ne_sissalnet = (?) , em_ne_region = (?) , Pagofondoreserva = (?) , aplica_utilidad = (?) , em_ne_motivo_salida = (?) , em_ne_observaciones = (?) , Liquidacion_Empleado = (?) , Docactionno = (?) , Reenvio_Azure = (?) , Tipo_Contrato = (?) , FIN_Paymentmethod_ID = (?) , FIN_Financial_Account_ID = (?) , NO_Contrato_Empleado_ID = (?) , AD_Client_ID = (?) , IS_Impuesto_Asumido = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_contrato_empleado.NO_Contrato_Empleado_ID = ? " +
      "        AND no_contrato_empleado.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_contrato_empleado.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, doctypeServiciosId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, proveedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtnorhCargoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeVacacionProp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeVacacionTom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeVacacionRes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salario);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeIsJornadaParcial);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeNumHorasParciales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeSissalnet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeRegion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagofondoreserva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aplicaUtilidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeMotivoSalida);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeObservaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, liquidacionEmpleado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reenvioAzure);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoContrato);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isImpuestoAsumido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_contrato_empleado " +
      "        (Docstatus, AD_Org_ID, Doctype_Servicios_ID, C_Doctype_ID, Documentno, Proveedor_ID, Fecha_Inicio, Fecha_Fin, em_ne_area_empresa_id, EM_Atnorh_Cargo_ID, Isactive, em_ne_vacacion_prop, em_ne_vacacion_tom, em_ne_vacacion_res, Salario, C_Currency_ID, em_ne_is_jornada_parcial, em_ne_num_horas_parciales, em_ne_sissalnet, em_ne_region, Pagofondoreserva, aplica_utilidad, em_ne_motivo_salida, em_ne_observaciones, Liquidacion_Empleado, Docactionno, Reenvio_Azure, Tipo_Contrato, FIN_Paymentmethod_ID, FIN_Financial_Account_ID, NO_Contrato_Empleado_ID, AD_Client_ID, IS_Impuesto_Asumido, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, doctypeServiciosId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, proveedorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtnorhCargoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeVacacionProp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeVacacionTom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeVacacionRes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salario);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeIsJornadaParcial);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeNumHorasParciales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeSissalnet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeRegion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagofondoreserva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aplicaUtilidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeMotivoSalida);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeObservaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, liquidacionEmpleado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reenvioAzure);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoContrato);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isImpuestoAsumido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_contrato_empleado" +
      "        WHERE no_contrato_empleado.NO_Contrato_Empleado_ID = ? " +
      "        AND no_contrato_empleado.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_contrato_empleado.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_contrato_empleado" +
      "         WHERE no_contrato_empleado.NO_Contrato_Empleado_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_contrato_empleado" +
      "         WHERE no_contrato_empleado.NO_Contrato_Empleado_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
