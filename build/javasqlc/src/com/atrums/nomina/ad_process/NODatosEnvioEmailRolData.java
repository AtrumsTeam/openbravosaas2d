//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NODatosEnvioEmailRolData implements FieldProvider {
static Logger log4j = Logger.getLogger(NODatosEnvioEmailRolData.class);
  private String InitRecordNumber="0";
  public String tercero;
  public String email;
  public String anio;
  public String mes;
  public String idrol;
  public String empresa;
  public String noMensajeEnviar;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("tercero"))
      return tercero;
    else if (fieldName.equalsIgnoreCase("email"))
      return email;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("mes"))
      return mes;
    else if (fieldName.equalsIgnoreCase("idrol"))
      return idrol;
    else if (fieldName.equalsIgnoreCase("empresa"))
      return empresa;
    else if (fieldName.equalsIgnoreCase("no_mensaje_enviar") || fieldName.equals("noMensajeEnviar"))
      return noMensajeEnviar;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NODatosEnvioEmailRolData[] seleccionarRolPagos(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return seleccionarRolPagos(connectionProvider, adClientId, 0, 0);
  }

  public static NODatosEnvioEmailRolData[] seleccionarRolPagos(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			select upper(b.name) as tercero," +
      "			       (select array_to_string(ARRAY_AGG(u.email),',') from ad_user u where (u.c_bpartner_id = a.c_bpartner_id) and u.em_no_rol_mail = 'Y') as email, " +
      "			       d.year as anio," +
      "			       upper(case when c.periodno = 1 then 'Enero'" +
      "			                  when c.periodno = 2 then 'Febrero'" +
      "			                  when c.periodno = 3 then 'Marzo'" +
      "			                  when c.periodno = 4 then 'Abril'" +
      "			                  when c.periodno = 5 then 'Mayo'" +
      "			                  when c.periodno = 6 then 'Junio'" +
      "			                  when c.periodno = 7 then 'Julio'" +
      "			                  when c.periodno = 8 then 'Agosto'" +
      "			                  when c.periodno = 9 then 'Septiembre'" +
      "			                  when c.periodno = 10 then 'Octubre'" +
      "			                  when c.periodno = 11 then 'Noviembre'" +
      "			                  when c.periodno = 12 then 'Diciembre'" +
      "			              end) as mes," +
      "			       a.no_rol_pago_provision_id as idrol," +
      "			       e.name as empresa," +
      "			       a.no_mensaje_enviar" +
      "			  from no_rol_pago_provision a, c_bpartner b, c_period c, c_year d, ad_client e" +
      "			 where a.c_bpartner_id = b.c_bpartner_id" +
      "			   and a.c_period_id = c.c_period_id" +
      "			   and c.c_year_id = d.c_year_id" +
      "			   and a.ad_client_id = e.ad_client_id" +
      "			   and a.no_enviar = 'Y'" +
      "			   and a.docstatus = 'CO'" +
      "			   and a.ad_client_id = ?" +
      "			 order by 2 desc, 7 asc" +
      "			 limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NODatosEnvioEmailRolData objectNODatosEnvioEmailRolData = new NODatosEnvioEmailRolData();
        objectNODatosEnvioEmailRolData.tercero = UtilSql.getValue(result, "tercero");
        objectNODatosEnvioEmailRolData.email = UtilSql.getValue(result, "email");
        objectNODatosEnvioEmailRolData.anio = UtilSql.getValue(result, "anio");
        objectNODatosEnvioEmailRolData.mes = UtilSql.getValue(result, "mes");
        objectNODatosEnvioEmailRolData.idrol = UtilSql.getValue(result, "idrol");
        objectNODatosEnvioEmailRolData.empresa = UtilSql.getValue(result, "empresa");
        objectNODatosEnvioEmailRolData.noMensajeEnviar = UtilSql.getValue(result, "no_mensaje_enviar");
        objectNODatosEnvioEmailRolData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNODatosEnvioEmailRolData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NODatosEnvioEmailRolData objectNODatosEnvioEmailRolData[] = new NODatosEnvioEmailRolData[vector.size()];
    vector.copyInto(objectNODatosEnvioEmailRolData);
    return(objectNODatosEnvioEmailRolData);
  }

  public static int actualizarEnvioRol(ConnectionProvider connectionProvider, String noRolPagoProvisionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	      update no_rol_pago_provision" +
      "			 set no_enviar = 'N', no_mensaje_enviar = 'El rol de Pagos fue enviado.'" +
      "		   where no_rol_pago_provision_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int actualizarNoEnvioRol(ConnectionProvider connectionProvider, String noRolPagoProvisionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	      update no_rol_pago_provision" +
      "			 set no_mensaje_enviar = 'No se logra enviar el Rol de Pagos, verificar el correo del empleado.'" +
      "		   where no_rol_pago_provision_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
