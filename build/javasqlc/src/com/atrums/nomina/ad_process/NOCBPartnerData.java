//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NOCBPartnerData implements FieldProvider {
static Logger log4j = Logger.getLogger(NOCBPartnerData.class);
  private String InitRecordNumber="0";
  public String organizacion;
  public String mes;
  public String anio;
  public String rolpago;
  public String tercero;
  public String email;
  public String valorpagoservicio;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("organizacion"))
      return organizacion;
    else if (fieldName.equalsIgnoreCase("mes"))
      return mes;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("rolpago"))
      return rolpago;
    else if (fieldName.equalsIgnoreCase("tercero"))
      return tercero;
    else if (fieldName.equalsIgnoreCase("email"))
      return email;
    else if (fieldName.equalsIgnoreCase("valorpagoservicio"))
      return valorpagoservicio;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NOCBPartnerData[] select(ConnectionProvider connectionProvider, String rolpagoId)    throws ServletException {
    return select(connectionProvider, rolpagoId, 0, 0);
  }

  public static NOCBPartnerData[] select(ConnectionProvider connectionProvider, String rolpagoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select upper(suc.name) as organizacion," +
      "		       upper(case when pe.periodno = 1 then 'Enero'" +
      "		            when pe.periodno = 2 then 'Febrero'" +
      "		            when pe.periodno = 3 then 'Marzo'" +
      "		            when pe.periodno = 4 then 'Abril'" +
      "		            when pe.periodno = 5 then 'Mayo'" +
      "		            when pe.periodno = 6 then 'Junio'" +
      "		            when pe.periodno = 7 then 'Julio'" +
      "		            when pe.periodno = 8 then 'Agosto'" +
      "		            when pe.periodno = 9 then 'Septiembre'" +
      "		            when pe.periodno = 10 then 'Octubre'" +
      "		            when pe.periodno = 11 then 'Noviembre'" +
      "		            when pe.periodno = 12 then 'Diciembre'" +
      "		       end) as mes," +
      "		       y.year as anio," +
      "		       rp.no_rol_pago_provision_id as rolpago, " +
      "		       em.name as tercero,  " +
      "		       us.email as email," +
      "		       round(rp.total_neto, 2)  as valorPagoServicio" +
      "		  from no_rol_pago_provision rp, ad_user us, c_period pe, c_bpartner em, ad_org suc," +
      "		       c_year y             " +
      "		 where us.c_bpartner_id = rp.c_bpartner_id" +
      "		   and pe.c_period_id = rp.c_period_id" +
      "		   and pe.c_year_id = y.c_year_id" +
      "		   and rp.c_bpartner_id = em.c_bpartner_id" +
      "		   and us.em_no_rol_mail = 'Y'" +
      "		   and rp.ispago='Y'" +
      "		   and rp.isactive = 'Y' " +
      "		   and suc.ad_org_id = rp.ad_org_id" +
      "		   and rp.no_rol_pago_provision_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rolpagoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOCBPartnerData objectNOCBPartnerData = new NOCBPartnerData();
        objectNOCBPartnerData.organizacion = UtilSql.getValue(result, "organizacion");
        objectNOCBPartnerData.mes = UtilSql.getValue(result, "mes");
        objectNOCBPartnerData.anio = UtilSql.getValue(result, "anio");
        objectNOCBPartnerData.rolpago = UtilSql.getValue(result, "rolpago");
        objectNOCBPartnerData.tercero = UtilSql.getValue(result, "tercero");
        objectNOCBPartnerData.email = UtilSql.getValue(result, "email");
        objectNOCBPartnerData.valorpagoservicio = UtilSql.getValue(result, "valorpagoservicio");
        objectNOCBPartnerData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOCBPartnerData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOCBPartnerData objectNOCBPartnerData[] = new NOCBPartnerData[vector.size()];
    vector.copyInto(objectNOCBPartnerData);
    return(objectNOCBPartnerData);
  }

  public static NOCBPartnerData[] selectEmployee(ConnectionProvider connectionProvider)    throws ServletException {
    return selectEmployee(connectionProvider, 0, 0);
  }

  public static NOCBPartnerData[] selectEmployee(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      	SELECT part.c_bpartner_id as tercero" +
      "      	  FROM C_BPARTNER part" +
      "		LEFT OUTER JOIN AD_USER  ON part.c_bpartner_id = AD_USER.c_bpartner_id" +
      "		WHERE  AD_USER.c_bpartner_id IS NULL";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOCBPartnerData objectNOCBPartnerData = new NOCBPartnerData();
        objectNOCBPartnerData.tercero = UtilSql.getValue(result, "tercero");
        objectNOCBPartnerData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOCBPartnerData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOCBPartnerData objectNOCBPartnerData[] = new NOCBPartnerData[vector.size()];
    vector.copyInto(objectNOCBPartnerData);
    return(objectNOCBPartnerData);
  }

  public static NOCBPartnerData[] selectPagoServicio(ConnectionProvider connectionProvider, String rolpagoId)    throws ServletException {
    return selectPagoServicio(connectionProvider, rolpagoId, 0, 0);
  }

  public static NOCBPartnerData[] selectPagoServicio(ConnectionProvider connectionProvider, String rolpagoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select upper(c.name) as organizacion," +
      "		       upper(case when to_char(date(p.startdate) - interval '1 month', 'mm') = '01' then 'Enero'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '02' then 'Febrero'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '03' then 'Marzo'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '04' then 'Abril'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '05' then 'Mayo'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '06' then 'Junio'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '07' then 'Julio'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '08' then 'Agosto'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '09' then 'Septiembre'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '10' then 'Octubre'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '11' then 'Noviembre'" +
      "            when to_char(date(p.startdate) - interval '1 month', 'mm') = '12' then 'Diciembre'" +
      "       end) as mes," +
      "               extract(year from date(p.startdate) - interval '1 month') as anio," +
      "		       rq.no_registra_quinc_line_id as rolpago," +
      "		       bp.name as tercero, " +
      "		       us.email as email," +
      "		       rq.valor as valorPagoServicio" +
      "		  from no_registra_quincena q, no_registra_quinc_line rq, ad_client cl, ad_org suc," +
      "		       c_period p, c_year y, c_bpartner bp, no_contrato_empleado ce, ad_user us," +
      "               ad_org c" +
      "		 where q.no_registra_quincena_id = rq.no_registra_quincena_id" +
      "		   and cl.ad_client_id = q.ad_client_id" +
      "		   and suc.ad_org_id = q.ad_org_id" +
      "           and ce.ad_org_id = c.ad_org_id" +
      "		   and q.c_period_id = p.c_period_id" +
      "		   and p.c_year_id = y.c_year_id" +
      "		   and bp.c_bpartner_id = rq.c_bpartner_id" +
      "		   and ce.c_bpartner_id = rq.c_bpartner_id" +
      "		   and us.c_bpartner_id = bp.c_bpartner_id" +
      "		   and rq.no_registra_quinc_line_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rolpagoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOCBPartnerData objectNOCBPartnerData = new NOCBPartnerData();
        objectNOCBPartnerData.organizacion = UtilSql.getValue(result, "organizacion");
        objectNOCBPartnerData.mes = UtilSql.getValue(result, "mes");
        objectNOCBPartnerData.anio = UtilSql.getValue(result, "anio");
        objectNOCBPartnerData.rolpago = UtilSql.getValue(result, "rolpago");
        objectNOCBPartnerData.tercero = UtilSql.getValue(result, "tercero");
        objectNOCBPartnerData.email = UtilSql.getValue(result, "email");
        objectNOCBPartnerData.valorpagoservicio = UtilSql.getValue(result, "valorpagoservicio");
        objectNOCBPartnerData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOCBPartnerData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOCBPartnerData objectNOCBPartnerData[] = new NOCBPartnerData[vector.size()];
    vector.copyInto(objectNOCBPartnerData);
    return(objectNOCBPartnerData);
  }
}
