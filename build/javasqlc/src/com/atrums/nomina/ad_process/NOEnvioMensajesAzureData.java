//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NOEnvioMensajesAzureData implements FieldProvider {
static Logger log4j = Logger.getLogger(NOEnvioMensajesAzureData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;
  public String dato9;
  public String dato10;
  public String dato11;
  public String dato12;
  public String dato13;
  public String dato14;
  public String dato15;
  public String dato16;
  public String dato17;
  public String dato18;
  public String dato19;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
    else if (fieldName.equalsIgnoreCase("dato9"))
      return dato9;
    else if (fieldName.equalsIgnoreCase("dato10"))
      return dato10;
    else if (fieldName.equalsIgnoreCase("dato11"))
      return dato11;
    else if (fieldName.equalsIgnoreCase("dato12"))
      return dato12;
    else if (fieldName.equalsIgnoreCase("dato13"))
      return dato13;
    else if (fieldName.equalsIgnoreCase("dato14"))
      return dato14;
    else if (fieldName.equalsIgnoreCase("dato15"))
      return dato15;
    else if (fieldName.equalsIgnoreCase("dato16"))
      return dato16;
    else if (fieldName.equalsIgnoreCase("dato17"))
      return dato17;
    else if (fieldName.equalsIgnoreCase("dato18"))
      return dato18;
    else if (fieldName.equalsIgnoreCase("dato19"))
      return dato19;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NOEnvioMensajesAzureData[] methodBuscarContratosNuevos(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return methodBuscarContratosNuevos(connectionProvider, adClientId, 0, 0);
  }

  public static NOEnvioMensajesAzureData[] methodBuscarContratosNuevos(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      (select (case when c.estado_envio = 'MPC' then '1'" +
      "                    when c.estado_envio = 'MPL' then '2'" +
      "                    when c.estado_envio = 'MPA' then '3' end) as dato1," +
      "          (select f.taxid from ad_org o inner join ad_orginfo f on (o.ad_org_id = f.ad_org_id)" +
      "              	      where o.ad_client_id = c.ad_client_id order by f.created desc limit 1) as dato2," +
      "	      p.value as dato3, p.em_co_nombres as dato4, p.em_co_apellidos as dato5, " +
      "	      (case when em_co_tipo_identificacion = '02' then '1'" +
      "	            when em_co_tipo_identificacion = '01' then '2'" +
      "	            when em_co_tipo_identificacion = '03' then '3' end) as dato6," +
      "	      p.value as dato7," +
      "	      coalesce((select email from ad_user where c_bpartner_id = p.c_bpartner_id and em_no_check_azure='Y' order by created desc limit 1),'') as dato8," +
      "	      coalesce((select coalesce(em_no_correo_empresarial, email) from ad_user where c_bpartner_id = p.c_bpartner_id and em_no_check_azure='Y' order by created desc limit 1),'') as dato9," +
      "	      (case when upper(d.name) = upper('Contrato Indefinido') then '1'" +
      "	            when upper(d.name) = upper('Contrato por Proyecto') then '2'" +
      "	            when upper(d.name) = upper('Contrato de Servicios Profesionales') then '3'" +
      "	            when upper(d.name) = upper('Contrato Eventual') then '4'" +
      "	            when upper(d.name) = upper('Contrato por Giro de Negocio') then '5' end) as dato10, " +
      "	      d.name as dato11, c.documentno as dato12, c.salario as dato13," +
      "	      to_char(c.fecha_inicio,'dd/mm/yyyy') as dato14, to_char(c.fecha_fin,'dd/mm/yyyy') as dato15, coalesce(a.atnorh_cargo_id, '') as dato16, coalesce(a.name, '') as dato17, " +
      "	      to_char(now(),'dd/mm/yy HH24:MI:SS') as dato18, c.no_contrato_empleado_id as dato19" +
      "	       from no_contrato_empleado c " +
      "	       inner join c_bpartner p on (c.c_bpartner_id = p.c_bpartner_id)" +
      "	       inner join c_doctype d on (c.c_doctype_id = d.c_doctype_id)" +
      "	       left join atnorh_cargo a on (c.em_atnorh_cargo_id = a.atnorh_cargo_id)" +
      "	       where c.estado_envio <> 'NA'" +
      "	       and c.estado_envio <> 'ME'" +
      "	       and c.docstatus <> 'BR'" +
      "		   and c.ad_client_id = ? " +
      "		   order by c.estado_envio)  " +
      "	   union all" +
      "       (select (case when c.estado_envio = 'MPC' then '1'" +
      "                     when c.estado_envio = 'MPL' then '2'" +
      "                     when c.estado_envio = 'MPA' then '3' end) as dato1," +
      "         (select f.taxid from ad_org o inner join ad_orginfo f on (o.ad_org_id = f.ad_org_id)" +
      "                     where o.ad_client_id = c.ad_client_id order by f.created desc limit 1) as dato2," +
      "	     (case when em_co_tipo_identificacion in ('01','03') then p.em_no_cedula else p.value end) as dato3, p.em_co_nombres as dato4, p.em_co_apellidos as dato5, " +
      "	     (case when em_co_tipo_identificacion = '02' then '1'" +
      "	           when em_co_tipo_identificacion = '01' then '2'" +
      "	           when em_co_tipo_identificacion = '03' then '3' end) as dato6," +
      "	      p.value as dato7," +
      "	      coalesce((select email from ad_user where c_bpartner_id = p.c_bpartner_id and em_no_check_azure='Y' order by created desc limit 1),'') as dato8," +
      "	      coalesce((select coalesce(em_no_correo_empresarial, email) from ad_user where c_bpartner_id = p.c_bpartner_id and em_no_check_azure='Y' order by created desc limit 1),'') as dato9,      " +
      "	      (case when upper(d.name) = upper('Contrato de Servicios Profesionales') then '3' end) as dato10, " +
      "	      d.name as dato11, c.documentno as dato12, c.salario as dato13," +
      "	      to_char(c.fecha_inicio,'dd/mm/yyyy') as dato14, to_char(c.fecha_fin,'dd/mm/yyyy') as dato15, coalesce(a.atnorh_cargo_id, '') as dato16, coalesce(a.name, '') as dato17, " +
      "	      to_char(now(),'dd/mm/yy HH24:MI:SS') as dato18, c.no_contrato_empleado_id as dato19" +
      "	       from no_contrato_empleado c " +
      "	       inner join c_bpartner p on (c.proveedor_id = p.c_bpartner_id)" +
      "	       inner join c_doctype d on (c.c_doctype_id = d.c_doctype_id)" +
      "	       left join atnorh_cargo a on (c.em_atnorh_cargo_id = a.atnorh_cargo_id)" +
      "	       where c.estado_envio <> 'NA'" +
      "	       and c.estado_envio <> 'ME'" +
      "	       and c.docstatus <> 'BR'" +
      "		   and c.ad_client_id = ?" +
      "		   order by c.estado_envio)   ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOEnvioMensajesAzureData objectNOEnvioMensajesAzureData = new NOEnvioMensajesAzureData();
        objectNOEnvioMensajesAzureData.dato1 = UtilSql.getValue(result, "dato1");
        objectNOEnvioMensajesAzureData.dato2 = UtilSql.getValue(result, "dato2");
        objectNOEnvioMensajesAzureData.dato3 = UtilSql.getValue(result, "dato3");
        objectNOEnvioMensajesAzureData.dato4 = UtilSql.getValue(result, "dato4");
        objectNOEnvioMensajesAzureData.dato5 = UtilSql.getValue(result, "dato5");
        objectNOEnvioMensajesAzureData.dato6 = UtilSql.getValue(result, "dato6");
        objectNOEnvioMensajesAzureData.dato7 = UtilSql.getValue(result, "dato7");
        objectNOEnvioMensajesAzureData.dato8 = UtilSql.getValue(result, "dato8");
        objectNOEnvioMensajesAzureData.dato9 = UtilSql.getValue(result, "dato9");
        objectNOEnvioMensajesAzureData.dato10 = UtilSql.getValue(result, "dato10");
        objectNOEnvioMensajesAzureData.dato11 = UtilSql.getValue(result, "dato11");
        objectNOEnvioMensajesAzureData.dato12 = UtilSql.getValue(result, "dato12");
        objectNOEnvioMensajesAzureData.dato13 = UtilSql.getValue(result, "dato13");
        objectNOEnvioMensajesAzureData.dato14 = UtilSql.getValue(result, "dato14");
        objectNOEnvioMensajesAzureData.dato15 = UtilSql.getValue(result, "dato15");
        objectNOEnvioMensajesAzureData.dato16 = UtilSql.getValue(result, "dato16");
        objectNOEnvioMensajesAzureData.dato17 = UtilSql.getValue(result, "dato17");
        objectNOEnvioMensajesAzureData.dato18 = UtilSql.getValue(result, "dato18");
        objectNOEnvioMensajesAzureData.dato19 = UtilSql.getValue(result, "dato19");
        objectNOEnvioMensajesAzureData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOEnvioMensajesAzureData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOEnvioMensajesAzureData objectNOEnvioMensajesAzureData[] = new NOEnvioMensajesAzureData[vector.size()];
    vector.copyInto(objectNOEnvioMensajesAzureData);
    return(objectNOEnvioMensajesAzureData);
  }

  public static int methodActualizaEstadoEnvio(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      update no_contrato_empleado set estado_envio = 'ME', reenvio_azure = 'Y', updated = now() where no_contrato_empleado_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
