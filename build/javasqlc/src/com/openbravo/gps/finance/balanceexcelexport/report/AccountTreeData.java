//Sqlc generated V1.O00-1
package com.openbravo.gps.finance.balanceexcelexport.report;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

public class AccountTreeData implements FieldProvider {
static Logger log4j = Logger.getLogger(AccountTreeData.class);
  private String InitRecordNumber="0";
  public String nodeId;
  public String parentId;
  public String seqno;
  public String id;
  public String name;
  public String description;
  public String issummary;
  public String accountsign;
  public String showelement;
  public String elementLevel;
  public String qty;
  public String qtyProy1;
  public String qtyProy2;
  public String qtyProy3;
  public String qtyProy4;
  public String qtyProy5;
  public String qtyProy6;
  public String qtyProy7;
  public String qtyProy8;
  public String qtyProy9;
  public String qtyProy10;
  public String qtyProy11;
  public String qtyProy12;
  public String qtyProy13;
  public String qtyProy14;
  public String qtyProy15;
  public String qtyProy16;
  public String qtyProy17;
  public String qtyProy18;
  public String qtyProy19;
  public String qtyProy20;
  public String qtyProy21;
  public String qtyProy22;
  public String qtyProy23;
  public String qtyProy24;
  public String qtyProy25;
  public String qtyProy26;
  public String qtyProy27;
  public String qtyProy28;
  public String qtyProy29;
  public String qtyProy30;
  public String qtyRef;
  public String qtyOperation;
  public String qtyOperationRef;
  public String qtyOperationProy1;
  public String qtyOperationProy2;
  public String qtyOperationProy3;
  public String qtyOperationProy4;
  public String qtyOperationProy5;
  public String qtyOperationProy6;
  public String qtyOperationProy7;
  public String qtyOperationProy8;
  public String qtyOperationProy9;
  public String qtyOperationProy10;
  public String qtyOperationProy11;
  public String qtyOperationProy12;
  public String qtyOperationProy13;
  public String qtyOperationProy14;
  public String qtyOperationProy15;
  public String qtyOperationProy16;
  public String qtyOperationProy17;
  public String qtyOperationProy18;
  public String qtyOperationProy19;
  public String qtyOperationProy20;
  public String qtyOperationProy21;
  public String qtyOperationProy22;
  public String qtyOperationProy23;
  public String qtyOperationProy24;
  public String qtyOperationProy25;
  public String qtyOperationProy26;
  public String qtyOperationProy27;
  public String qtyOperationProy28;
  public String qtyOperationProy29;
  public String qtyOperationProy30;
  public String qtycredit;
  public String qtycreditRef;
  public String qtycreditProy1;
  public String qtycreditProy2;
  public String qtycreditProy3;
  public String qtycreditProy4;
  public String qtycreditProy5;
  public String qtycreditProy6;
  public String qtycreditProy7;
  public String qtycreditProy8;
  public String qtycreditProy9;
  public String qtycreditProy10;
  public String qtycreditProy11;
  public String qtycreditProy12;
  public String qtycreditProy13;
  public String qtycreditProy14;
  public String qtycreditProy15;
  public String qtycreditProy16;
  public String qtycreditProy17;
  public String qtycreditProy18;
  public String qtycreditProy19;
  public String qtycreditProy20;
  public String qtycreditProy21;
  public String qtycreditProy22;
  public String qtycreditProy23;
  public String qtycreditProy24;
  public String qtycreditProy25;
  public String qtycreditProy26;
  public String qtycreditProy27;
  public String qtycreditProy28;
  public String qtycreditProy29;
  public String qtycreditProy30;
  public String showvaluecond;
  public String elementlevel;
  public String value;
  public String calculated;
  public String svcreset;
  public String svcresetref;
  public String svcresetproy1;
  public String svcresetproy2;
  public String svcresetproy3;
  public String svcresetproy4;
  public String svcresetproy5;
  public String svcresetproy6;
  public String svcresetproy7;
  public String svcresetproy8;
  public String svcresetproy9;
  public String svcresetproy10;
  public String svcresetproy11;
  public String svcresetproy12;
  public String svcresetproy13;
  public String svcresetproy14;
  public String svcresetproy15;
  public String svcresetproy16;
  public String svcresetproy17;
  public String svcresetproy18;
  public String svcresetproy19;
  public String svcresetproy20;
  public String svcresetproy21;
  public String svcresetproy22;
  public String svcresetproy23;
  public String svcresetproy24;
  public String svcresetproy25;
  public String svcresetproy26;
  public String svcresetproy27;
  public String svcresetproy28;
  public String svcresetproy29;
  public String svcresetproy30;
  public String isalwaysshown;
  public String sign;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("node_id") || fieldName.equals("nodeId"))
      return nodeId;
    else if (fieldName.equalsIgnoreCase("parent_id") || fieldName.equals("parentId"))
      return parentId;
    else if (fieldName.equalsIgnoreCase("seqno"))
      return seqno;
    else if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("accountsign"))
      return accountsign;
    else if (fieldName.equalsIgnoreCase("showelement"))
      return showelement;
    else if (fieldName.equalsIgnoreCase("element_level") || fieldName.equals("elementLevel"))
      return elementLevel;
    else if (fieldName.equalsIgnoreCase("qty"))
      return qty;
    else if (fieldName.equalsIgnoreCase("qty_proy1") || fieldName.equals("qtyProy1"))
      return qtyProy1;
    else if (fieldName.equalsIgnoreCase("qty_proy2") || fieldName.equals("qtyProy2"))
      return qtyProy2;
    else if (fieldName.equalsIgnoreCase("qty_proy3") || fieldName.equals("qtyProy3"))
      return qtyProy3;
    else if (fieldName.equalsIgnoreCase("qty_proy4") || fieldName.equals("qtyProy4"))
      return qtyProy4;
    else if (fieldName.equalsIgnoreCase("qty_proy5") || fieldName.equals("qtyProy5"))
      return qtyProy5;
    else if (fieldName.equalsIgnoreCase("qty_proy6") || fieldName.equals("qtyProy6"))
      return qtyProy6;
    else if (fieldName.equalsIgnoreCase("qty_proy7") || fieldName.equals("qtyProy7"))
      return qtyProy7;
    else if (fieldName.equalsIgnoreCase("qty_proy8") || fieldName.equals("qtyProy8"))
      return qtyProy8;
    else if (fieldName.equalsIgnoreCase("qty_proy9") || fieldName.equals("qtyProy9"))
      return qtyProy9;
    else if (fieldName.equalsIgnoreCase("qty_proy10") || fieldName.equals("qtyProy10"))
      return qtyProy10;
    else if (fieldName.equalsIgnoreCase("qty_proy11") || fieldName.equals("qtyProy11"))
      return qtyProy11;
    else if (fieldName.equalsIgnoreCase("qty_proy12") || fieldName.equals("qtyProy12"))
      return qtyProy12;
    else if (fieldName.equalsIgnoreCase("qty_proy13") || fieldName.equals("qtyProy13"))
      return qtyProy13;
    else if (fieldName.equalsIgnoreCase("qty_proy14") || fieldName.equals("qtyProy14"))
      return qtyProy14;
    else if (fieldName.equalsIgnoreCase("qty_proy15") || fieldName.equals("qtyProy15"))
      return qtyProy15;
    else if (fieldName.equalsIgnoreCase("qty_proy16") || fieldName.equals("qtyProy16"))
      return qtyProy16;
    else if (fieldName.equalsIgnoreCase("qty_proy17") || fieldName.equals("qtyProy17"))
      return qtyProy17;
    else if (fieldName.equalsIgnoreCase("qty_proy18") || fieldName.equals("qtyProy18"))
      return qtyProy18;
    else if (fieldName.equalsIgnoreCase("qty_proy19") || fieldName.equals("qtyProy19"))
      return qtyProy19;
    else if (fieldName.equalsIgnoreCase("qty_proy20") || fieldName.equals("qtyProy20"))
      return qtyProy20;
    else if (fieldName.equalsIgnoreCase("qty_proy21") || fieldName.equals("qtyProy21"))
      return qtyProy21;
    else if (fieldName.equalsIgnoreCase("qty_proy22") || fieldName.equals("qtyProy22"))
      return qtyProy22;
    else if (fieldName.equalsIgnoreCase("qty_proy23") || fieldName.equals("qtyProy23"))
      return qtyProy23;
    else if (fieldName.equalsIgnoreCase("qty_proy24") || fieldName.equals("qtyProy24"))
      return qtyProy24;
    else if (fieldName.equalsIgnoreCase("qty_proy25") || fieldName.equals("qtyProy25"))
      return qtyProy25;
    else if (fieldName.equalsIgnoreCase("qty_proy26") || fieldName.equals("qtyProy26"))
      return qtyProy26;
    else if (fieldName.equalsIgnoreCase("qty_proy27") || fieldName.equals("qtyProy27"))
      return qtyProy27;
    else if (fieldName.equalsIgnoreCase("qty_proy28") || fieldName.equals("qtyProy28"))
      return qtyProy28;
    else if (fieldName.equalsIgnoreCase("qty_proy29") || fieldName.equals("qtyProy29"))
      return qtyProy29;
    else if (fieldName.equalsIgnoreCase("qty_proy30") || fieldName.equals("qtyProy30"))
      return qtyProy30;
    else if (fieldName.equalsIgnoreCase("qty_ref") || fieldName.equals("qtyRef"))
      return qtyRef;
    else if (fieldName.equalsIgnoreCase("qty_operation") || fieldName.equals("qtyOperation"))
      return qtyOperation;
    else if (fieldName.equalsIgnoreCase("qty_operation_ref") || fieldName.equals("qtyOperationRef"))
      return qtyOperationRef;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy1") || fieldName.equals("qtyOperationProy1"))
      return qtyOperationProy1;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy2") || fieldName.equals("qtyOperationProy2"))
      return qtyOperationProy2;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy3") || fieldName.equals("qtyOperationProy3"))
      return qtyOperationProy3;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy4") || fieldName.equals("qtyOperationProy4"))
      return qtyOperationProy4;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy5") || fieldName.equals("qtyOperationProy5"))
      return qtyOperationProy5;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy6") || fieldName.equals("qtyOperationProy6"))
      return qtyOperationProy6;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy7") || fieldName.equals("qtyOperationProy7"))
      return qtyOperationProy7;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy8") || fieldName.equals("qtyOperationProy8"))
      return qtyOperationProy8;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy9") || fieldName.equals("qtyOperationProy9"))
      return qtyOperationProy9;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy10") || fieldName.equals("qtyOperationProy10"))
      return qtyOperationProy10;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy11") || fieldName.equals("qtyOperationProy11"))
      return qtyOperationProy11;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy12") || fieldName.equals("qtyOperationProy12"))
      return qtyOperationProy12;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy13") || fieldName.equals("qtyOperationProy13"))
      return qtyOperationProy13;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy14") || fieldName.equals("qtyOperationProy14"))
      return qtyOperationProy14;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy15") || fieldName.equals("qtyOperationProy15"))
      return qtyOperationProy15;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy16") || fieldName.equals("qtyOperationProy16"))
      return qtyOperationProy16;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy17") || fieldName.equals("qtyOperationProy17"))
      return qtyOperationProy17;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy18") || fieldName.equals("qtyOperationProy18"))
      return qtyOperationProy18;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy19") || fieldName.equals("qtyOperationProy19"))
      return qtyOperationProy19;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy20") || fieldName.equals("qtyOperationProy20"))
      return qtyOperationProy20;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy21") || fieldName.equals("qtyOperationProy21"))
      return qtyOperationProy21;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy22") || fieldName.equals("qtyOperationProy22"))
      return qtyOperationProy22;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy23") || fieldName.equals("qtyOperationProy23"))
      return qtyOperationProy23;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy24") || fieldName.equals("qtyOperationProy24"))
      return qtyOperationProy24;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy25") || fieldName.equals("qtyOperationProy25"))
      return qtyOperationProy25;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy26") || fieldName.equals("qtyOperationProy26"))
      return qtyOperationProy26;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy27") || fieldName.equals("qtyOperationProy27"))
      return qtyOperationProy27;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy28") || fieldName.equals("qtyOperationProy28"))
      return qtyOperationProy28;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy29") || fieldName.equals("qtyOperationProy29"))
      return qtyOperationProy29;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy30") || fieldName.equals("qtyOperationProy30"))
      return qtyOperationProy30;
    else if (fieldName.equalsIgnoreCase("qtycredit"))
      return qtycredit;
    else if (fieldName.equalsIgnoreCase("qtycredit_ref") || fieldName.equals("qtycreditRef"))
      return qtycreditRef;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy1") || fieldName.equals("qtycreditProy1"))
      return qtycreditProy1;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy2") || fieldName.equals("qtycreditProy2"))
      return qtycreditProy2;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy3") || fieldName.equals("qtycreditProy3"))
      return qtycreditProy3;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy4") || fieldName.equals("qtycreditProy4"))
      return qtycreditProy4;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy5") || fieldName.equals("qtycreditProy5"))
      return qtycreditProy5;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy6") || fieldName.equals("qtycreditProy6"))
      return qtycreditProy6;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy7") || fieldName.equals("qtycreditProy7"))
      return qtycreditProy7;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy8") || fieldName.equals("qtycreditProy8"))
      return qtycreditProy8;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy9") || fieldName.equals("qtycreditProy9"))
      return qtycreditProy9;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy10") || fieldName.equals("qtycreditProy10"))
      return qtycreditProy10;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy11") || fieldName.equals("qtycreditProy11"))
      return qtycreditProy11;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy12") || fieldName.equals("qtycreditProy12"))
      return qtycreditProy12;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy13") || fieldName.equals("qtycreditProy13"))
      return qtycreditProy13;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy14") || fieldName.equals("qtycreditProy14"))
      return qtycreditProy14;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy15") || fieldName.equals("qtycreditProy15"))
      return qtycreditProy15;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy16") || fieldName.equals("qtycreditProy16"))
      return qtycreditProy16;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy17") || fieldName.equals("qtycreditProy17"))
      return qtycreditProy17;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy18") || fieldName.equals("qtycreditProy18"))
      return qtycreditProy18;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy19") || fieldName.equals("qtycreditProy19"))
      return qtycreditProy19;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy20") || fieldName.equals("qtycreditProy20"))
      return qtycreditProy20;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy21") || fieldName.equals("qtycreditProy21"))
      return qtycreditProy21;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy22") || fieldName.equals("qtycreditProy22"))
      return qtycreditProy22;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy23") || fieldName.equals("qtycreditProy23"))
      return qtycreditProy23;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy24") || fieldName.equals("qtycreditProy24"))
      return qtycreditProy24;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy25") || fieldName.equals("qtycreditProy25"))
      return qtycreditProy25;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy26") || fieldName.equals("qtycreditProy26"))
      return qtycreditProy26;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy27") || fieldName.equals("qtycreditProy27"))
      return qtycreditProy27;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy28") || fieldName.equals("qtycreditProy28"))
      return qtycreditProy28;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy29") || fieldName.equals("qtycreditProy29"))
      return qtycreditProy29;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy30") || fieldName.equals("qtycreditProy30"))
      return qtycreditProy30;
    else if (fieldName.equalsIgnoreCase("showvaluecond"))
      return showvaluecond;
    else if (fieldName.equalsIgnoreCase("elementlevel"))
      return elementlevel;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("calculated"))
      return calculated;
    else if (fieldName.equalsIgnoreCase("svcreset"))
      return svcreset;
    else if (fieldName.equalsIgnoreCase("svcresetref"))
      return svcresetref;
    else if (fieldName.equalsIgnoreCase("svcresetproy1"))
      return svcresetproy1;
    else if (fieldName.equalsIgnoreCase("svcresetproy2"))
      return svcresetproy2;
    else if (fieldName.equalsIgnoreCase("svcresetproy3"))
      return svcresetproy3;
    else if (fieldName.equalsIgnoreCase("svcresetproy4"))
      return svcresetproy4;
    else if (fieldName.equalsIgnoreCase("svcresetproy5"))
      return svcresetproy5;
    else if (fieldName.equalsIgnoreCase("svcresetproy6"))
      return svcresetproy6;
    else if (fieldName.equalsIgnoreCase("svcresetproy7"))
      return svcresetproy7;
    else if (fieldName.equalsIgnoreCase("svcresetproy8"))
      return svcresetproy8;
    else if (fieldName.equalsIgnoreCase("svcresetproy9"))
      return svcresetproy9;
    else if (fieldName.equalsIgnoreCase("svcresetproy10"))
      return svcresetproy10;
    else if (fieldName.equalsIgnoreCase("svcresetproy11"))
      return svcresetproy11;
    else if (fieldName.equalsIgnoreCase("svcresetproy12"))
      return svcresetproy12;
    else if (fieldName.equalsIgnoreCase("svcresetproy13"))
      return svcresetproy13;
    else if (fieldName.equalsIgnoreCase("svcresetproy14"))
      return svcresetproy14;
    else if (fieldName.equalsIgnoreCase("svcresetproy15"))
      return svcresetproy15;
    else if (fieldName.equalsIgnoreCase("svcresetproy16"))
      return svcresetproy16;
    else if (fieldName.equalsIgnoreCase("svcresetproy17"))
      return svcresetproy17;
    else if (fieldName.equalsIgnoreCase("svcresetproy18"))
      return svcresetproy18;
    else if (fieldName.equalsIgnoreCase("svcresetproy19"))
      return svcresetproy19;
    else if (fieldName.equalsIgnoreCase("svcresetproy20"))
      return svcresetproy20;
    else if (fieldName.equalsIgnoreCase("svcresetproy21"))
      return svcresetproy21;
    else if (fieldName.equalsIgnoreCase("svcresetproy22"))
      return svcresetproy22;
    else if (fieldName.equalsIgnoreCase("svcresetproy23"))
      return svcresetproy23;
    else if (fieldName.equalsIgnoreCase("svcresetproy24"))
      return svcresetproy24;
    else if (fieldName.equalsIgnoreCase("svcresetproy25"))
      return svcresetproy25;
    else if (fieldName.equalsIgnoreCase("svcresetproy26"))
      return svcresetproy26;
    else if (fieldName.equalsIgnoreCase("svcresetproy27"))
      return svcresetproy27;
    else if (fieldName.equalsIgnoreCase("svcresetproy28"))
      return svcresetproy28;
    else if (fieldName.equalsIgnoreCase("svcresetproy29"))
      return svcresetproy29;
    else if (fieldName.equalsIgnoreCase("svcresetproy30"))
      return svcresetproy30;
    else if (fieldName.equalsIgnoreCase("isalwaysshown"))
      return isalwaysshown;
    else if (fieldName.equalsIgnoreCase("sign"))
      return sign;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static AccountTreeData[] select(ConnectionProvider connectionProvider, String conCodigo, String adTreeId)    throws ServletException {
    return select(connectionProvider, conCodigo, adTreeId, 0, 0);
  }

  public static AccountTreeData[] select(ConnectionProvider connectionProvider, String conCodigo, String adTreeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT tn.Node_ID,tn.Parent_ID,tn.SeqNo, m.C_ElementValue_ID AS ID, " +
      "        ((CASE ? WHEN 'Y' THEN TO_CHAR(m.Value) || ' - ' ELSE '' END) || m.Name) AS NAME,m.Description, m.IsSummary, " +
      "        m.ACCOUNTSIGN, m.ShowElement, 0 as element_level, 0 as qty, " +
      "        0 AS qty_proy1, " +
      "        0 AS qty_proy2, " +
      "        0 AS qty_proy3, " +
      "        0 AS qty_proy4, " +
      "        0 AS qty_proy5, " +
      "        0 AS qty_proy6, " +
      "        0 AS qty_proy7, " +
      "        0 AS qty_proy8, " +
      "        0 AS qty_proy9," +
      "        0 AS qty_proy10," +
      "        0 AS qty_proy11, " +
      "        0 AS qty_proy12," +
      "        0 AS qty_proy13, " +
      "        0 AS qty_proy14," +
      "        0 AS qty_proy15," +
      "        0 AS qty_proy16, " +
      "        0 AS qty_proy17," +
      "        0 AS qty_proy18," +
      "        0 AS qty_proy19, " +
      "        0 AS qty_proy20," +
      "        0 AS qty_proy21, " +
      "        0 AS qty_proy22," +
      "        0 AS qty_proy23," +
      "        0 AS qty_proy24, " +
      "        0 AS qty_proy25," +
      "        0 AS qty_proy26, " +
      "        0 AS qty_proy27, " +
      "        0 AS qty_proy28, " +
      "        0 AS qty_proy29," +
      "        0 AS qty_proy30," +
      "        0 as qty_ref, 0 as qty_operation, 0 as qty_operation_ref, " +
      "        0 as qty_operation_proy1," +
      "        0 as qty_operation_proy2," +
      "        0 as qty_operation_proy3," +
      "        0 as qty_operation_proy4," +
      "        0 as qty_operation_proy5," +
      "        0 as qty_operation_proy6," +
      "        0 as qty_operation_proy7," +
      "        0 as qty_operation_proy8," +
      "        0 as qty_operation_proy9," +
      "        0 as qty_operation_proy10," +
      "        0 as qty_operation_proy11," +
      "        0 as qty_operation_proy12," +
      "        0 as qty_operation_proy13," +
      "        0 as qty_operation_proy14," +
      "        0 as qty_operation_proy15," +
      "        0 as qty_operation_proy16," +
      "        0 as qty_operation_proy17," +
      "        0 as qty_operation_proy18," +
      "        0 as qty_operation_proy19," +
      "        0 as qty_operation_proy20," +
      "        0 as qty_operation_proy21," +
      "        0 as qty_operation_proy22," +
      "        0 as qty_operation_proy23," +
      "        0 as qty_operation_proy24," +
      "        0 as qty_operation_proy25," +
      "        0 as qty_operation_proy26," +
      "        0 as qty_operation_proy27," +
      "        0 as qty_operation_proy28," +
      "        0 as qty_operation_proy29," +
      "        0 as qty_operation_proy30," +
      "        0 as QTYCREDIT, 0 as QTYCREDIT_REF, " +
      "        0 as QTYCREDIT_PROY1," +
      "        0 as QTYCREDIT_PROY2," +
      "        0 as QTYCREDIT_PROY3," +
      "        0 as QTYCREDIT_PROY4," +
      "        0 as QTYCREDIT_PROY5," +
      "        0 as QTYCREDIT_PROY6," +
      "        0 as QTYCREDIT_PROY7," +
      "        0 as QTYCREDIT_PROY8," +
      "        0 as QTYCREDIT_PROY9," +
      "        0 as QTYCREDIT_PROY10," +
      "        0 as QTYCREDIT_PROY11," +
      "        0 as QTYCREDIT_PROY12," +
      "        0 as QTYCREDIT_PROY13," +
      "        0 as QTYCREDIT_PROY14," +
      "        0 as QTYCREDIT_PROY15," +
      "        0 as QTYCREDIT_PROY16," +
      "        0 as QTYCREDIT_PROY17," +
      "        0 as QTYCREDIT_PROY18," +
      "        0 as QTYCREDIT_PROY19," +
      "        0 as QTYCREDIT_PROY20," +
      "        0 as QTYCREDIT_PROY21," +
      "        0 as QTYCREDIT_PROY22," +
      "        0 as QTYCREDIT_PROY23," +
      "        0 as QTYCREDIT_PROY24," +
      "        0 as QTYCREDIT_PROY25," +
      "        0 as QTYCREDIT_PROY26," +
      "        0 as QTYCREDIT_PROY27," +
      "        0 as QTYCREDIT_PROY28," +
      "        0 as QTYCREDIT_PROY29," +
      "        0 as QTYCREDIT_PROY30," +
      "        m.ShowValueCond, m.ElementLevel, m.Value, " +
      "        'N' AS CALCULATED, 'N' AS SVCRESET, 'N' AS SVCRESETREF, " +
      "        'N' AS SVCRESETPROY1," +
      "        'N' AS SVCRESETPROY2," +
      "        'N' AS SVCRESETPROY3," +
      "        'N' AS SVCRESETPROY4, " +
      "        'N' AS SVCRESETPROY5," +
      "        'N' AS SVCRESETPROY6," +
      "        'N' AS SVCRESETPROY7," +
      "        'N' AS SVCRESETPROY8," +
      "        'N' AS SVCRESETPROY9," +
      "        'N' AS SVCRESETPROY10," +
      "        'N' AS SVCRESETPROY11," +
      "        'N' AS SVCRESETPROY12," +
      "        'N' AS SVCRESETPROY13," +
      "        'N' AS SVCRESETPROY14, " +
      "        'N' AS SVCRESETPROY15," +
      "        'N' AS SVCRESETPROY16," +
      "        'N' AS SVCRESETPROY17," +
      "        'N' AS SVCRESETPROY18," +
      "        'N' AS SVCRESETPROY19," +
      "        'N' AS SVCRESETPROY20," +
      "        'N' AS SVCRESETPROY21," +
      "        'N' AS SVCRESETPROY22," +
      "        'N' AS SVCRESETPROY23," +
      "        'N' AS SVCRESETPROY24, " +
      "        'N' AS SVCRESETPROY25," +
      "        'N' AS SVCRESETPROY26," +
      "        'N' AS SVCRESETPROY27," +
      "        'N' AS SVCRESETPROY28," +
      "        'N' AS SVCRESETPROY29," +
      "        'N' AS SVCRESETPROY30," +
      "        m.isalwaysshown, '' as sign" +
      "        FROM AD_TreeNode tn, C_ElementValue m" +
      "        WHERE tn.IsActive='Y' " +
      "        AND tn.Node_ID = m.C_ElementValue_ID " +
      "        AND tn.AD_Tree_ID = ? " +
      "        ORDER BY COALESCE(tn.Parent_ID, '-1'), tn.SeqNo";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, conCodigo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.parentId = UtilSql.getValue(result, "parent_id");
        objectAccountTreeData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.name = UtilSql.getValue(result, "name");
        objectAccountTreeData.description = UtilSql.getValue(result, "description");
        objectAccountTreeData.issummary = UtilSql.getValue(result, "issummary");
        objectAccountTreeData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeData.showelement = UtilSql.getValue(result, "showelement");
        objectAccountTreeData.elementLevel = UtilSql.getValue(result, "element_level");
        objectAccountTreeData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeData.qtyProy1 = UtilSql.getValue(result, "qty_proy1");
        objectAccountTreeData.qtyProy2 = UtilSql.getValue(result, "qty_proy2");
        objectAccountTreeData.qtyProy3 = UtilSql.getValue(result, "qty_proy3");
        objectAccountTreeData.qtyProy4 = UtilSql.getValue(result, "qty_proy4");
        objectAccountTreeData.qtyProy5 = UtilSql.getValue(result, "qty_proy5");
        objectAccountTreeData.qtyProy6 = UtilSql.getValue(result, "qty_proy6");
        objectAccountTreeData.qtyProy7 = UtilSql.getValue(result, "qty_proy7");
        objectAccountTreeData.qtyProy8 = UtilSql.getValue(result, "qty_proy8");
        objectAccountTreeData.qtyProy9 = UtilSql.getValue(result, "qty_proy9");
        objectAccountTreeData.qtyProy10 = UtilSql.getValue(result, "qty_proy10");
        objectAccountTreeData.qtyProy11 = UtilSql.getValue(result, "qty_proy11");
        objectAccountTreeData.qtyProy12 = UtilSql.getValue(result, "qty_proy12");
        objectAccountTreeData.qtyProy13 = UtilSql.getValue(result, "qty_proy13");
        objectAccountTreeData.qtyProy14 = UtilSql.getValue(result, "qty_proy14");
        objectAccountTreeData.qtyProy15 = UtilSql.getValue(result, "qty_proy15");
        objectAccountTreeData.qtyProy16 = UtilSql.getValue(result, "qty_proy16");
        objectAccountTreeData.qtyProy17 = UtilSql.getValue(result, "qty_proy17");
        objectAccountTreeData.qtyProy18 = UtilSql.getValue(result, "qty_proy18");
        objectAccountTreeData.qtyProy19 = UtilSql.getValue(result, "qty_proy19");
        objectAccountTreeData.qtyProy20 = UtilSql.getValue(result, "qty_proy20");
        objectAccountTreeData.qtyProy21 = UtilSql.getValue(result, "qty_proy21");
        objectAccountTreeData.qtyProy22 = UtilSql.getValue(result, "qty_proy22");
        objectAccountTreeData.qtyProy23 = UtilSql.getValue(result, "qty_proy23");
        objectAccountTreeData.qtyProy24 = UtilSql.getValue(result, "qty_proy24");
        objectAccountTreeData.qtyProy25 = UtilSql.getValue(result, "qty_proy25");
        objectAccountTreeData.qtyProy26 = UtilSql.getValue(result, "qty_proy26");
        objectAccountTreeData.qtyProy27 = UtilSql.getValue(result, "qty_proy27");
        objectAccountTreeData.qtyProy28 = UtilSql.getValue(result, "qty_proy28");
        objectAccountTreeData.qtyProy29 = UtilSql.getValue(result, "qty_proy29");
        objectAccountTreeData.qtyProy30 = UtilSql.getValue(result, "qty_proy30");
        objectAccountTreeData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeData.qtyOperation = UtilSql.getValue(result, "qty_operation");
        objectAccountTreeData.qtyOperationRef = UtilSql.getValue(result, "qty_operation_ref");
        objectAccountTreeData.qtyOperationProy1 = UtilSql.getValue(result, "qty_operation_proy1");
        objectAccountTreeData.qtyOperationProy2 = UtilSql.getValue(result, "qty_operation_proy2");
        objectAccountTreeData.qtyOperationProy3 = UtilSql.getValue(result, "qty_operation_proy3");
        objectAccountTreeData.qtyOperationProy4 = UtilSql.getValue(result, "qty_operation_proy4");
        objectAccountTreeData.qtyOperationProy5 = UtilSql.getValue(result, "qty_operation_proy5");
        objectAccountTreeData.qtyOperationProy6 = UtilSql.getValue(result, "qty_operation_proy6");
        objectAccountTreeData.qtyOperationProy7 = UtilSql.getValue(result, "qty_operation_proy7");
        objectAccountTreeData.qtyOperationProy8 = UtilSql.getValue(result, "qty_operation_proy8");
        objectAccountTreeData.qtyOperationProy9 = UtilSql.getValue(result, "qty_operation_proy9");
        objectAccountTreeData.qtyOperationProy10 = UtilSql.getValue(result, "qty_operation_proy10");
        objectAccountTreeData.qtyOperationProy11 = UtilSql.getValue(result, "qty_operation_proy11");
        objectAccountTreeData.qtyOperationProy12 = UtilSql.getValue(result, "qty_operation_proy12");
        objectAccountTreeData.qtyOperationProy13 = UtilSql.getValue(result, "qty_operation_proy13");
        objectAccountTreeData.qtyOperationProy14 = UtilSql.getValue(result, "qty_operation_proy14");
        objectAccountTreeData.qtyOperationProy15 = UtilSql.getValue(result, "qty_operation_proy15");
        objectAccountTreeData.qtyOperationProy16 = UtilSql.getValue(result, "qty_operation_proy16");
        objectAccountTreeData.qtyOperationProy17 = UtilSql.getValue(result, "qty_operation_proy17");
        objectAccountTreeData.qtyOperationProy18 = UtilSql.getValue(result, "qty_operation_proy18");
        objectAccountTreeData.qtyOperationProy19 = UtilSql.getValue(result, "qty_operation_proy19");
        objectAccountTreeData.qtyOperationProy20 = UtilSql.getValue(result, "qty_operation_proy20");
        objectAccountTreeData.qtyOperationProy21 = UtilSql.getValue(result, "qty_operation_proy21");
        objectAccountTreeData.qtyOperationProy22 = UtilSql.getValue(result, "qty_operation_proy22");
        objectAccountTreeData.qtyOperationProy23 = UtilSql.getValue(result, "qty_operation_proy23");
        objectAccountTreeData.qtyOperationProy24 = UtilSql.getValue(result, "qty_operation_proy24");
        objectAccountTreeData.qtyOperationProy25 = UtilSql.getValue(result, "qty_operation_proy25");
        objectAccountTreeData.qtyOperationProy26 = UtilSql.getValue(result, "qty_operation_proy26");
        objectAccountTreeData.qtyOperationProy27 = UtilSql.getValue(result, "qty_operation_proy27");
        objectAccountTreeData.qtyOperationProy28 = UtilSql.getValue(result, "qty_operation_proy28");
        objectAccountTreeData.qtyOperationProy29 = UtilSql.getValue(result, "qty_operation_proy29");
        objectAccountTreeData.qtyOperationProy30 = UtilSql.getValue(result, "qty_operation_proy30");
        objectAccountTreeData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeData.qtycreditProy1 = UtilSql.getValue(result, "qtycredit_proy1");
        objectAccountTreeData.qtycreditProy2 = UtilSql.getValue(result, "qtycredit_proy2");
        objectAccountTreeData.qtycreditProy3 = UtilSql.getValue(result, "qtycredit_proy3");
        objectAccountTreeData.qtycreditProy4 = UtilSql.getValue(result, "qtycredit_proy4");
        objectAccountTreeData.qtycreditProy5 = UtilSql.getValue(result, "qtycredit_proy5");
        objectAccountTreeData.qtycreditProy6 = UtilSql.getValue(result, "qtycredit_proy6");
        objectAccountTreeData.qtycreditProy7 = UtilSql.getValue(result, "qtycredit_proy7");
        objectAccountTreeData.qtycreditProy8 = UtilSql.getValue(result, "qtycredit_proy8");
        objectAccountTreeData.qtycreditProy9 = UtilSql.getValue(result, "qtycredit_proy9");
        objectAccountTreeData.qtycreditProy10 = UtilSql.getValue(result, "qtycredit_proy10");
        objectAccountTreeData.qtycreditProy11 = UtilSql.getValue(result, "qtycredit_proy11");
        objectAccountTreeData.qtycreditProy12 = UtilSql.getValue(result, "qtycredit_proy12");
        objectAccountTreeData.qtycreditProy13 = UtilSql.getValue(result, "qtycredit_proy13");
        objectAccountTreeData.qtycreditProy14 = UtilSql.getValue(result, "qtycredit_proy14");
        objectAccountTreeData.qtycreditProy15 = UtilSql.getValue(result, "qtycredit_proy15");
        objectAccountTreeData.qtycreditProy16 = UtilSql.getValue(result, "qtycredit_proy16");
        objectAccountTreeData.qtycreditProy17 = UtilSql.getValue(result, "qtycredit_proy17");
        objectAccountTreeData.qtycreditProy18 = UtilSql.getValue(result, "qtycredit_proy18");
        objectAccountTreeData.qtycreditProy19 = UtilSql.getValue(result, "qtycredit_proy19");
        objectAccountTreeData.qtycreditProy20 = UtilSql.getValue(result, "qtycredit_proy20");
        objectAccountTreeData.qtycreditProy21 = UtilSql.getValue(result, "qtycredit_proy21");
        objectAccountTreeData.qtycreditProy22 = UtilSql.getValue(result, "qtycredit_proy22");
        objectAccountTreeData.qtycreditProy23 = UtilSql.getValue(result, "qtycredit_proy23");
        objectAccountTreeData.qtycreditProy24 = UtilSql.getValue(result, "qtycredit_proy24");
        objectAccountTreeData.qtycreditProy25 = UtilSql.getValue(result, "qtycredit_proy25");
        objectAccountTreeData.qtycreditProy26 = UtilSql.getValue(result, "qtycredit_proy26");
        objectAccountTreeData.qtycreditProy27 = UtilSql.getValue(result, "qtycredit_proy27");
        objectAccountTreeData.qtycreditProy28 = UtilSql.getValue(result, "qtycredit_proy28");
        objectAccountTreeData.qtycreditProy29 = UtilSql.getValue(result, "qtycredit_proy29");
        objectAccountTreeData.qtycreditProy30 = UtilSql.getValue(result, "qtycredit_proy30");
        objectAccountTreeData.showvaluecond = UtilSql.getValue(result, "showvaluecond");
        objectAccountTreeData.elementlevel = UtilSql.getValue(result, "elementlevel");
        objectAccountTreeData.value = UtilSql.getValue(result, "value");
        objectAccountTreeData.calculated = UtilSql.getValue(result, "calculated");
        objectAccountTreeData.svcreset = UtilSql.getValue(result, "svcreset");
        objectAccountTreeData.svcresetref = UtilSql.getValue(result, "svcresetref");
        objectAccountTreeData.svcresetproy1 = UtilSql.getValue(result, "svcresetproy1");
        objectAccountTreeData.svcresetproy2 = UtilSql.getValue(result, "svcresetproy2");
        objectAccountTreeData.svcresetproy3 = UtilSql.getValue(result, "svcresetproy3");
        objectAccountTreeData.svcresetproy4 = UtilSql.getValue(result, "svcresetproy4");
        objectAccountTreeData.svcresetproy5 = UtilSql.getValue(result, "svcresetproy5");
        objectAccountTreeData.svcresetproy6 = UtilSql.getValue(result, "svcresetproy6");
        objectAccountTreeData.svcresetproy7 = UtilSql.getValue(result, "svcresetproy7");
        objectAccountTreeData.svcresetproy8 = UtilSql.getValue(result, "svcresetproy8");
        objectAccountTreeData.svcresetproy9 = UtilSql.getValue(result, "svcresetproy9");
        objectAccountTreeData.svcresetproy10 = UtilSql.getValue(result, "svcresetproy10");
        objectAccountTreeData.svcresetproy11 = UtilSql.getValue(result, "svcresetproy11");
        objectAccountTreeData.svcresetproy12 = UtilSql.getValue(result, "svcresetproy12");
        objectAccountTreeData.svcresetproy13 = UtilSql.getValue(result, "svcresetproy13");
        objectAccountTreeData.svcresetproy14 = UtilSql.getValue(result, "svcresetproy14");
        objectAccountTreeData.svcresetproy15 = UtilSql.getValue(result, "svcresetproy15");
        objectAccountTreeData.svcresetproy16 = UtilSql.getValue(result, "svcresetproy16");
        objectAccountTreeData.svcresetproy17 = UtilSql.getValue(result, "svcresetproy17");
        objectAccountTreeData.svcresetproy18 = UtilSql.getValue(result, "svcresetproy18");
        objectAccountTreeData.svcresetproy19 = UtilSql.getValue(result, "svcresetproy19");
        objectAccountTreeData.svcresetproy20 = UtilSql.getValue(result, "svcresetproy20");
        objectAccountTreeData.svcresetproy21 = UtilSql.getValue(result, "svcresetproy21");
        objectAccountTreeData.svcresetproy22 = UtilSql.getValue(result, "svcresetproy22");
        objectAccountTreeData.svcresetproy23 = UtilSql.getValue(result, "svcresetproy23");
        objectAccountTreeData.svcresetproy24 = UtilSql.getValue(result, "svcresetproy24");
        objectAccountTreeData.svcresetproy25 = UtilSql.getValue(result, "svcresetproy25");
        objectAccountTreeData.svcresetproy26 = UtilSql.getValue(result, "svcresetproy26");
        objectAccountTreeData.svcresetproy27 = UtilSql.getValue(result, "svcresetproy27");
        objectAccountTreeData.svcresetproy28 = UtilSql.getValue(result, "svcresetproy28");
        objectAccountTreeData.svcresetproy29 = UtilSql.getValue(result, "svcresetproy29");
        objectAccountTreeData.svcresetproy30 = UtilSql.getValue(result, "svcresetproy30");
        objectAccountTreeData.isalwaysshown = UtilSql.getValue(result, "isalwaysshown");
        objectAccountTreeData.sign = UtilSql.getValue(result, "sign");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectTrl(ConnectionProvider connectionProvider, String conCodigo, String adLanguage, String adTreeId)    throws ServletException {
    return selectTrl(connectionProvider, conCodigo, adLanguage, adTreeId, 0, 0);
  }

  public static AccountTreeData[] selectTrl(ConnectionProvider connectionProvider, String conCodigo, String adLanguage, String adTreeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT tn.Node_ID,tn.Parent_ID,tn.SeqNo, m.C_ElementValue_ID AS ID, ((CASE ? WHEN 'Y' THEN TO_CHAR(m.Value) || ' - ' ELSE '' END) || COALESCE(mt.Name, m.Name)) as Name, " +
      "        COALESCE(mt.Description, m.Description) as description ,m.IsSummary, m.ACCOUNTSIGN, " +
      "        m.ShowElement, 0 as element_level, 0 as qty, " +
      "        0 AS qty_proy1, " +
      "        0 AS qty_proy2, " +
      "        0 AS qty_proy3, " +
      "        0 AS qty_proy4, " +
      "        0 AS qty_proy5, " +
      "        0 AS qty_proy6, " +
      "        0 AS qty_proy7, " +
      "        0 AS qty_proy8, " +
      "        0 AS qty_proy9," +
      "        0 AS qty_proy10," +
      "        0 AS qty_proy11, " +
      "        0 AS qty_proy12," +
      "        0 AS qty_proy13, " +
      "        0 AS qty_proy14, " +
      "        0 AS qty_proy15, " +
      "        0 AS qty_proy16, " +
      "        0 AS qty_proy17, " +
      "        0 AS qty_proy18, " +
      "        0 AS qty_proy19," +
      "        0 AS qty_proy20," +
      "        0 AS qty_proy21, " +
      "        0 AS qty_proy22," +
      "        0 AS qty_proy23, " +
      "        0 AS qty_proy24, " +
      "        0 AS qty_proy25, " +
      "        0 AS qty_proy26, " +
      "        0 AS qty_proy27, " +
      "        0 AS qty_proy28, " +
      "        0 AS qty_proy29," +
      "        0 AS qty_proy30," +
      "        0 as qty_ref, 0 as qty_operation, 0 as qty_operation_ref," +
      "        0 as qty_operation_proy1," +
      "        0 as qty_operation_proy2," +
      "        0 as qty_operation_proy3," +
      "        0 as qty_operation_proy4, " +
      "        0 as qty_operation_proy5," +
      "        0 as qty_operation_proy6," +
      "        0 as qty_operation_proy7," +
      "        0 as qty_operation_proy8, " +
      "        0 as qty_operation_proy9," +
      "        0 as qty_operation_proy10," +
      "        0 as qty_operation_proy11," +
      "        0 as qty_operation_proy12, " +
      "        0 as qty_operation_proy13," +
      "        0 as qty_operation_proy14, " +
      "        0 as qty_operation_proy15," +
      "        0 as qty_operation_proy16," +
      "        0 as qty_operation_proy17," +
      "        0 as qty_operation_proy18, " +
      "        0 as qty_operation_proy19," +
      "        0 as qty_operation_proy20, " +
      "        0 as qty_operation_proy21," +
      "        0 as qty_operation_proy22," +
      "        0 as qty_operation_proy23," +
      "        0 as qty_operation_proy24, " +
      "        0 as qty_operation_proy25," +
      "        0 as qty_operation_proy26," +
      "        0 as qty_operation_proy27," +
      "        0 as qty_operation_proy28, " +
      "        0 as qty_operation_proy29," +
      "        0 as qty_operation_proy30," +
      "        m.ShowValueCond, m.ElementLevel, m.Value, 'N' AS CALCULATED, 'N' AS SVCRESET, 'N' AS SVCRESETREF, " +
      "        'N' AS SVCRESETPROY1," +
      "        'N' AS SVCRESETPROY2," +
      "        'N' AS SVCRESETPROY3," +
      "        'N' AS SVCRESETPROY4, " +
      "        'N' AS SVCRESETPROY5," +
      "        'N' AS SVCRESETPROY6," +
      "        'N' AS SVCRESETPROY7," +
      "        'N' AS SVCRESETPROY8," +
      "        'N' AS SVCRESETPROY9," +
      "        'N' AS SVCRESETPROY10," +
      "        'N' AS SVCRESETPROY11," +
      "        'N' AS SVCRESETPROY12," +
      "        'N' AS SVCRESETPROY13," +
      "        'N' AS SVCRESETPROY14, " +
      "        'N' AS SVCRESETPROY15," +
      "        'N' AS SVCRESETPROY16," +
      "        'N' AS SVCRESETPROY17," +
      "        'N' AS SVCRESETPROY18," +
      "        'N' AS SVCRESETPROY19," +
      "        'N' AS SVCRESETPROY20," +
      "        'N' AS SVCRESETPROY21," +
      "        'N' AS SVCRESETPROY22," +
      "        'N' AS SVCRESETPROY23," +
      "        'N' AS SVCRESETPROY24, " +
      "        'N' AS SVCRESETPROY25," +
      "        'N' AS SVCRESETPROY26," +
      "        'N' AS SVCRESETPROY27," +
      "        'N' AS SVCRESETPROY28," +
      "        'N' AS SVCRESETPROY29," +
      "        'N' AS SVCRESETPROY30," +
      "        m.isalwaysshown" +
      "        FROM C_ElementValue m left join C_ElementValue_Trl mt on m.C_ElementValue_ID = mt.C_ElementValue_ID " +
      "                                                              and mt.AD_Language = ? ," +
      "              AD_TreeNode tn" +
      "        WHERE tn.IsActive='Y' " +
      "        AND tn.Node_ID = m.C_ElementValue_ID " +
      "        AND tn.AD_Tree_ID = ? " +
      "        ORDER BY COALESCE(tn.Parent_ID, '-1'), tn.SeqNo ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, conCodigo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.parentId = UtilSql.getValue(result, "parent_id");
        objectAccountTreeData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.name = UtilSql.getValue(result, "name");
        objectAccountTreeData.description = UtilSql.getValue(result, "description");
        objectAccountTreeData.issummary = UtilSql.getValue(result, "issummary");
        objectAccountTreeData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeData.showelement = UtilSql.getValue(result, "showelement");
        objectAccountTreeData.elementLevel = UtilSql.getValue(result, "element_level");
        objectAccountTreeData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeData.qtyProy1 = UtilSql.getValue(result, "qty_proy1");
        objectAccountTreeData.qtyProy2 = UtilSql.getValue(result, "qty_proy2");
        objectAccountTreeData.qtyProy3 = UtilSql.getValue(result, "qty_proy3");
        objectAccountTreeData.qtyProy4 = UtilSql.getValue(result, "qty_proy4");
        objectAccountTreeData.qtyProy5 = UtilSql.getValue(result, "qty_proy5");
        objectAccountTreeData.qtyProy6 = UtilSql.getValue(result, "qty_proy6");
        objectAccountTreeData.qtyProy7 = UtilSql.getValue(result, "qty_proy7");
        objectAccountTreeData.qtyProy8 = UtilSql.getValue(result, "qty_proy8");
        objectAccountTreeData.qtyProy9 = UtilSql.getValue(result, "qty_proy9");
        objectAccountTreeData.qtyProy10 = UtilSql.getValue(result, "qty_proy10");
        objectAccountTreeData.qtyProy11 = UtilSql.getValue(result, "qty_proy11");
        objectAccountTreeData.qtyProy12 = UtilSql.getValue(result, "qty_proy12");
        objectAccountTreeData.qtyProy13 = UtilSql.getValue(result, "qty_proy13");
        objectAccountTreeData.qtyProy14 = UtilSql.getValue(result, "qty_proy14");
        objectAccountTreeData.qtyProy15 = UtilSql.getValue(result, "qty_proy15");
        objectAccountTreeData.qtyProy16 = UtilSql.getValue(result, "qty_proy16");
        objectAccountTreeData.qtyProy17 = UtilSql.getValue(result, "qty_proy17");
        objectAccountTreeData.qtyProy18 = UtilSql.getValue(result, "qty_proy18");
        objectAccountTreeData.qtyProy19 = UtilSql.getValue(result, "qty_proy19");
        objectAccountTreeData.qtyProy20 = UtilSql.getValue(result, "qty_proy20");
        objectAccountTreeData.qtyProy21 = UtilSql.getValue(result, "qty_proy21");
        objectAccountTreeData.qtyProy22 = UtilSql.getValue(result, "qty_proy22");
        objectAccountTreeData.qtyProy23 = UtilSql.getValue(result, "qty_proy23");
        objectAccountTreeData.qtyProy24 = UtilSql.getValue(result, "qty_proy24");
        objectAccountTreeData.qtyProy25 = UtilSql.getValue(result, "qty_proy25");
        objectAccountTreeData.qtyProy26 = UtilSql.getValue(result, "qty_proy26");
        objectAccountTreeData.qtyProy27 = UtilSql.getValue(result, "qty_proy27");
        objectAccountTreeData.qtyProy28 = UtilSql.getValue(result, "qty_proy28");
        objectAccountTreeData.qtyProy29 = UtilSql.getValue(result, "qty_proy29");
        objectAccountTreeData.qtyProy30 = UtilSql.getValue(result, "qty_proy30");
        objectAccountTreeData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeData.qtyOperation = UtilSql.getValue(result, "qty_operation");
        objectAccountTreeData.qtyOperationRef = UtilSql.getValue(result, "qty_operation_ref");
        objectAccountTreeData.qtyOperationProy1 = UtilSql.getValue(result, "qty_operation_proy1");
        objectAccountTreeData.qtyOperationProy2 = UtilSql.getValue(result, "qty_operation_proy2");
        objectAccountTreeData.qtyOperationProy3 = UtilSql.getValue(result, "qty_operation_proy3");
        objectAccountTreeData.qtyOperationProy4 = UtilSql.getValue(result, "qty_operation_proy4");
        objectAccountTreeData.qtyOperationProy5 = UtilSql.getValue(result, "qty_operation_proy5");
        objectAccountTreeData.qtyOperationProy6 = UtilSql.getValue(result, "qty_operation_proy6");
        objectAccountTreeData.qtyOperationProy7 = UtilSql.getValue(result, "qty_operation_proy7");
        objectAccountTreeData.qtyOperationProy8 = UtilSql.getValue(result, "qty_operation_proy8");
        objectAccountTreeData.qtyOperationProy9 = UtilSql.getValue(result, "qty_operation_proy9");
        objectAccountTreeData.qtyOperationProy10 = UtilSql.getValue(result, "qty_operation_proy10");
        objectAccountTreeData.qtyOperationProy11 = UtilSql.getValue(result, "qty_operation_proy11");
        objectAccountTreeData.qtyOperationProy12 = UtilSql.getValue(result, "qty_operation_proy12");
        objectAccountTreeData.qtyOperationProy13 = UtilSql.getValue(result, "qty_operation_proy13");
        objectAccountTreeData.qtyOperationProy14 = UtilSql.getValue(result, "qty_operation_proy14");
        objectAccountTreeData.qtyOperationProy15 = UtilSql.getValue(result, "qty_operation_proy15");
        objectAccountTreeData.qtyOperationProy16 = UtilSql.getValue(result, "qty_operation_proy16");
        objectAccountTreeData.qtyOperationProy17 = UtilSql.getValue(result, "qty_operation_proy17");
        objectAccountTreeData.qtyOperationProy18 = UtilSql.getValue(result, "qty_operation_proy18");
        objectAccountTreeData.qtyOperationProy19 = UtilSql.getValue(result, "qty_operation_proy19");
        objectAccountTreeData.qtyOperationProy20 = UtilSql.getValue(result, "qty_operation_proy20");
        objectAccountTreeData.qtyOperationProy21 = UtilSql.getValue(result, "qty_operation_proy21");
        objectAccountTreeData.qtyOperationProy22 = UtilSql.getValue(result, "qty_operation_proy22");
        objectAccountTreeData.qtyOperationProy23 = UtilSql.getValue(result, "qty_operation_proy23");
        objectAccountTreeData.qtyOperationProy24 = UtilSql.getValue(result, "qty_operation_proy24");
        objectAccountTreeData.qtyOperationProy25 = UtilSql.getValue(result, "qty_operation_proy25");
        objectAccountTreeData.qtyOperationProy26 = UtilSql.getValue(result, "qty_operation_proy26");
        objectAccountTreeData.qtyOperationProy27 = UtilSql.getValue(result, "qty_operation_proy27");
        objectAccountTreeData.qtyOperationProy28 = UtilSql.getValue(result, "qty_operation_proy28");
        objectAccountTreeData.qtyOperationProy29 = UtilSql.getValue(result, "qty_operation_proy29");
        objectAccountTreeData.qtyOperationProy30 = UtilSql.getValue(result, "qty_operation_proy30");
        objectAccountTreeData.showvaluecond = UtilSql.getValue(result, "showvaluecond");
        objectAccountTreeData.elementlevel = UtilSql.getValue(result, "elementlevel");
        objectAccountTreeData.value = UtilSql.getValue(result, "value");
        objectAccountTreeData.calculated = UtilSql.getValue(result, "calculated");
        objectAccountTreeData.svcreset = UtilSql.getValue(result, "svcreset");
        objectAccountTreeData.svcresetref = UtilSql.getValue(result, "svcresetref");
        objectAccountTreeData.svcresetproy1 = UtilSql.getValue(result, "svcresetproy1");
        objectAccountTreeData.svcresetproy2 = UtilSql.getValue(result, "svcresetproy2");
        objectAccountTreeData.svcresetproy3 = UtilSql.getValue(result, "svcresetproy3");
        objectAccountTreeData.svcresetproy4 = UtilSql.getValue(result, "svcresetproy4");
        objectAccountTreeData.svcresetproy5 = UtilSql.getValue(result, "svcresetproy5");
        objectAccountTreeData.svcresetproy6 = UtilSql.getValue(result, "svcresetproy6");
        objectAccountTreeData.svcresetproy7 = UtilSql.getValue(result, "svcresetproy7");
        objectAccountTreeData.svcresetproy8 = UtilSql.getValue(result, "svcresetproy8");
        objectAccountTreeData.svcresetproy9 = UtilSql.getValue(result, "svcresetproy9");
        objectAccountTreeData.svcresetproy10 = UtilSql.getValue(result, "svcresetproy10");
        objectAccountTreeData.svcresetproy11 = UtilSql.getValue(result, "svcresetproy11");
        objectAccountTreeData.svcresetproy12 = UtilSql.getValue(result, "svcresetproy12");
        objectAccountTreeData.svcresetproy13 = UtilSql.getValue(result, "svcresetproy13");
        objectAccountTreeData.svcresetproy14 = UtilSql.getValue(result, "svcresetproy14");
        objectAccountTreeData.svcresetproy15 = UtilSql.getValue(result, "svcresetproy15");
        objectAccountTreeData.svcresetproy16 = UtilSql.getValue(result, "svcresetproy16");
        objectAccountTreeData.svcresetproy17 = UtilSql.getValue(result, "svcresetproy17");
        objectAccountTreeData.svcresetproy18 = UtilSql.getValue(result, "svcresetproy18");
        objectAccountTreeData.svcresetproy19 = UtilSql.getValue(result, "svcresetproy19");
        objectAccountTreeData.svcresetproy20 = UtilSql.getValue(result, "svcresetproy20");
        objectAccountTreeData.svcresetproy21 = UtilSql.getValue(result, "svcresetproy21");
        objectAccountTreeData.svcresetproy22 = UtilSql.getValue(result, "svcresetproy22");
        objectAccountTreeData.svcresetproy23 = UtilSql.getValue(result, "svcresetproy23");
        objectAccountTreeData.svcresetproy24 = UtilSql.getValue(result, "svcresetproy24");
        objectAccountTreeData.svcresetproy25 = UtilSql.getValue(result, "svcresetproy25");
        objectAccountTreeData.svcresetproy26 = UtilSql.getValue(result, "svcresetproy26");
        objectAccountTreeData.svcresetproy27 = UtilSql.getValue(result, "svcresetproy27");
        objectAccountTreeData.svcresetproy28 = UtilSql.getValue(result, "svcresetproy28");
        objectAccountTreeData.svcresetproy29 = UtilSql.getValue(result, "svcresetproy29");
        objectAccountTreeData.svcresetproy30 = UtilSql.getValue(result, "svcresetproy30");
        objectAccountTreeData.isalwaysshown = UtilSql.getValue(result, "isalwaysshown");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectAcct(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, String orgM1, String clientM1, String acctschemaM1, String anioRef, String orgM2, String clientM2, String acctschemaM2, String anioRefM2, String orgM3, String clientM3, String acctschemaM3, String anioRefM3, String orgM4, String clientM4, String acctschemaM4, String anioRefM4, String orgM5, String clientM5, String acctschemaM5, String anioRefM5, String orgM6, String clientM6, String acctschemaM6, String anioRefM6, String orgM7, String clientM7, String acctschemaM7, String anioRefM7, String orgM8, String clientM8, String acctschemaM8, String anioRefM8, String orgM9, String clientM9, String acctschemaM9, String anioRefM9, String orgM10, String clientM10, String acctschemaM10, String anioRefM10, String orgM11, String clientM11, String acctschemaM11, String anioRefM11, String orgM12, String clientM12, String acctschemaM12, String anioRefM12, String orgM13, String clientM13, String acctschemaM13, String anioRefM13, String orgM14, String clientM14, String acctschemaM14, String anioRefM14, String orgM15, String clientM15, String acctschemaM15, String anioRefM15, String orgM16, String clientM16, String acctschemaM16, String anioRefM16, String orgM17, String clientM17, String acctschemaM17, String anioRefM17, String orgM18, String clientM18, String acctschemaM18, String anioRefM18, String orgM19, String clientM19, String acctschemaM19, String anioRefM19, String orgM20, String clientM20, String acctschemaM20, String anioRefM20, String orgM21, String clientM121, String acctschemaM21, String anioRefM21, String orgM22, String clientM22, String acctschemaM22, String anioRefM22, String orgM23, String clientM23, String acctschemaM23, String anioRefM23, String orgM24, String clientM24, String acctschemaM24, String anioRefM124, String orgM25, String clientM25, String acctschemaM25, String anioRefM25, String orgM26, String clientM26, String acctschemaM26, String anioRefM26, String orgM27, String clientM27, String acctschemaM27, String anioRefM27, String orgM28, String clientM28, String acctschemaM28, String anioRefM28, String orgM29, String clientM29, String acctschemaM29, String anioRefM29, String orgM30, String clientM30, String acctschemaM30, String anioRefM30)    throws ServletException {
    return selectAcct(connectionProvider, adOrgClient, adUserClient, dateFrom, dateTo, acctschema, org, agno, dateFromRef, dateToRef, agnoRef, orgM1, clientM1, acctschemaM1, anioRef, orgM2, clientM2, acctschemaM2, anioRefM2, orgM3, clientM3, acctschemaM3, anioRefM3, orgM4, clientM4, acctschemaM4, anioRefM4, orgM5, clientM5, acctschemaM5, anioRefM5, orgM6, clientM6, acctschemaM6, anioRefM6, orgM7, clientM7, acctschemaM7, anioRefM7, orgM8, clientM8, acctschemaM8, anioRefM8, orgM9, clientM9, acctschemaM9, anioRefM9, orgM10, clientM10, acctschemaM10, anioRefM10, orgM11, clientM11, acctschemaM11, anioRefM11, orgM12, clientM12, acctschemaM12, anioRefM12, orgM13, clientM13, acctschemaM13, anioRefM13, orgM14, clientM14, acctschemaM14, anioRefM14, orgM15, clientM15, acctschemaM15, anioRefM15, orgM16, clientM16, acctschemaM16, anioRefM16, orgM17, clientM17, acctschemaM17, anioRefM17, orgM18, clientM18, acctschemaM18, anioRefM18, orgM19, clientM19, acctschemaM19, anioRefM19, orgM20, clientM20, acctschemaM20, anioRefM20, orgM21, clientM121, acctschemaM21, anioRefM21, orgM22, clientM22, acctschemaM22, anioRefM22, orgM23, clientM23, acctschemaM23, anioRefM23, orgM24, clientM24, acctschemaM24, anioRefM124, orgM25, clientM25, acctschemaM25, anioRefM25, orgM26, clientM26, acctschemaM26, anioRefM26, orgM27, clientM27, acctschemaM27, anioRefM27, orgM28, clientM28, acctschemaM28, anioRefM28, orgM29, clientM29, acctschemaM29, anioRefM29, orgM30, clientM30, acctschemaM30, anioRefM30, 0, 0);
  }

  public static AccountTreeData[] selectAcct(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, String orgM1, String clientM1, String acctschemaM1, String anioRef, String orgM2, String clientM2, String acctschemaM2, String anioRefM2, String orgM3, String clientM3, String acctschemaM3, String anioRefM3, String orgM4, String clientM4, String acctschemaM4, String anioRefM4, String orgM5, String clientM5, String acctschemaM5, String anioRefM5, String orgM6, String clientM6, String acctschemaM6, String anioRefM6, String orgM7, String clientM7, String acctschemaM7, String anioRefM7, String orgM8, String clientM8, String acctschemaM8, String anioRefM8, String orgM9, String clientM9, String acctschemaM9, String anioRefM9, String orgM10, String clientM10, String acctschemaM10, String anioRefM10, String orgM11, String clientM11, String acctschemaM11, String anioRefM11, String orgM12, String clientM12, String acctschemaM12, String anioRefM12, String orgM13, String clientM13, String acctschemaM13, String anioRefM13, String orgM14, String clientM14, String acctschemaM14, String anioRefM14, String orgM15, String clientM15, String acctschemaM15, String anioRefM15, String orgM16, String clientM16, String acctschemaM16, String anioRefM16, String orgM17, String clientM17, String acctschemaM17, String anioRefM17, String orgM18, String clientM18, String acctschemaM18, String anioRefM18, String orgM19, String clientM19, String acctschemaM19, String anioRefM19, String orgM20, String clientM20, String acctschemaM20, String anioRefM20, String orgM21, String clientM121, String acctschemaM21, String anioRefM21, String orgM22, String clientM22, String acctschemaM22, String anioRefM22, String orgM23, String clientM23, String acctschemaM23, String anioRefM23, String orgM24, String clientM24, String acctschemaM24, String anioRefM124, String orgM25, String clientM25, String acctschemaM25, String anioRefM25, String orgM26, String clientM26, String acctschemaM26, String anioRefM26, String orgM27, String clientM27, String acctschemaM27, String anioRefM27, String orgM28, String clientM28, String acctschemaM28, String anioRefM28, String orgM29, String clientM29, String acctschemaM29, String anioRefM29, String orgM30, String clientM30, String acctschemaM30, String anioRefM30, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ID, SUM(QTY) AS QTY, SUM(QTYCREDIT) AS QTYCREDIT, SUM(QTY_REF) AS QTY_REF, SUM(QTYCREDIT_REF) AS QTYCREDIT_REF," +
      "        SUM(qty_proy1) AS qty_proy1, " +
      "        SUM(qty_proy2) AS qty_proy2, " +
      "        SUM(qty_proy3) AS qty_proy3, " +
      "        SUM(qty_proy4) AS qty_proy4, " +
      "        SUM(qty_proy5) AS qty_proy5, " +
      "        SUM(qty_proy6) AS qty_proy6, " +
      "        SUM(qty_proy7) AS qty_proy7, " +
      "        SUM(qty_proy8) AS qty_proy8, " +
      "        SUM(qty_proy9) AS qty_proy9," +
      "        SUM(qty_proy10) AS qty_proy10," +
      "        SUM(qty_proy11) AS qty_proy11, " +
      "        SUM(qty_proy12) AS qty_proy12," +
      "        SUM(qty_proy13) AS qty_proy13, " +
      "        SUM(qty_proy14) AS qty_proy14, " +
      "        SUM(qty_proy15) AS qty_proy15, " +
      "        SUM(qty_proy16) AS qty_proy16, " +
      "        SUM(qty_proy17) AS qty_proy17, " +
      "        SUM(qty_proy18) AS qty_proy18, " +
      "        SUM(qty_proy19) AS qty_proy19," +
      "        SUM(qty_proy20) AS qty_proy20," +
      "        SUM(qty_proy21) AS qty_proy21, " +
      "        SUM(qty_proy22) AS qty_proy22," +
      "        SUM(qty_proy23) AS qty_proy23, " +
      "        SUM(qty_proy24) AS qty_proy24, " +
      "        SUM(qty_proy25) AS qty_proy25, " +
      "        SUM(qty_proy26) AS qty_proy26, " +
      "        SUM(qty_proy27) AS qty_proy27, " +
      "        SUM(qty_proy28) AS qty_proy28, " +
      "        SUM(qty_proy29) AS qty_proy29," +
      "        SUM(qty_proy30) AS qty_proy30" +
      "        FROM (" +
      "        SELECT m.C_ElementValue_ID as id, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty," +
      "        (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "				        0 as qty_proy1,  0 as qty_proy2, " +
      "				        0 as qty_proy3,  0 as qty_proy4," +
      "				        0 as qty_proy5,  0 as qty_proy6, " +
      "				        0 as qty_proy7,  0 as qty_proy8, " +
      "				        0 as qty_proy9,  0 as qty_proy10, " +
      "				        0 as qty_proy11, 0 as qty_proy12," +
      "				        0 as qty_proy13, 0 as qty_proy14, " +
      "				        0 as qty_proy15, 0 as qty_proy16," +
      "				        0 as qty_proy17, 0 as qty_proy18, " +
      "				        0 as qty_proy19, 0 as qty_proy20, " +
      "				        0 as qty_proy21, 0 as qty_proy22, " +
      "				        0 as qty_proy23, 0 as qty_proy24," +
      "				        0 as qty_proy25, 0 as qty_proy26, " +
      "				        0 as qty_proy27, 0 as qty_proy28, " +
      "				        0 as qty_proy29, 0 as qty_proy30" +
      "                FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "                WHERE m.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "                AND m.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "                AND 1=1 ";
    strSql = strSql + ((dateFrom==null || dateFrom.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateTo==null || dateTo.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "                AND f.FACTACCTTYPE <> 'R'" +
      "                AND f.FACTACCTTYPE <> 'C'" +
      "                AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND 0=0 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND Y.YEAR IN (";
    strSql = strSql + ((agno==null || agno.equals(""))?"":agno);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS QTY, 0 as qtyCredit, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty_ref, " +
      "                (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit_ref," +
      "                0 as qty_proy1, 0 as qty_proy2," +
      "                0 as qty_proy3, 0 as qty_proy4," +
      "                0 as qty_proy5, 0 as qty_proy6," +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14," +
      "                0 as qty_proy15, 0 as qty_proy16," +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20," +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24," +
      "                0 as qty_proy25, 0 as qty_proy26," +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30 " +
      "                FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "                WHERE m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "                AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "                AND 2=2 ";
    strSql = strSql + ((dateFromRef==null || dateFromRef.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef==null || dateToRef.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "                AND f.FACTACCTTYPE <> 'R'" +
      "                AND f.FACTACCTTYPE <> 'C'" +
      "                AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND 1=1 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (";
    strSql = strSql + ((agnoRef==null || agnoRef.equals(""))?"":agnoRef);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy1," +
      "                0 as qty_proy2, " +
      "                0 as qty_proy3, 0 as qty_proy4, " +
      "                0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 1" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)  " +
      "         UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy2," +
      "                0 as qty_proy3, 0 as qty_proy4, " +
      "                0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 2" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy3," +
      "                0 as qty_proy4, " +
      "                0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 3" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy4, " +
      "                0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 4" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy5, " +
      "                0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 5" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 6" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy7, " +
      "                0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 7" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 8" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy9, " +
      "                0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 9" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 10" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy11, " +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 11" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy13, " +
      "                0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy15, " +
      "                0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy17, " +
      "                0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy19," +
      "                0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "             UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy21, " +
      "                0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy23, " +
      "                0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy25, " +
      "                0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy27, " +
      "                0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy29, " +
      "                0 as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "           UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy30" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        ) AA" +
      "        GROUP BY ID";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFrom != null && !(dateFrom.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFrom);
      }
      if (dateTo != null && !(dateTo.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTo);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agno != null && !(agno.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFromRef != null && !(dateFromRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef);
      }
      if (dateToRef != null && !(dateToRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agnoRef != null && !(agnoRef.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRef);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM13);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM13);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM13);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM13);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM13);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM14);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM14);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM14);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM14);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM14);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM15);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM15);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM15);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM15);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM15);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM16);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM16);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM16);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM16);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM16);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM17);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM17);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM17);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM17);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM17);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM18);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM18);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM18);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM18);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM18);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM19);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM19);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM19);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM19);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM19);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM20);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM20);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM20);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM20);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM20);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM21);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM21);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM121);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM21);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM21);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM22);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM22);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM22);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM22);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM22);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM23);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM23);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM23);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM23);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM23);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM24);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM24);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM24);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM24);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM124);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM25);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM25);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM25);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM25);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM25);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM26);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM26);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM26);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM26);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM26);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM27);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM27);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM27);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM27);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM27);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM28);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM28);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM28);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM28);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM28);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM29);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM29);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM29);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM29);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM29);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM30);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM30);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM30);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM30);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM30);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeData.qtyProy1 = UtilSql.getValue(result, "qty_proy1");
        objectAccountTreeData.qtyProy2 = UtilSql.getValue(result, "qty_proy2");
        objectAccountTreeData.qtyProy3 = UtilSql.getValue(result, "qty_proy3");
        objectAccountTreeData.qtyProy4 = UtilSql.getValue(result, "qty_proy4");
        objectAccountTreeData.qtyProy5 = UtilSql.getValue(result, "qty_proy5");
        objectAccountTreeData.qtyProy6 = UtilSql.getValue(result, "qty_proy6");
        objectAccountTreeData.qtyProy7 = UtilSql.getValue(result, "qty_proy7");
        objectAccountTreeData.qtyProy8 = UtilSql.getValue(result, "qty_proy8");
        objectAccountTreeData.qtyProy9 = UtilSql.getValue(result, "qty_proy9");
        objectAccountTreeData.qtyProy10 = UtilSql.getValue(result, "qty_proy10");
        objectAccountTreeData.qtyProy11 = UtilSql.getValue(result, "qty_proy11");
        objectAccountTreeData.qtyProy12 = UtilSql.getValue(result, "qty_proy12");
        objectAccountTreeData.qtyProy13 = UtilSql.getValue(result, "qty_proy13");
        objectAccountTreeData.qtyProy14 = UtilSql.getValue(result, "qty_proy14");
        objectAccountTreeData.qtyProy15 = UtilSql.getValue(result, "qty_proy15");
        objectAccountTreeData.qtyProy16 = UtilSql.getValue(result, "qty_proy16");
        objectAccountTreeData.qtyProy17 = UtilSql.getValue(result, "qty_proy17");
        objectAccountTreeData.qtyProy18 = UtilSql.getValue(result, "qty_proy18");
        objectAccountTreeData.qtyProy19 = UtilSql.getValue(result, "qty_proy19");
        objectAccountTreeData.qtyProy20 = UtilSql.getValue(result, "qty_proy20");
        objectAccountTreeData.qtyProy21 = UtilSql.getValue(result, "qty_proy21");
        objectAccountTreeData.qtyProy22 = UtilSql.getValue(result, "qty_proy22");
        objectAccountTreeData.qtyProy23 = UtilSql.getValue(result, "qty_proy23");
        objectAccountTreeData.qtyProy24 = UtilSql.getValue(result, "qty_proy24");
        objectAccountTreeData.qtyProy25 = UtilSql.getValue(result, "qty_proy25");
        objectAccountTreeData.qtyProy26 = UtilSql.getValue(result, "qty_proy26");
        objectAccountTreeData.qtyProy27 = UtilSql.getValue(result, "qty_proy27");
        objectAccountTreeData.qtyProy28 = UtilSql.getValue(result, "qty_proy28");
        objectAccountTreeData.qtyProy29 = UtilSql.getValue(result, "qty_proy29");
        objectAccountTreeData.qtyProy30 = UtilSql.getValue(result, "qty_proy30");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectForms(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient)    throws ServletException {
    return selectForms(connectionProvider, adOrgClient, adUserClient, 0, 0);
  }

  public static AccountTreeData[] selectForms(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT m.C_ElementValue_ID as id, o.account_id as node_id, o.sign as ACCOUNTSIGN" +
      "        FROM C_ElementValue m, C_ELEMENTVALUE_OPERAND o  " +
      "        WHERE m.isActive='Y' " +
      "        AND m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND m.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND o.isactive = 'Y' " +
      "        order by m.C_elementvalue_id, o.seqno";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectOperands(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String element)    throws ServletException {
    return selectOperands(connectionProvider, adOrgClient, adUserClient, element, 0, 0);
  }

  public static AccountTreeData[] selectOperands(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String element, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT m.C_ElementValue_ID as id, o.account_id as node_id, o.sign" +
      "        FROM C_ElementValue m, C_ELEMENTVALUE_OPERAND o, C_ElementValue n" +
      "        WHERE m.isActive='Y' " +
      "        AND m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND m.C_Element_ID = ?" +
      "        AND m.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND n.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND o.isactive = 'Y' " +
      "        order by m.C_elementvalue_id, o.seqno";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, element);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.sign = UtilSql.getValue(result, "sign");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }
}
