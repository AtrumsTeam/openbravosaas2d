//Sqlc generated V1.O00-1
package com.openbravo.gps.finance.balanceexcelexport.report;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

public class AccountTreeProyectoData implements FieldProvider {
static Logger log4j = Logger.getLogger(AccountTreeProyectoData.class);
  private String InitRecordNumber="0";
  public String nodeId;
  public String parentId;
  public String seqno;
  public String id;
  public String name;
  public String description;
  public String issummary;
  public String accountsign;
  public String showelement;
  public String elementLevel;
  public String qty;
  public String qtyProy;
  public String qtyProy1;
  public String qtyProy2;
  public String qtyProy3;
  public String qtyProy4;
  public String qtyProy5;
  public String qtyProy6;
  public String qtyProy7;
  public String qtyProy8;
  public String qtyProy9;
  public String qtyProy10;
  public String qtyProy11;
  public String qtyProy12;
  public String qtyProy13;
  public String qtyProy14;
  public String qtyProy15;
  public String qtyProy16;
  public String qtyProy17;
  public String qtyProy18;
  public String qtyProy19;
  public String qtyProy20;
  public String qtyProy21;
  public String qtyProy22;
  public String qtyProy23;
  public String qtyProy24;
  public String qtyProy25;
  public String qtyProy26;
  public String qtyProy27;
  public String qtyProy28;
  public String qtyProy29;
  public String qtyProy30;
  public String qtyRef;
  public String qtyOperation;
  public String qtyOperationRef;
  public String qtyOperationProy;
  public String qtyOperationProy1;
  public String qtyOperationProy2;
  public String qtyOperationProy3;
  public String qtyOperationProy4;
  public String qtyOperationProy5;
  public String qtyOperationProy6;
  public String qtyOperationProy7;
  public String qtyOperationProy8;
  public String qtyOperationProy9;
  public String qtyOperationProy10;
  public String qtyOperationProy11;
  public String qtyOperationProy12;
  public String qtyOperationProy13;
  public String qtyOperationProy14;
  public String qtyOperationProy15;
  public String qtyOperationProy16;
  public String qtyOperationProy17;
  public String qtyOperationProy18;
  public String qtyOperationProy19;
  public String qtyOperationProy20;
  public String qtyOperationProy21;
  public String qtyOperationProy22;
  public String qtyOperationProy23;
  public String qtyOperationProy24;
  public String qtyOperationProy25;
  public String qtyOperationProy26;
  public String qtyOperationProy27;
  public String qtyOperationProy28;
  public String qtyOperationProy29;
  public String qtyOperationProy30;
  public String qtycredit;
  public String qtycreditRef;
  public String qtycreditProy;
  public String qtycreditProy1;
  public String qtycreditProy2;
  public String qtycreditProy3;
  public String qtycreditProy4;
  public String qtycreditProy5;
  public String qtycreditProy6;
  public String qtycreditProy7;
  public String qtycreditProy8;
  public String qtycreditProy9;
  public String qtycreditProy10;
  public String qtycreditProy11;
  public String qtycreditProy12;
  public String qtycreditProy13;
  public String qtycreditProy14;
  public String qtycreditProy15;
  public String qtycreditProy16;
  public String qtycreditProy17;
  public String qtycreditProy18;
  public String qtycreditProy19;
  public String qtycreditProy20;
  public String qtycreditProy21;
  public String qtycreditProy22;
  public String qtycreditProy23;
  public String qtycreditProy24;
  public String qtycreditProy25;
  public String qtycreditProy26;
  public String qtycreditProy27;
  public String qtycreditProy28;
  public String qtycreditProy29;
  public String qtycreditProy30;
  public String showvaluecond;
  public String elementlevel;
  public String value;
  public String calculated;
  public String svcreset;
  public String svcresetref;
  public String svcresetproy;
  public String svcresetproy1;
  public String svcresetproy2;
  public String svcresetproy3;
  public String svcresetproy4;
  public String svcresetproy5;
  public String svcresetproy6;
  public String svcresetproy7;
  public String svcresetproy8;
  public String svcresetproy9;
  public String svcresetproy10;
  public String svcresetproy11;
  public String svcresetproy12;
  public String svcresetproy13;
  public String svcresetproy14;
  public String svcresetproy15;
  public String svcresetproy16;
  public String svcresetproy17;
  public String svcresetproy18;
  public String svcresetproy19;
  public String svcresetproy20;
  public String svcresetproy21;
  public String svcresetproy22;
  public String svcresetproy23;
  public String svcresetproy24;
  public String svcresetproy25;
  public String svcresetproy26;
  public String svcresetproy27;
  public String svcresetproy28;
  public String svcresetproy29;
  public String svcresetproy30;
  public String isalwaysshown;
  public String sign;
  public String fechaInicio;
  public String fechaFin;
  public String proyecto;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("node_id") || fieldName.equals("nodeId"))
      return nodeId;
    else if (fieldName.equalsIgnoreCase("parent_id") || fieldName.equals("parentId"))
      return parentId;
    else if (fieldName.equalsIgnoreCase("seqno"))
      return seqno;
    else if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("accountsign"))
      return accountsign;
    else if (fieldName.equalsIgnoreCase("showelement"))
      return showelement;
    else if (fieldName.equalsIgnoreCase("element_level") || fieldName.equals("elementLevel"))
      return elementLevel;
    else if (fieldName.equalsIgnoreCase("qty"))
      return qty;
    else if (fieldName.equalsIgnoreCase("qty_proy") || fieldName.equals("qtyProy"))
      return qtyProy;
    else if (fieldName.equalsIgnoreCase("qty_proy1") || fieldName.equals("qtyProy1"))
      return qtyProy1;
    else if (fieldName.equalsIgnoreCase("qty_proy2") || fieldName.equals("qtyProy2"))
      return qtyProy2;
    else if (fieldName.equalsIgnoreCase("qty_proy3") || fieldName.equals("qtyProy3"))
      return qtyProy3;
    else if (fieldName.equalsIgnoreCase("qty_proy4") || fieldName.equals("qtyProy4"))
      return qtyProy4;
    else if (fieldName.equalsIgnoreCase("qty_proy5") || fieldName.equals("qtyProy5"))
      return qtyProy5;
    else if (fieldName.equalsIgnoreCase("qty_proy6") || fieldName.equals("qtyProy6"))
      return qtyProy6;
    else if (fieldName.equalsIgnoreCase("qty_proy7") || fieldName.equals("qtyProy7"))
      return qtyProy7;
    else if (fieldName.equalsIgnoreCase("qty_proy8") || fieldName.equals("qtyProy8"))
      return qtyProy8;
    else if (fieldName.equalsIgnoreCase("qty_proy9") || fieldName.equals("qtyProy9"))
      return qtyProy9;
    else if (fieldName.equalsIgnoreCase("qty_proy10") || fieldName.equals("qtyProy10"))
      return qtyProy10;
    else if (fieldName.equalsIgnoreCase("qty_proy11") || fieldName.equals("qtyProy11"))
      return qtyProy11;
    else if (fieldName.equalsIgnoreCase("qty_proy12") || fieldName.equals("qtyProy12"))
      return qtyProy12;
    else if (fieldName.equalsIgnoreCase("qty_proy13") || fieldName.equals("qtyProy13"))
      return qtyProy13;
    else if (fieldName.equalsIgnoreCase("qty_proy14") || fieldName.equals("qtyProy14"))
      return qtyProy14;
    else if (fieldName.equalsIgnoreCase("qty_proy15") || fieldName.equals("qtyProy15"))
      return qtyProy15;
    else if (fieldName.equalsIgnoreCase("qty_proy16") || fieldName.equals("qtyProy16"))
      return qtyProy16;
    else if (fieldName.equalsIgnoreCase("qty_proy17") || fieldName.equals("qtyProy17"))
      return qtyProy17;
    else if (fieldName.equalsIgnoreCase("qty_proy18") || fieldName.equals("qtyProy18"))
      return qtyProy18;
    else if (fieldName.equalsIgnoreCase("qty_proy19") || fieldName.equals("qtyProy19"))
      return qtyProy19;
    else if (fieldName.equalsIgnoreCase("qty_proy20") || fieldName.equals("qtyProy20"))
      return qtyProy20;
    else if (fieldName.equalsIgnoreCase("qty_proy21") || fieldName.equals("qtyProy21"))
      return qtyProy21;
    else if (fieldName.equalsIgnoreCase("qty_proy22") || fieldName.equals("qtyProy22"))
      return qtyProy22;
    else if (fieldName.equalsIgnoreCase("qty_proy23") || fieldName.equals("qtyProy23"))
      return qtyProy23;
    else if (fieldName.equalsIgnoreCase("qty_proy24") || fieldName.equals("qtyProy24"))
      return qtyProy24;
    else if (fieldName.equalsIgnoreCase("qty_proy25") || fieldName.equals("qtyProy25"))
      return qtyProy25;
    else if (fieldName.equalsIgnoreCase("qty_proy26") || fieldName.equals("qtyProy26"))
      return qtyProy26;
    else if (fieldName.equalsIgnoreCase("qty_proy27") || fieldName.equals("qtyProy27"))
      return qtyProy27;
    else if (fieldName.equalsIgnoreCase("qty_proy28") || fieldName.equals("qtyProy28"))
      return qtyProy28;
    else if (fieldName.equalsIgnoreCase("qty_proy29") || fieldName.equals("qtyProy29"))
      return qtyProy29;
    else if (fieldName.equalsIgnoreCase("qty_proy30") || fieldName.equals("qtyProy30"))
      return qtyProy30;
    else if (fieldName.equalsIgnoreCase("qty_ref") || fieldName.equals("qtyRef"))
      return qtyRef;
    else if (fieldName.equalsIgnoreCase("qty_operation") || fieldName.equals("qtyOperation"))
      return qtyOperation;
    else if (fieldName.equalsIgnoreCase("qty_operation_ref") || fieldName.equals("qtyOperationRef"))
      return qtyOperationRef;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy") || fieldName.equals("qtyOperationProy"))
      return qtyOperationProy;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy1") || fieldName.equals("qtyOperationProy1"))
      return qtyOperationProy1;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy2") || fieldName.equals("qtyOperationProy2"))
      return qtyOperationProy2;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy3") || fieldName.equals("qtyOperationProy3"))
      return qtyOperationProy3;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy4") || fieldName.equals("qtyOperationProy4"))
      return qtyOperationProy4;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy5") || fieldName.equals("qtyOperationProy5"))
      return qtyOperationProy5;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy6") || fieldName.equals("qtyOperationProy6"))
      return qtyOperationProy6;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy7") || fieldName.equals("qtyOperationProy7"))
      return qtyOperationProy7;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy8") || fieldName.equals("qtyOperationProy8"))
      return qtyOperationProy8;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy9") || fieldName.equals("qtyOperationProy9"))
      return qtyOperationProy9;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy10") || fieldName.equals("qtyOperationProy10"))
      return qtyOperationProy10;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy11") || fieldName.equals("qtyOperationProy11"))
      return qtyOperationProy11;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy12") || fieldName.equals("qtyOperationProy12"))
      return qtyOperationProy12;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy13") || fieldName.equals("qtyOperationProy13"))
      return qtyOperationProy13;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy14") || fieldName.equals("qtyOperationProy14"))
      return qtyOperationProy14;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy15") || fieldName.equals("qtyOperationProy15"))
      return qtyOperationProy15;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy16") || fieldName.equals("qtyOperationProy16"))
      return qtyOperationProy16;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy17") || fieldName.equals("qtyOperationProy17"))
      return qtyOperationProy17;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy18") || fieldName.equals("qtyOperationProy18"))
      return qtyOperationProy18;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy19") || fieldName.equals("qtyOperationProy19"))
      return qtyOperationProy19;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy20") || fieldName.equals("qtyOperationProy20"))
      return qtyOperationProy20;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy21") || fieldName.equals("qtyOperationProy21"))
      return qtyOperationProy21;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy22") || fieldName.equals("qtyOperationProy22"))
      return qtyOperationProy22;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy23") || fieldName.equals("qtyOperationProy23"))
      return qtyOperationProy23;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy24") || fieldName.equals("qtyOperationProy24"))
      return qtyOperationProy24;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy25") || fieldName.equals("qtyOperationProy25"))
      return qtyOperationProy25;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy26") || fieldName.equals("qtyOperationProy26"))
      return qtyOperationProy26;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy27") || fieldName.equals("qtyOperationProy27"))
      return qtyOperationProy27;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy28") || fieldName.equals("qtyOperationProy28"))
      return qtyOperationProy28;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy29") || fieldName.equals("qtyOperationProy29"))
      return qtyOperationProy29;
    else if (fieldName.equalsIgnoreCase("qty_operation_proy30") || fieldName.equals("qtyOperationProy30"))
      return qtyOperationProy30;
    else if (fieldName.equalsIgnoreCase("qtycredit"))
      return qtycredit;
    else if (fieldName.equalsIgnoreCase("qtycredit_ref") || fieldName.equals("qtycreditRef"))
      return qtycreditRef;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy") || fieldName.equals("qtycreditProy"))
      return qtycreditProy;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy1") || fieldName.equals("qtycreditProy1"))
      return qtycreditProy1;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy2") || fieldName.equals("qtycreditProy2"))
      return qtycreditProy2;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy3") || fieldName.equals("qtycreditProy3"))
      return qtycreditProy3;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy4") || fieldName.equals("qtycreditProy4"))
      return qtycreditProy4;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy5") || fieldName.equals("qtycreditProy5"))
      return qtycreditProy5;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy6") || fieldName.equals("qtycreditProy6"))
      return qtycreditProy6;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy7") || fieldName.equals("qtycreditProy7"))
      return qtycreditProy7;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy8") || fieldName.equals("qtycreditProy8"))
      return qtycreditProy8;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy9") || fieldName.equals("qtycreditProy9"))
      return qtycreditProy9;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy10") || fieldName.equals("qtycreditProy10"))
      return qtycreditProy10;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy11") || fieldName.equals("qtycreditProy11"))
      return qtycreditProy11;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy12") || fieldName.equals("qtycreditProy12"))
      return qtycreditProy12;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy13") || fieldName.equals("qtycreditProy13"))
      return qtycreditProy13;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy14") || fieldName.equals("qtycreditProy14"))
      return qtycreditProy14;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy15") || fieldName.equals("qtycreditProy15"))
      return qtycreditProy15;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy16") || fieldName.equals("qtycreditProy16"))
      return qtycreditProy16;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy17") || fieldName.equals("qtycreditProy17"))
      return qtycreditProy17;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy18") || fieldName.equals("qtycreditProy18"))
      return qtycreditProy18;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy19") || fieldName.equals("qtycreditProy19"))
      return qtycreditProy19;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy20") || fieldName.equals("qtycreditProy20"))
      return qtycreditProy20;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy21") || fieldName.equals("qtycreditProy21"))
      return qtycreditProy21;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy22") || fieldName.equals("qtycreditProy22"))
      return qtycreditProy22;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy23") || fieldName.equals("qtycreditProy23"))
      return qtycreditProy23;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy24") || fieldName.equals("qtycreditProy24"))
      return qtycreditProy24;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy25") || fieldName.equals("qtycreditProy25"))
      return qtycreditProy25;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy26") || fieldName.equals("qtycreditProy26"))
      return qtycreditProy26;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy27") || fieldName.equals("qtycreditProy27"))
      return qtycreditProy27;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy28") || fieldName.equals("qtycreditProy28"))
      return qtycreditProy28;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy29") || fieldName.equals("qtycreditProy29"))
      return qtycreditProy29;
    else if (fieldName.equalsIgnoreCase("qtycredit_proy30") || fieldName.equals("qtycreditProy30"))
      return qtycreditProy30;
    else if (fieldName.equalsIgnoreCase("showvaluecond"))
      return showvaluecond;
    else if (fieldName.equalsIgnoreCase("elementlevel"))
      return elementlevel;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("calculated"))
      return calculated;
    else if (fieldName.equalsIgnoreCase("svcreset"))
      return svcreset;
    else if (fieldName.equalsIgnoreCase("svcresetref"))
      return svcresetref;
    else if (fieldName.equalsIgnoreCase("svcresetproy"))
      return svcresetproy;
    else if (fieldName.equalsIgnoreCase("svcresetproy1"))
      return svcresetproy1;
    else if (fieldName.equalsIgnoreCase("svcresetproy2"))
      return svcresetproy2;
    else if (fieldName.equalsIgnoreCase("svcresetproy3"))
      return svcresetproy3;
    else if (fieldName.equalsIgnoreCase("svcresetproy4"))
      return svcresetproy4;
    else if (fieldName.equalsIgnoreCase("svcresetproy5"))
      return svcresetproy5;
    else if (fieldName.equalsIgnoreCase("svcresetproy6"))
      return svcresetproy6;
    else if (fieldName.equalsIgnoreCase("svcresetproy7"))
      return svcresetproy7;
    else if (fieldName.equalsIgnoreCase("svcresetproy8"))
      return svcresetproy8;
    else if (fieldName.equalsIgnoreCase("svcresetproy9"))
      return svcresetproy9;
    else if (fieldName.equalsIgnoreCase("svcresetproy10"))
      return svcresetproy10;
    else if (fieldName.equalsIgnoreCase("svcresetproy11"))
      return svcresetproy11;
    else if (fieldName.equalsIgnoreCase("svcresetproy12"))
      return svcresetproy12;
    else if (fieldName.equalsIgnoreCase("svcresetproy13"))
      return svcresetproy13;
    else if (fieldName.equalsIgnoreCase("svcresetproy14"))
      return svcresetproy14;
    else if (fieldName.equalsIgnoreCase("svcresetproy15"))
      return svcresetproy15;
    else if (fieldName.equalsIgnoreCase("svcresetproy16"))
      return svcresetproy16;
    else if (fieldName.equalsIgnoreCase("svcresetproy17"))
      return svcresetproy17;
    else if (fieldName.equalsIgnoreCase("svcresetproy18"))
      return svcresetproy18;
    else if (fieldName.equalsIgnoreCase("svcresetproy19"))
      return svcresetproy19;
    else if (fieldName.equalsIgnoreCase("svcresetproy20"))
      return svcresetproy20;
    else if (fieldName.equalsIgnoreCase("svcresetproy21"))
      return svcresetproy21;
    else if (fieldName.equalsIgnoreCase("svcresetproy22"))
      return svcresetproy22;
    else if (fieldName.equalsIgnoreCase("svcresetproy23"))
      return svcresetproy23;
    else if (fieldName.equalsIgnoreCase("svcresetproy24"))
      return svcresetproy24;
    else if (fieldName.equalsIgnoreCase("svcresetproy25"))
      return svcresetproy25;
    else if (fieldName.equalsIgnoreCase("svcresetproy26"))
      return svcresetproy26;
    else if (fieldName.equalsIgnoreCase("svcresetproy27"))
      return svcresetproy27;
    else if (fieldName.equalsIgnoreCase("svcresetproy28"))
      return svcresetproy28;
    else if (fieldName.equalsIgnoreCase("svcresetproy29"))
      return svcresetproy29;
    else if (fieldName.equalsIgnoreCase("svcresetproy30"))
      return svcresetproy30;
    else if (fieldName.equalsIgnoreCase("isalwaysshown"))
      return isalwaysshown;
    else if (fieldName.equalsIgnoreCase("sign"))
      return sign;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("proyecto"))
      return proyecto;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static AccountTreeProyectoData[] select(ConnectionProvider connectionProvider, String conCodigo, String adTreeId)    throws ServletException {
    return select(connectionProvider, conCodigo, adTreeId, 0, 0);
  }

  public static AccountTreeProyectoData[] select(ConnectionProvider connectionProvider, String conCodigo, String adTreeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT tn.Node_ID,tn.Parent_ID,tn.SeqNo, m.C_ElementValue_ID AS ID, " +
      "        ((CASE ? WHEN 'Y' THEN TO_CHAR(m.Value) || ' - ' ELSE '' END) || m.Name) AS NAME,m.Description, m.IsSummary, " +
      "        m.ACCOUNTSIGN, m.ShowElement, 0 as element_level, 0 as qty, " +
      "        0 AS qty_proy," +
      "        0 AS qty_proy1, " +
      "        0 AS qty_proy2, " +
      "        0 AS qty_proy3, " +
      "        0 AS qty_proy4, " +
      "        0 AS qty_proy5, " +
      "        0 AS qty_proy6, " +
      "        0 AS qty_proy7, " +
      "        0 AS qty_proy8, " +
      "        0 AS qty_proy9," +
      "        0 AS qty_proy10," +
      "        0 AS qty_proy11, " +
      "        0 AS qty_proy12," +
      "        0 AS qty_proy13, " +
      "        0 AS qty_proy14," +
      "        0 AS qty_proy15," +
      "        0 AS qty_proy16, " +
      "        0 AS qty_proy17," +
      "        0 AS qty_proy18," +
      "        0 AS qty_proy19, " +
      "        0 AS qty_proy20," +
      "        0 AS qty_proy21, " +
      "        0 AS qty_proy22," +
      "        0 AS qty_proy23," +
      "        0 AS qty_proy24, " +
      "        0 AS qty_proy25," +
      "        0 AS qty_proy26, " +
      "        0 AS qty_proy27, " +
      "        0 AS qty_proy28, " +
      "        0 AS qty_proy29," +
      "        0 AS qty_proy30," +
      "        0 as qty_ref, 0 as qty_operation, 0 as qty_operation_ref, " +
      "        0 as qty_operation_proy," +
      "        0 as qty_operation_proy1," +
      "        0 as qty_operation_proy2," +
      "        0 as qty_operation_proy3," +
      "        0 as qty_operation_proy4," +
      "        0 as qty_operation_proy5," +
      "        0 as qty_operation_proy6," +
      "        0 as qty_operation_proy7," +
      "        0 as qty_operation_proy8," +
      "        0 as qty_operation_proy9," +
      "        0 as qty_operation_proy10," +
      "        0 as qty_operation_proy11," +
      "        0 as qty_operation_proy12," +
      "        0 as qty_operation_proy13," +
      "        0 as qty_operation_proy14," +
      "        0 as qty_operation_proy15," +
      "        0 as qty_operation_proy16," +
      "        0 as qty_operation_proy17," +
      "        0 as qty_operation_proy18," +
      "        0 as qty_operation_proy19," +
      "        0 as qty_operation_proy20," +
      "        0 as qty_operation_proy21," +
      "        0 as qty_operation_proy22," +
      "        0 as qty_operation_proy23," +
      "        0 as qty_operation_proy24," +
      "        0 as qty_operation_proy25," +
      "        0 as qty_operation_proy26," +
      "        0 as qty_operation_proy27," +
      "        0 as qty_operation_proy28," +
      "        0 as qty_operation_proy29," +
      "        0 as qty_operation_proy30," +
      "        0 as QTYCREDIT, 0 as QTYCREDIT_REF," +
      "        0 as QTYCREDIT_PROY, " +
      "        0 as QTYCREDIT_PROY1," +
      "        0 as QTYCREDIT_PROY2," +
      "        0 as QTYCREDIT_PROY3," +
      "        0 as QTYCREDIT_PROY4," +
      "        0 as QTYCREDIT_PROY5," +
      "        0 as QTYCREDIT_PROY6," +
      "        0 as QTYCREDIT_PROY7," +
      "        0 as QTYCREDIT_PROY8," +
      "        0 as QTYCREDIT_PROY9," +
      "        0 as QTYCREDIT_PROY10," +
      "        0 as QTYCREDIT_PROY11," +
      "        0 as QTYCREDIT_PROY12," +
      "        0 as QTYCREDIT_PROY13," +
      "        0 as QTYCREDIT_PROY14," +
      "        0 as QTYCREDIT_PROY15," +
      "        0 as QTYCREDIT_PROY16," +
      "        0 as QTYCREDIT_PROY17," +
      "        0 as QTYCREDIT_PROY18," +
      "        0 as QTYCREDIT_PROY19," +
      "        0 as QTYCREDIT_PROY20," +
      "        0 as QTYCREDIT_PROY21," +
      "        0 as QTYCREDIT_PROY22," +
      "        0 as QTYCREDIT_PROY23," +
      "        0 as QTYCREDIT_PROY24," +
      "        0 as QTYCREDIT_PROY25," +
      "        0 as QTYCREDIT_PROY26," +
      "        0 as QTYCREDIT_PROY27," +
      "        0 as QTYCREDIT_PROY28," +
      "        0 as QTYCREDIT_PROY29," +
      "        0 as QTYCREDIT_PROY30," +
      "        m.ShowValueCond, m.ElementLevel, m.Value, " +
      "        'N' AS CALCULATED, 'N' AS SVCRESET, 'N' AS SVCRESETREF, " +
      "        'N' AS SVCRESETPROY," +
      "        'N' AS SVCRESETPROY1," +
      "        'N' AS SVCRESETPROY2," +
      "        'N' AS SVCRESETPROY3," +
      "        'N' AS SVCRESETPROY4, " +
      "        'N' AS SVCRESETPROY5," +
      "        'N' AS SVCRESETPROY6," +
      "        'N' AS SVCRESETPROY7," +
      "        'N' AS SVCRESETPROY8," +
      "        'N' AS SVCRESETPROY9," +
      "        'N' AS SVCRESETPROY10," +
      "        'N' AS SVCRESETPROY11," +
      "        'N' AS SVCRESETPROY12," +
      "        'N' AS SVCRESETPROY13," +
      "        'N' AS SVCRESETPROY14, " +
      "        'N' AS SVCRESETPROY15," +
      "        'N' AS SVCRESETPROY16," +
      "        'N' AS SVCRESETPROY17," +
      "        'N' AS SVCRESETPROY18," +
      "        'N' AS SVCRESETPROY19," +
      "        'N' AS SVCRESETPROY20," +
      "        'N' AS SVCRESETPROY21," +
      "        'N' AS SVCRESETPROY22," +
      "        'N' AS SVCRESETPROY23," +
      "        'N' AS SVCRESETPROY24, " +
      "        'N' AS SVCRESETPROY25," +
      "        'N' AS SVCRESETPROY26," +
      "        'N' AS SVCRESETPROY27," +
      "        'N' AS SVCRESETPROY28," +
      "        'N' AS SVCRESETPROY29," +
      "        'N' AS SVCRESETPROY30," +
      "        m.isalwaysshown, '' as sign," +
      "        null as fecha_inicio," +
      "        null as fecha_fin," +
      "        null as proyecto" +
      "        FROM AD_TreeNode tn, C_ElementValue m" +
      "        WHERE tn.IsActive='Y' " +
      "        AND tn.Node_ID = m.C_ElementValue_ID " +
      "        AND tn.AD_Tree_ID = ? " +
      "        ORDER BY COALESCE(tn.Parent_ID, '-1'), tn.SeqNo";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, conCodigo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeProyectoData.parentId = UtilSql.getValue(result, "parent_id");
        objectAccountTreeProyectoData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountTreeProyectoData.id = UtilSql.getValue(result, "id");
        objectAccountTreeProyectoData.name = UtilSql.getValue(result, "name");
        objectAccountTreeProyectoData.description = UtilSql.getValue(result, "description");
        objectAccountTreeProyectoData.issummary = UtilSql.getValue(result, "issummary");
        objectAccountTreeProyectoData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeProyectoData.showelement = UtilSql.getValue(result, "showelement");
        objectAccountTreeProyectoData.elementLevel = UtilSql.getValue(result, "element_level");
        objectAccountTreeProyectoData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeProyectoData.qtyProy = UtilSql.getValue(result, "qty_proy");
        objectAccountTreeProyectoData.qtyProy1 = UtilSql.getValue(result, "qty_proy1");
        objectAccountTreeProyectoData.qtyProy2 = UtilSql.getValue(result, "qty_proy2");
        objectAccountTreeProyectoData.qtyProy3 = UtilSql.getValue(result, "qty_proy3");
        objectAccountTreeProyectoData.qtyProy4 = UtilSql.getValue(result, "qty_proy4");
        objectAccountTreeProyectoData.qtyProy5 = UtilSql.getValue(result, "qty_proy5");
        objectAccountTreeProyectoData.qtyProy6 = UtilSql.getValue(result, "qty_proy6");
        objectAccountTreeProyectoData.qtyProy7 = UtilSql.getValue(result, "qty_proy7");
        objectAccountTreeProyectoData.qtyProy8 = UtilSql.getValue(result, "qty_proy8");
        objectAccountTreeProyectoData.qtyProy9 = UtilSql.getValue(result, "qty_proy9");
        objectAccountTreeProyectoData.qtyProy10 = UtilSql.getValue(result, "qty_proy10");
        objectAccountTreeProyectoData.qtyProy11 = UtilSql.getValue(result, "qty_proy11");
        objectAccountTreeProyectoData.qtyProy12 = UtilSql.getValue(result, "qty_proy12");
        objectAccountTreeProyectoData.qtyProy13 = UtilSql.getValue(result, "qty_proy13");
        objectAccountTreeProyectoData.qtyProy14 = UtilSql.getValue(result, "qty_proy14");
        objectAccountTreeProyectoData.qtyProy15 = UtilSql.getValue(result, "qty_proy15");
        objectAccountTreeProyectoData.qtyProy16 = UtilSql.getValue(result, "qty_proy16");
        objectAccountTreeProyectoData.qtyProy17 = UtilSql.getValue(result, "qty_proy17");
        objectAccountTreeProyectoData.qtyProy18 = UtilSql.getValue(result, "qty_proy18");
        objectAccountTreeProyectoData.qtyProy19 = UtilSql.getValue(result, "qty_proy19");
        objectAccountTreeProyectoData.qtyProy20 = UtilSql.getValue(result, "qty_proy20");
        objectAccountTreeProyectoData.qtyProy21 = UtilSql.getValue(result, "qty_proy21");
        objectAccountTreeProyectoData.qtyProy22 = UtilSql.getValue(result, "qty_proy22");
        objectAccountTreeProyectoData.qtyProy23 = UtilSql.getValue(result, "qty_proy23");
        objectAccountTreeProyectoData.qtyProy24 = UtilSql.getValue(result, "qty_proy24");
        objectAccountTreeProyectoData.qtyProy25 = UtilSql.getValue(result, "qty_proy25");
        objectAccountTreeProyectoData.qtyProy26 = UtilSql.getValue(result, "qty_proy26");
        objectAccountTreeProyectoData.qtyProy27 = UtilSql.getValue(result, "qty_proy27");
        objectAccountTreeProyectoData.qtyProy28 = UtilSql.getValue(result, "qty_proy28");
        objectAccountTreeProyectoData.qtyProy29 = UtilSql.getValue(result, "qty_proy29");
        objectAccountTreeProyectoData.qtyProy30 = UtilSql.getValue(result, "qty_proy30");
        objectAccountTreeProyectoData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeProyectoData.qtyOperation = UtilSql.getValue(result, "qty_operation");
        objectAccountTreeProyectoData.qtyOperationRef = UtilSql.getValue(result, "qty_operation_ref");
        objectAccountTreeProyectoData.qtyOperationProy = UtilSql.getValue(result, "qty_operation_proy");
        objectAccountTreeProyectoData.qtyOperationProy1 = UtilSql.getValue(result, "qty_operation_proy1");
        objectAccountTreeProyectoData.qtyOperationProy2 = UtilSql.getValue(result, "qty_operation_proy2");
        objectAccountTreeProyectoData.qtyOperationProy3 = UtilSql.getValue(result, "qty_operation_proy3");
        objectAccountTreeProyectoData.qtyOperationProy4 = UtilSql.getValue(result, "qty_operation_proy4");
        objectAccountTreeProyectoData.qtyOperationProy5 = UtilSql.getValue(result, "qty_operation_proy5");
        objectAccountTreeProyectoData.qtyOperationProy6 = UtilSql.getValue(result, "qty_operation_proy6");
        objectAccountTreeProyectoData.qtyOperationProy7 = UtilSql.getValue(result, "qty_operation_proy7");
        objectAccountTreeProyectoData.qtyOperationProy8 = UtilSql.getValue(result, "qty_operation_proy8");
        objectAccountTreeProyectoData.qtyOperationProy9 = UtilSql.getValue(result, "qty_operation_proy9");
        objectAccountTreeProyectoData.qtyOperationProy10 = UtilSql.getValue(result, "qty_operation_proy10");
        objectAccountTreeProyectoData.qtyOperationProy11 = UtilSql.getValue(result, "qty_operation_proy11");
        objectAccountTreeProyectoData.qtyOperationProy12 = UtilSql.getValue(result, "qty_operation_proy12");
        objectAccountTreeProyectoData.qtyOperationProy13 = UtilSql.getValue(result, "qty_operation_proy13");
        objectAccountTreeProyectoData.qtyOperationProy14 = UtilSql.getValue(result, "qty_operation_proy14");
        objectAccountTreeProyectoData.qtyOperationProy15 = UtilSql.getValue(result, "qty_operation_proy15");
        objectAccountTreeProyectoData.qtyOperationProy16 = UtilSql.getValue(result, "qty_operation_proy16");
        objectAccountTreeProyectoData.qtyOperationProy17 = UtilSql.getValue(result, "qty_operation_proy17");
        objectAccountTreeProyectoData.qtyOperationProy18 = UtilSql.getValue(result, "qty_operation_proy18");
        objectAccountTreeProyectoData.qtyOperationProy19 = UtilSql.getValue(result, "qty_operation_proy19");
        objectAccountTreeProyectoData.qtyOperationProy20 = UtilSql.getValue(result, "qty_operation_proy20");
        objectAccountTreeProyectoData.qtyOperationProy21 = UtilSql.getValue(result, "qty_operation_proy21");
        objectAccountTreeProyectoData.qtyOperationProy22 = UtilSql.getValue(result, "qty_operation_proy22");
        objectAccountTreeProyectoData.qtyOperationProy23 = UtilSql.getValue(result, "qty_operation_proy23");
        objectAccountTreeProyectoData.qtyOperationProy24 = UtilSql.getValue(result, "qty_operation_proy24");
        objectAccountTreeProyectoData.qtyOperationProy25 = UtilSql.getValue(result, "qty_operation_proy25");
        objectAccountTreeProyectoData.qtyOperationProy26 = UtilSql.getValue(result, "qty_operation_proy26");
        objectAccountTreeProyectoData.qtyOperationProy27 = UtilSql.getValue(result, "qty_operation_proy27");
        objectAccountTreeProyectoData.qtyOperationProy28 = UtilSql.getValue(result, "qty_operation_proy28");
        objectAccountTreeProyectoData.qtyOperationProy29 = UtilSql.getValue(result, "qty_operation_proy29");
        objectAccountTreeProyectoData.qtyOperationProy30 = UtilSql.getValue(result, "qty_operation_proy30");
        objectAccountTreeProyectoData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeProyectoData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeProyectoData.qtycreditProy = UtilSql.getValue(result, "qtycredit_proy");
        objectAccountTreeProyectoData.qtycreditProy1 = UtilSql.getValue(result, "qtycredit_proy1");
        objectAccountTreeProyectoData.qtycreditProy2 = UtilSql.getValue(result, "qtycredit_proy2");
        objectAccountTreeProyectoData.qtycreditProy3 = UtilSql.getValue(result, "qtycredit_proy3");
        objectAccountTreeProyectoData.qtycreditProy4 = UtilSql.getValue(result, "qtycredit_proy4");
        objectAccountTreeProyectoData.qtycreditProy5 = UtilSql.getValue(result, "qtycredit_proy5");
        objectAccountTreeProyectoData.qtycreditProy6 = UtilSql.getValue(result, "qtycredit_proy6");
        objectAccountTreeProyectoData.qtycreditProy7 = UtilSql.getValue(result, "qtycredit_proy7");
        objectAccountTreeProyectoData.qtycreditProy8 = UtilSql.getValue(result, "qtycredit_proy8");
        objectAccountTreeProyectoData.qtycreditProy9 = UtilSql.getValue(result, "qtycredit_proy9");
        objectAccountTreeProyectoData.qtycreditProy10 = UtilSql.getValue(result, "qtycredit_proy10");
        objectAccountTreeProyectoData.qtycreditProy11 = UtilSql.getValue(result, "qtycredit_proy11");
        objectAccountTreeProyectoData.qtycreditProy12 = UtilSql.getValue(result, "qtycredit_proy12");
        objectAccountTreeProyectoData.qtycreditProy13 = UtilSql.getValue(result, "qtycredit_proy13");
        objectAccountTreeProyectoData.qtycreditProy14 = UtilSql.getValue(result, "qtycredit_proy14");
        objectAccountTreeProyectoData.qtycreditProy15 = UtilSql.getValue(result, "qtycredit_proy15");
        objectAccountTreeProyectoData.qtycreditProy16 = UtilSql.getValue(result, "qtycredit_proy16");
        objectAccountTreeProyectoData.qtycreditProy17 = UtilSql.getValue(result, "qtycredit_proy17");
        objectAccountTreeProyectoData.qtycreditProy18 = UtilSql.getValue(result, "qtycredit_proy18");
        objectAccountTreeProyectoData.qtycreditProy19 = UtilSql.getValue(result, "qtycredit_proy19");
        objectAccountTreeProyectoData.qtycreditProy20 = UtilSql.getValue(result, "qtycredit_proy20");
        objectAccountTreeProyectoData.qtycreditProy21 = UtilSql.getValue(result, "qtycredit_proy21");
        objectAccountTreeProyectoData.qtycreditProy22 = UtilSql.getValue(result, "qtycredit_proy22");
        objectAccountTreeProyectoData.qtycreditProy23 = UtilSql.getValue(result, "qtycredit_proy23");
        objectAccountTreeProyectoData.qtycreditProy24 = UtilSql.getValue(result, "qtycredit_proy24");
        objectAccountTreeProyectoData.qtycreditProy25 = UtilSql.getValue(result, "qtycredit_proy25");
        objectAccountTreeProyectoData.qtycreditProy26 = UtilSql.getValue(result, "qtycredit_proy26");
        objectAccountTreeProyectoData.qtycreditProy27 = UtilSql.getValue(result, "qtycredit_proy27");
        objectAccountTreeProyectoData.qtycreditProy28 = UtilSql.getValue(result, "qtycredit_proy28");
        objectAccountTreeProyectoData.qtycreditProy29 = UtilSql.getValue(result, "qtycredit_proy29");
        objectAccountTreeProyectoData.qtycreditProy30 = UtilSql.getValue(result, "qtycredit_proy30");
        objectAccountTreeProyectoData.showvaluecond = UtilSql.getValue(result, "showvaluecond");
        objectAccountTreeProyectoData.elementlevel = UtilSql.getValue(result, "elementlevel");
        objectAccountTreeProyectoData.value = UtilSql.getValue(result, "value");
        objectAccountTreeProyectoData.calculated = UtilSql.getValue(result, "calculated");
        objectAccountTreeProyectoData.svcreset = UtilSql.getValue(result, "svcreset");
        objectAccountTreeProyectoData.svcresetref = UtilSql.getValue(result, "svcresetref");
        objectAccountTreeProyectoData.svcresetproy = UtilSql.getValue(result, "svcresetproy");
        objectAccountTreeProyectoData.svcresetproy1 = UtilSql.getValue(result, "svcresetproy1");
        objectAccountTreeProyectoData.svcresetproy2 = UtilSql.getValue(result, "svcresetproy2");
        objectAccountTreeProyectoData.svcresetproy3 = UtilSql.getValue(result, "svcresetproy3");
        objectAccountTreeProyectoData.svcresetproy4 = UtilSql.getValue(result, "svcresetproy4");
        objectAccountTreeProyectoData.svcresetproy5 = UtilSql.getValue(result, "svcresetproy5");
        objectAccountTreeProyectoData.svcresetproy6 = UtilSql.getValue(result, "svcresetproy6");
        objectAccountTreeProyectoData.svcresetproy7 = UtilSql.getValue(result, "svcresetproy7");
        objectAccountTreeProyectoData.svcresetproy8 = UtilSql.getValue(result, "svcresetproy8");
        objectAccountTreeProyectoData.svcresetproy9 = UtilSql.getValue(result, "svcresetproy9");
        objectAccountTreeProyectoData.svcresetproy10 = UtilSql.getValue(result, "svcresetproy10");
        objectAccountTreeProyectoData.svcresetproy11 = UtilSql.getValue(result, "svcresetproy11");
        objectAccountTreeProyectoData.svcresetproy12 = UtilSql.getValue(result, "svcresetproy12");
        objectAccountTreeProyectoData.svcresetproy13 = UtilSql.getValue(result, "svcresetproy13");
        objectAccountTreeProyectoData.svcresetproy14 = UtilSql.getValue(result, "svcresetproy14");
        objectAccountTreeProyectoData.svcresetproy15 = UtilSql.getValue(result, "svcresetproy15");
        objectAccountTreeProyectoData.svcresetproy16 = UtilSql.getValue(result, "svcresetproy16");
        objectAccountTreeProyectoData.svcresetproy17 = UtilSql.getValue(result, "svcresetproy17");
        objectAccountTreeProyectoData.svcresetproy18 = UtilSql.getValue(result, "svcresetproy18");
        objectAccountTreeProyectoData.svcresetproy19 = UtilSql.getValue(result, "svcresetproy19");
        objectAccountTreeProyectoData.svcresetproy20 = UtilSql.getValue(result, "svcresetproy20");
        objectAccountTreeProyectoData.svcresetproy21 = UtilSql.getValue(result, "svcresetproy21");
        objectAccountTreeProyectoData.svcresetproy22 = UtilSql.getValue(result, "svcresetproy22");
        objectAccountTreeProyectoData.svcresetproy23 = UtilSql.getValue(result, "svcresetproy23");
        objectAccountTreeProyectoData.svcresetproy24 = UtilSql.getValue(result, "svcresetproy24");
        objectAccountTreeProyectoData.svcresetproy25 = UtilSql.getValue(result, "svcresetproy25");
        objectAccountTreeProyectoData.svcresetproy26 = UtilSql.getValue(result, "svcresetproy26");
        objectAccountTreeProyectoData.svcresetproy27 = UtilSql.getValue(result, "svcresetproy27");
        objectAccountTreeProyectoData.svcresetproy28 = UtilSql.getValue(result, "svcresetproy28");
        objectAccountTreeProyectoData.svcresetproy29 = UtilSql.getValue(result, "svcresetproy29");
        objectAccountTreeProyectoData.svcresetproy30 = UtilSql.getValue(result, "svcresetproy30");
        objectAccountTreeProyectoData.isalwaysshown = UtilSql.getValue(result, "isalwaysshown");
        objectAccountTreeProyectoData.sign = UtilSql.getValue(result, "sign");
        objectAccountTreeProyectoData.fechaInicio = UtilSql.getValue(result, "fecha_inicio");
        objectAccountTreeProyectoData.fechaFin = UtilSql.getValue(result, "fecha_fin");
        objectAccountTreeProyectoData.proyecto = UtilSql.getValue(result, "proyecto");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectTrl(ConnectionProvider connectionProvider, String conCodigo, String adLanguage, String adTreeId)    throws ServletException {
    return selectTrl(connectionProvider, conCodigo, adLanguage, adTreeId, 0, 0);
  }

  public static AccountTreeProyectoData[] selectTrl(ConnectionProvider connectionProvider, String conCodigo, String adLanguage, String adTreeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT tn.Node_ID,tn.Parent_ID,tn.SeqNo, m.C_ElementValue_ID AS ID, ((CASE ? WHEN 'Y' THEN TO_CHAR(m.Value) || ' - ' ELSE '' END) || COALESCE(mt.Name, m.Name)) as Name, " +
      "        COALESCE(mt.Description, m.Description) as description ,m.IsSummary, m.ACCOUNTSIGN, " +
      "        m.ShowElement, 0 as element_level, 0 as qty, " +
      "        0 AS qty_proy," +
      "        0 AS qty_proy1, " +
      "        0 AS qty_proy2, " +
      "        0 AS qty_proy3, " +
      "        0 AS qty_proy4, " +
      "        0 AS qty_proy5, " +
      "        0 AS qty_proy6, " +
      "        0 AS qty_proy7, " +
      "        0 AS qty_proy8, " +
      "        0 AS qty_proy9," +
      "        0 AS qty_proy10," +
      "        0 AS qty_proy11, " +
      "        0 AS qty_proy12," +
      "        0 AS qty_proy13, " +
      "        0 AS qty_proy14, " +
      "        0 AS qty_proy15, " +
      "        0 AS qty_proy16, " +
      "        0 AS qty_proy17, " +
      "        0 AS qty_proy18, " +
      "        0 AS qty_proy19," +
      "        0 AS qty_proy20," +
      "        0 AS qty_proy21, " +
      "        0 AS qty_proy22," +
      "        0 AS qty_proy23, " +
      "        0 AS qty_proy24, " +
      "        0 AS qty_proy25, " +
      "        0 AS qty_proy26, " +
      "        0 AS qty_proy27, " +
      "        0 AS qty_proy28, " +
      "        0 AS qty_proy29," +
      "        0 AS qty_proy30," +
      "        0 as qty_ref, 0 as qty_operation, 0 as qty_operation_ref," +
      "        0 as qty_operation_proy," +
      "        0 as qty_operation_proy1," +
      "        0 as qty_operation_proy2," +
      "        0 as qty_operation_proy3," +
      "        0 as qty_operation_proy4, " +
      "        0 as qty_operation_proy5," +
      "        0 as qty_operation_proy6," +
      "        0 as qty_operation_proy7," +
      "        0 as qty_operation_proy8, " +
      "        0 as qty_operation_proy9," +
      "        0 as qty_operation_proy10," +
      "        0 as qty_operation_proy11," +
      "        0 as qty_operation_proy12, " +
      "        0 as qty_operation_proy13," +
      "        0 as qty_operation_proy14, " +
      "        0 as qty_operation_proy15," +
      "        0 as qty_operation_proy16," +
      "        0 as qty_operation_proy17," +
      "        0 as qty_operation_proy18, " +
      "        0 as qty_operation_proy19," +
      "        0 as qty_operation_proy20, " +
      "        0 as qty_operation_proy21," +
      "        0 as qty_operation_proy22," +
      "        0 as qty_operation_proy23," +
      "        0 as qty_operation_proy24, " +
      "        0 as qty_operation_proy25," +
      "        0 as qty_operation_proy26," +
      "        0 as qty_operation_proy27," +
      "        0 as qty_operation_proy28, " +
      "        0 as qty_operation_proy29," +
      "        0 as qty_operation_proy30," +
      "        m.ShowValueCond, m.ElementLevel, m.Value, 'N' AS CALCULATED, 'N' AS SVCRESET, 'N' AS SVCRESETREF, " +
      "        'N' AS SVCRESETPROY," +
      "        'N' AS SVCRESETPROY1," +
      "        'N' AS SVCRESETPROY2," +
      "        'N' AS SVCRESETPROY3," +
      "        'N' AS SVCRESETPROY4, " +
      "        'N' AS SVCRESETPROY5," +
      "        'N' AS SVCRESETPROY6," +
      "        'N' AS SVCRESETPROY7," +
      "        'N' AS SVCRESETPROY8," +
      "        'N' AS SVCRESETPROY9," +
      "        'N' AS SVCRESETPROY10," +
      "        'N' AS SVCRESETPROY11," +
      "        'N' AS SVCRESETPROY12," +
      "        'N' AS SVCRESETPROY13," +
      "        'N' AS SVCRESETPROY14, " +
      "        'N' AS SVCRESETPROY15," +
      "        'N' AS SVCRESETPROY16," +
      "        'N' AS SVCRESETPROY17," +
      "        'N' AS SVCRESETPROY18," +
      "        'N' AS SVCRESETPROY19," +
      "        'N' AS SVCRESETPROY20," +
      "        'N' AS SVCRESETPROY21," +
      "        'N' AS SVCRESETPROY22," +
      "        'N' AS SVCRESETPROY23," +
      "        'N' AS SVCRESETPROY24, " +
      "        'N' AS SVCRESETPROY25," +
      "        'N' AS SVCRESETPROY26," +
      "        'N' AS SVCRESETPROY27," +
      "        'N' AS SVCRESETPROY28," +
      "        'N' AS SVCRESETPROY29," +
      "        'N' AS SVCRESETPROY30," +
      "        m.isalwaysshown" +
      "        FROM C_ElementValue m left join C_ElementValue_Trl mt on m.C_ElementValue_ID = mt.C_ElementValue_ID " +
      "                                                              and mt.AD_Language = ? ," +
      "              AD_TreeNode tn" +
      "        WHERE tn.IsActive='Y' " +
      "        AND tn.Node_ID = m.C_ElementValue_ID " +
      "        AND tn.AD_Tree_ID = ? " +
      "        ORDER BY COALESCE(tn.Parent_ID, '-1'), tn.SeqNo ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, conCodigo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeProyectoData.parentId = UtilSql.getValue(result, "parent_id");
        objectAccountTreeProyectoData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountTreeProyectoData.id = UtilSql.getValue(result, "id");
        objectAccountTreeProyectoData.name = UtilSql.getValue(result, "name");
        objectAccountTreeProyectoData.description = UtilSql.getValue(result, "description");
        objectAccountTreeProyectoData.issummary = UtilSql.getValue(result, "issummary");
        objectAccountTreeProyectoData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeProyectoData.showelement = UtilSql.getValue(result, "showelement");
        objectAccountTreeProyectoData.elementLevel = UtilSql.getValue(result, "element_level");
        objectAccountTreeProyectoData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeProyectoData.qtyProy = UtilSql.getValue(result, "qty_proy");
        objectAccountTreeProyectoData.qtyProy1 = UtilSql.getValue(result, "qty_proy1");
        objectAccountTreeProyectoData.qtyProy2 = UtilSql.getValue(result, "qty_proy2");
        objectAccountTreeProyectoData.qtyProy3 = UtilSql.getValue(result, "qty_proy3");
        objectAccountTreeProyectoData.qtyProy4 = UtilSql.getValue(result, "qty_proy4");
        objectAccountTreeProyectoData.qtyProy5 = UtilSql.getValue(result, "qty_proy5");
        objectAccountTreeProyectoData.qtyProy6 = UtilSql.getValue(result, "qty_proy6");
        objectAccountTreeProyectoData.qtyProy7 = UtilSql.getValue(result, "qty_proy7");
        objectAccountTreeProyectoData.qtyProy8 = UtilSql.getValue(result, "qty_proy8");
        objectAccountTreeProyectoData.qtyProy9 = UtilSql.getValue(result, "qty_proy9");
        objectAccountTreeProyectoData.qtyProy10 = UtilSql.getValue(result, "qty_proy10");
        objectAccountTreeProyectoData.qtyProy11 = UtilSql.getValue(result, "qty_proy11");
        objectAccountTreeProyectoData.qtyProy12 = UtilSql.getValue(result, "qty_proy12");
        objectAccountTreeProyectoData.qtyProy13 = UtilSql.getValue(result, "qty_proy13");
        objectAccountTreeProyectoData.qtyProy14 = UtilSql.getValue(result, "qty_proy14");
        objectAccountTreeProyectoData.qtyProy15 = UtilSql.getValue(result, "qty_proy15");
        objectAccountTreeProyectoData.qtyProy16 = UtilSql.getValue(result, "qty_proy16");
        objectAccountTreeProyectoData.qtyProy17 = UtilSql.getValue(result, "qty_proy17");
        objectAccountTreeProyectoData.qtyProy18 = UtilSql.getValue(result, "qty_proy18");
        objectAccountTreeProyectoData.qtyProy19 = UtilSql.getValue(result, "qty_proy19");
        objectAccountTreeProyectoData.qtyProy20 = UtilSql.getValue(result, "qty_proy20");
        objectAccountTreeProyectoData.qtyProy21 = UtilSql.getValue(result, "qty_proy21");
        objectAccountTreeProyectoData.qtyProy22 = UtilSql.getValue(result, "qty_proy22");
        objectAccountTreeProyectoData.qtyProy23 = UtilSql.getValue(result, "qty_proy23");
        objectAccountTreeProyectoData.qtyProy24 = UtilSql.getValue(result, "qty_proy24");
        objectAccountTreeProyectoData.qtyProy25 = UtilSql.getValue(result, "qty_proy25");
        objectAccountTreeProyectoData.qtyProy26 = UtilSql.getValue(result, "qty_proy26");
        objectAccountTreeProyectoData.qtyProy27 = UtilSql.getValue(result, "qty_proy27");
        objectAccountTreeProyectoData.qtyProy28 = UtilSql.getValue(result, "qty_proy28");
        objectAccountTreeProyectoData.qtyProy29 = UtilSql.getValue(result, "qty_proy29");
        objectAccountTreeProyectoData.qtyProy30 = UtilSql.getValue(result, "qty_proy30");
        objectAccountTreeProyectoData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeProyectoData.qtyOperation = UtilSql.getValue(result, "qty_operation");
        objectAccountTreeProyectoData.qtyOperationRef = UtilSql.getValue(result, "qty_operation_ref");
        objectAccountTreeProyectoData.qtyOperationProy = UtilSql.getValue(result, "qty_operation_proy");
        objectAccountTreeProyectoData.qtyOperationProy1 = UtilSql.getValue(result, "qty_operation_proy1");
        objectAccountTreeProyectoData.qtyOperationProy2 = UtilSql.getValue(result, "qty_operation_proy2");
        objectAccountTreeProyectoData.qtyOperationProy3 = UtilSql.getValue(result, "qty_operation_proy3");
        objectAccountTreeProyectoData.qtyOperationProy4 = UtilSql.getValue(result, "qty_operation_proy4");
        objectAccountTreeProyectoData.qtyOperationProy5 = UtilSql.getValue(result, "qty_operation_proy5");
        objectAccountTreeProyectoData.qtyOperationProy6 = UtilSql.getValue(result, "qty_operation_proy6");
        objectAccountTreeProyectoData.qtyOperationProy7 = UtilSql.getValue(result, "qty_operation_proy7");
        objectAccountTreeProyectoData.qtyOperationProy8 = UtilSql.getValue(result, "qty_operation_proy8");
        objectAccountTreeProyectoData.qtyOperationProy9 = UtilSql.getValue(result, "qty_operation_proy9");
        objectAccountTreeProyectoData.qtyOperationProy10 = UtilSql.getValue(result, "qty_operation_proy10");
        objectAccountTreeProyectoData.qtyOperationProy11 = UtilSql.getValue(result, "qty_operation_proy11");
        objectAccountTreeProyectoData.qtyOperationProy12 = UtilSql.getValue(result, "qty_operation_proy12");
        objectAccountTreeProyectoData.qtyOperationProy13 = UtilSql.getValue(result, "qty_operation_proy13");
        objectAccountTreeProyectoData.qtyOperationProy14 = UtilSql.getValue(result, "qty_operation_proy14");
        objectAccountTreeProyectoData.qtyOperationProy15 = UtilSql.getValue(result, "qty_operation_proy15");
        objectAccountTreeProyectoData.qtyOperationProy16 = UtilSql.getValue(result, "qty_operation_proy16");
        objectAccountTreeProyectoData.qtyOperationProy17 = UtilSql.getValue(result, "qty_operation_proy17");
        objectAccountTreeProyectoData.qtyOperationProy18 = UtilSql.getValue(result, "qty_operation_proy18");
        objectAccountTreeProyectoData.qtyOperationProy19 = UtilSql.getValue(result, "qty_operation_proy19");
        objectAccountTreeProyectoData.qtyOperationProy20 = UtilSql.getValue(result, "qty_operation_proy20");
        objectAccountTreeProyectoData.qtyOperationProy21 = UtilSql.getValue(result, "qty_operation_proy21");
        objectAccountTreeProyectoData.qtyOperationProy22 = UtilSql.getValue(result, "qty_operation_proy22");
        objectAccountTreeProyectoData.qtyOperationProy23 = UtilSql.getValue(result, "qty_operation_proy23");
        objectAccountTreeProyectoData.qtyOperationProy24 = UtilSql.getValue(result, "qty_operation_proy24");
        objectAccountTreeProyectoData.qtyOperationProy25 = UtilSql.getValue(result, "qty_operation_proy25");
        objectAccountTreeProyectoData.qtyOperationProy26 = UtilSql.getValue(result, "qty_operation_proy26");
        objectAccountTreeProyectoData.qtyOperationProy27 = UtilSql.getValue(result, "qty_operation_proy27");
        objectAccountTreeProyectoData.qtyOperationProy28 = UtilSql.getValue(result, "qty_operation_proy28");
        objectAccountTreeProyectoData.qtyOperationProy29 = UtilSql.getValue(result, "qty_operation_proy29");
        objectAccountTreeProyectoData.qtyOperationProy30 = UtilSql.getValue(result, "qty_operation_proy30");
        objectAccountTreeProyectoData.showvaluecond = UtilSql.getValue(result, "showvaluecond");
        objectAccountTreeProyectoData.elementlevel = UtilSql.getValue(result, "elementlevel");
        objectAccountTreeProyectoData.value = UtilSql.getValue(result, "value");
        objectAccountTreeProyectoData.calculated = UtilSql.getValue(result, "calculated");
        objectAccountTreeProyectoData.svcreset = UtilSql.getValue(result, "svcreset");
        objectAccountTreeProyectoData.svcresetref = UtilSql.getValue(result, "svcresetref");
        objectAccountTreeProyectoData.svcresetproy = UtilSql.getValue(result, "svcresetproy");
        objectAccountTreeProyectoData.svcresetproy1 = UtilSql.getValue(result, "svcresetproy1");
        objectAccountTreeProyectoData.svcresetproy2 = UtilSql.getValue(result, "svcresetproy2");
        objectAccountTreeProyectoData.svcresetproy3 = UtilSql.getValue(result, "svcresetproy3");
        objectAccountTreeProyectoData.svcresetproy4 = UtilSql.getValue(result, "svcresetproy4");
        objectAccountTreeProyectoData.svcresetproy5 = UtilSql.getValue(result, "svcresetproy5");
        objectAccountTreeProyectoData.svcresetproy6 = UtilSql.getValue(result, "svcresetproy6");
        objectAccountTreeProyectoData.svcresetproy7 = UtilSql.getValue(result, "svcresetproy7");
        objectAccountTreeProyectoData.svcresetproy8 = UtilSql.getValue(result, "svcresetproy8");
        objectAccountTreeProyectoData.svcresetproy9 = UtilSql.getValue(result, "svcresetproy9");
        objectAccountTreeProyectoData.svcresetproy10 = UtilSql.getValue(result, "svcresetproy10");
        objectAccountTreeProyectoData.svcresetproy11 = UtilSql.getValue(result, "svcresetproy11");
        objectAccountTreeProyectoData.svcresetproy12 = UtilSql.getValue(result, "svcresetproy12");
        objectAccountTreeProyectoData.svcresetproy13 = UtilSql.getValue(result, "svcresetproy13");
        objectAccountTreeProyectoData.svcresetproy14 = UtilSql.getValue(result, "svcresetproy14");
        objectAccountTreeProyectoData.svcresetproy15 = UtilSql.getValue(result, "svcresetproy15");
        objectAccountTreeProyectoData.svcresetproy16 = UtilSql.getValue(result, "svcresetproy16");
        objectAccountTreeProyectoData.svcresetproy17 = UtilSql.getValue(result, "svcresetproy17");
        objectAccountTreeProyectoData.svcresetproy18 = UtilSql.getValue(result, "svcresetproy18");
        objectAccountTreeProyectoData.svcresetproy19 = UtilSql.getValue(result, "svcresetproy19");
        objectAccountTreeProyectoData.svcresetproy20 = UtilSql.getValue(result, "svcresetproy20");
        objectAccountTreeProyectoData.svcresetproy21 = UtilSql.getValue(result, "svcresetproy21");
        objectAccountTreeProyectoData.svcresetproy22 = UtilSql.getValue(result, "svcresetproy22");
        objectAccountTreeProyectoData.svcresetproy23 = UtilSql.getValue(result, "svcresetproy23");
        objectAccountTreeProyectoData.svcresetproy24 = UtilSql.getValue(result, "svcresetproy24");
        objectAccountTreeProyectoData.svcresetproy25 = UtilSql.getValue(result, "svcresetproy25");
        objectAccountTreeProyectoData.svcresetproy26 = UtilSql.getValue(result, "svcresetproy26");
        objectAccountTreeProyectoData.svcresetproy27 = UtilSql.getValue(result, "svcresetproy27");
        objectAccountTreeProyectoData.svcresetproy28 = UtilSql.getValue(result, "svcresetproy28");
        objectAccountTreeProyectoData.svcresetproy29 = UtilSql.getValue(result, "svcresetproy29");
        objectAccountTreeProyectoData.svcresetproy30 = UtilSql.getValue(result, "svcresetproy30");
        objectAccountTreeProyectoData.isalwaysshown = UtilSql.getValue(result, "isalwaysshown");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectAcctNormal(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef)    throws ServletException {
    return selectAcctNormal(connectionProvider, adOrgClient, adUserClient, dateFrom, dateTo, acctschema, org, agno, dateFromRef, dateToRef, agnoRef, 0, 0);
  }

  public static AccountTreeProyectoData[] selectAcctNormal(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ID, SUM(QTY) AS QTY, SUM(QTYCREDIT) AS QTYCREDIT, SUM(QTY_REF) AS QTY_REF, SUM(QTYCREDIT_REF) AS QTYCREDIT_REF " +
      "        FROM (" +
      "	        SELECT m.C_ElementValue_ID as id, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty, " +
      "	        	   (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref" +
      "	          FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "	         WHERE f.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "	           AND f.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ")" +
      "	           AND 1=1 ";
    strSql = strSql + ((dateFrom==null || dateFrom.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateTo==null || dateTo.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "	           AND f.FACTACCTTYPE <> 'R'" +
      "	           AND f.FACTACCTTYPE <> 'C'" +
      "	           AND m.C_ElementValue_ID = f.Account_ID" +
      "	           AND 0=0 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "	           AND f.C_PERIOD_ID = p.C_PERIOD_ID" +
      "	           AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "	           AND Y.YEAR IN (";
    strSql = strSql + ((agno==null || agno.equals(""))?"":agno);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "	        SELECT m.C_ElementValue_ID as id, 0 AS QTY, 0 as qtyCredit, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty_ref, " +
      "	               (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit_ref " +
      "	          FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "	         WHERE f.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "	           AND f.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "	           AND 2=2 ";
    strSql = strSql + ((dateFromRef==null || dateFromRef.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef==null || dateToRef.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "	           AND f.FACTACCTTYPE <> 'R'" +
      "	           AND f.FACTACCTTYPE <> 'C'" +
      "	           AND m.C_ElementValue_ID = f.Account_ID" +
      "	           AND 1=1 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "	           AND f.C_PERIOD_ID = p.C_PERIOD_ID  " +
      "	           AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "	           AND y.YEAR IN (";
    strSql = strSql + ((agnoRef==null || agnoRef.equals(""))?"":agnoRef);
    strSql = strSql + 
      ") " +
      "        ) AA" +
      "        GROUP BY ID";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFrom != null && !(dateFrom.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFrom);
      }
      if (dateTo != null && !(dateTo.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTo);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agno != null && !(agno.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFromRef != null && !(dateFromRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef);
      }
      if (dateToRef != null && !(dateToRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agnoRef != null && !(agnoRef.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.id = UtilSql.getValue(result, "id");
        objectAccountTreeProyectoData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeProyectoData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeProyectoData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeProyectoData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectAcct(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, String adOrgClient0, String adUserClient0, String dateFromRef0, String dateToRef0, String acctschema0, String org0, String agnoRef0, String adOrgClient1, String adUserClient1, String dateFromRef1, String dateToRef1, String acctschema1, String org1, String agnoRef1, String mProject1, String adOrgClient2, String adUserClient2, String dateFromRef2, String dateToRef2, String acctschema2, String org2, String agnoRef2, String mProject2, String adOrgClient3, String adUserClient3, String dateFromRef3, String dateToRef3, String acctschema3, String org3, String agnoRef3, String mProject3, String adOrgClient4, String adUserClient4, String dateFromRef4, String dateToRef4, String acctschema4, String org4, String agnoRef4, String mProject4, String adOrgClient5, String adUserClient5, String dateFromRef5, String dateToRef5, String acctschema5, String org5, String agnoRef5, String mProject5, String adOrgClient6, String adUserClient6, String dateFromRef6, String dateToRef6, String acctschema6, String org6, String agnoRef6, String mProject6, String adOrgClient7, String adUserClient7, String dateFromRef7, String dateToRef7, String acctschema7, String org7, String agnoRef7, String mProject7, String adOrgClient8, String adUserClient8, String dateFromRef8, String dateToRef8, String acctschema8, String org8, String agnoRef8, String mProject8, String adOrgClient9, String adUserClient9, String dateFromRef9, String dateToRef9, String acctschema9, String org9, String agnoRef9, String mProject9, String adOrgClient10, String adUserClient10, String dateFromRef10, String dateToRef10, String acctschema10, String org10, String agnoRef10, String mProject10, String adOrgClient11, String adUserClient11, String dateFromRef11, String dateToRef11, String acctschema11, String org11, String agnoRef11, String mProject11, String adOrgClient12, String adUserClient12, String dateFromRef12, String dateToRef12, String acctschema12, String org12, String agnoRef12, String mProject12, String adOrgClient13, String adUserClient13, String dateFromRef13, String dateToRef13, String acctschema13, String org13, String agnoRef13, String mProject13, String adOrgClient14, String adUserClient14, String dateFromRef14, String dateToRef14, String acctschema14, String org14, String agnoRef14, String mProject14, String adOrgClient15, String adUserClient15, String dateFromRef15, String dateToRef15, String acctschema15, String org15, String agnoRef15, String mProject15, String adOrgClient16, String adUserClient16, String dateFromRef16, String dateToRef16, String acctschema16, String org16, String agnoRef16, String mProject16, String adOrgClient17, String adUserClient17, String dateFromRef17, String dateToRef17, String acctschema17, String org17, String agnoRef17, String mProject17, String adOrgClient18, String adUserClient18, String dateFromRef18, String dateToRef18, String acctschema18, String org18, String agnoRef18, String mProject18, String adOrgClient19, String adUserClient19, String dateFromRef19, String dateToRef19, String acctschema19, String org19, String agnoRef19, String mProject19, String adOrgClient20, String adUserClient20, String dateFromRef20, String dateToRef20, String acctschema20, String org20, String agnoRef20, String mProject20, String adOrgClient21, String adUserClient21, String dateFromRef21, String dateToRef21, String acctschema21, String org21, String agnoRef21, String mProject21, String adOrgClient22, String adUserClient22, String dateFromRef22, String dateToRef22, String acctschema22, String org22, String agnoRef22, String mProject22, String adOrgClient23, String adUserClient23, String dateFromRef23, String dateToRef23, String acctschema23, String org23, String agnoRef23, String mProject23, String adOrgClient24, String adUserClient24, String dateFromRef24, String dateToRef24, String acctschema24, String org24, String agnoRef24, String mProject24, String adOrgClient25, String adUserClient25, String dateFromRef25, String dateToRef25, String acctschema25, String org25, String agnoRef25, String mProject25, String adOrgClient26, String adUserClient26, String dateFromRef26, String dateToRef26, String acctschema26, String org26, String agnoRef26, String mProject26, String adOrgClient27, String adUserClient27, String dateFromRef27, String dateToRef27, String acctschema27, String org27, String agnoRef27, String mProject27, String adOrgClient28, String adUserClient28, String dateFromRef28, String dateToRef28, String acctschema28, String org28, String agnoRef28, String mProject28, String adOrgClient29, String adUserClient29, String dateFromRef29, String dateToRef29, String acctschema29, String org29, String agnoRef29, String mProject29)    throws ServletException {
    return selectAcct(connectionProvider, adOrgClient, adUserClient, dateFrom, dateTo, acctschema, org, agno, dateFromRef, dateToRef, agnoRef, adOrgClient0, adUserClient0, dateFromRef0, dateToRef0, acctschema0, org0, agnoRef0, adOrgClient1, adUserClient1, dateFromRef1, dateToRef1, acctschema1, org1, agnoRef1, mProject1, adOrgClient2, adUserClient2, dateFromRef2, dateToRef2, acctschema2, org2, agnoRef2, mProject2, adOrgClient3, adUserClient3, dateFromRef3, dateToRef3, acctschema3, org3, agnoRef3, mProject3, adOrgClient4, adUserClient4, dateFromRef4, dateToRef4, acctschema4, org4, agnoRef4, mProject4, adOrgClient5, adUserClient5, dateFromRef5, dateToRef5, acctschema5, org5, agnoRef5, mProject5, adOrgClient6, adUserClient6, dateFromRef6, dateToRef6, acctschema6, org6, agnoRef6, mProject6, adOrgClient7, adUserClient7, dateFromRef7, dateToRef7, acctschema7, org7, agnoRef7, mProject7, adOrgClient8, adUserClient8, dateFromRef8, dateToRef8, acctschema8, org8, agnoRef8, mProject8, adOrgClient9, adUserClient9, dateFromRef9, dateToRef9, acctschema9, org9, agnoRef9, mProject9, adOrgClient10, adUserClient10, dateFromRef10, dateToRef10, acctschema10, org10, agnoRef10, mProject10, adOrgClient11, adUserClient11, dateFromRef11, dateToRef11, acctschema11, org11, agnoRef11, mProject11, adOrgClient12, adUserClient12, dateFromRef12, dateToRef12, acctschema12, org12, agnoRef12, mProject12, adOrgClient13, adUserClient13, dateFromRef13, dateToRef13, acctschema13, org13, agnoRef13, mProject13, adOrgClient14, adUserClient14, dateFromRef14, dateToRef14, acctschema14, org14, agnoRef14, mProject14, adOrgClient15, adUserClient15, dateFromRef15, dateToRef15, acctschema15, org15, agnoRef15, mProject15, adOrgClient16, adUserClient16, dateFromRef16, dateToRef16, acctschema16, org16, agnoRef16, mProject16, adOrgClient17, adUserClient17, dateFromRef17, dateToRef17, acctschema17, org17, agnoRef17, mProject17, adOrgClient18, adUserClient18, dateFromRef18, dateToRef18, acctschema18, org18, agnoRef18, mProject18, adOrgClient19, adUserClient19, dateFromRef19, dateToRef19, acctschema19, org19, agnoRef19, mProject19, adOrgClient20, adUserClient20, dateFromRef20, dateToRef20, acctschema20, org20, agnoRef20, mProject20, adOrgClient21, adUserClient21, dateFromRef21, dateToRef21, acctschema21, org21, agnoRef21, mProject21, adOrgClient22, adUserClient22, dateFromRef22, dateToRef22, acctschema22, org22, agnoRef22, mProject22, adOrgClient23, adUserClient23, dateFromRef23, dateToRef23, acctschema23, org23, agnoRef23, mProject23, adOrgClient24, adUserClient24, dateFromRef24, dateToRef24, acctschema24, org24, agnoRef24, mProject24, adOrgClient25, adUserClient25, dateFromRef25, dateToRef25, acctschema25, org25, agnoRef25, mProject25, adOrgClient26, adUserClient26, dateFromRef26, dateToRef26, acctschema26, org26, agnoRef26, mProject26, adOrgClient27, adUserClient27, dateFromRef27, dateToRef27, acctschema27, org27, agnoRef27, mProject27, adOrgClient28, adUserClient28, dateFromRef28, dateToRef28, acctschema28, org28, agnoRef28, mProject28, adOrgClient29, adUserClient29, dateFromRef29, dateToRef29, acctschema29, org29, agnoRef29, mProject29, 0, 0);
  }

  public static AccountTreeProyectoData[] selectAcct(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, String adOrgClient0, String adUserClient0, String dateFromRef0, String dateToRef0, String acctschema0, String org0, String agnoRef0, String adOrgClient1, String adUserClient1, String dateFromRef1, String dateToRef1, String acctschema1, String org1, String agnoRef1, String mProject1, String adOrgClient2, String adUserClient2, String dateFromRef2, String dateToRef2, String acctschema2, String org2, String agnoRef2, String mProject2, String adOrgClient3, String adUserClient3, String dateFromRef3, String dateToRef3, String acctschema3, String org3, String agnoRef3, String mProject3, String adOrgClient4, String adUserClient4, String dateFromRef4, String dateToRef4, String acctschema4, String org4, String agnoRef4, String mProject4, String adOrgClient5, String adUserClient5, String dateFromRef5, String dateToRef5, String acctschema5, String org5, String agnoRef5, String mProject5, String adOrgClient6, String adUserClient6, String dateFromRef6, String dateToRef6, String acctschema6, String org6, String agnoRef6, String mProject6, String adOrgClient7, String adUserClient7, String dateFromRef7, String dateToRef7, String acctschema7, String org7, String agnoRef7, String mProject7, String adOrgClient8, String adUserClient8, String dateFromRef8, String dateToRef8, String acctschema8, String org8, String agnoRef8, String mProject8, String adOrgClient9, String adUserClient9, String dateFromRef9, String dateToRef9, String acctschema9, String org9, String agnoRef9, String mProject9, String adOrgClient10, String adUserClient10, String dateFromRef10, String dateToRef10, String acctschema10, String org10, String agnoRef10, String mProject10, String adOrgClient11, String adUserClient11, String dateFromRef11, String dateToRef11, String acctschema11, String org11, String agnoRef11, String mProject11, String adOrgClient12, String adUserClient12, String dateFromRef12, String dateToRef12, String acctschema12, String org12, String agnoRef12, String mProject12, String adOrgClient13, String adUserClient13, String dateFromRef13, String dateToRef13, String acctschema13, String org13, String agnoRef13, String mProject13, String adOrgClient14, String adUserClient14, String dateFromRef14, String dateToRef14, String acctschema14, String org14, String agnoRef14, String mProject14, String adOrgClient15, String adUserClient15, String dateFromRef15, String dateToRef15, String acctschema15, String org15, String agnoRef15, String mProject15, String adOrgClient16, String adUserClient16, String dateFromRef16, String dateToRef16, String acctschema16, String org16, String agnoRef16, String mProject16, String adOrgClient17, String adUserClient17, String dateFromRef17, String dateToRef17, String acctschema17, String org17, String agnoRef17, String mProject17, String adOrgClient18, String adUserClient18, String dateFromRef18, String dateToRef18, String acctschema18, String org18, String agnoRef18, String mProject18, String adOrgClient19, String adUserClient19, String dateFromRef19, String dateToRef19, String acctschema19, String org19, String agnoRef19, String mProject19, String adOrgClient20, String adUserClient20, String dateFromRef20, String dateToRef20, String acctschema20, String org20, String agnoRef20, String mProject20, String adOrgClient21, String adUserClient21, String dateFromRef21, String dateToRef21, String acctschema21, String org21, String agnoRef21, String mProject21, String adOrgClient22, String adUserClient22, String dateFromRef22, String dateToRef22, String acctschema22, String org22, String agnoRef22, String mProject22, String adOrgClient23, String adUserClient23, String dateFromRef23, String dateToRef23, String acctschema23, String org23, String agnoRef23, String mProject23, String adOrgClient24, String adUserClient24, String dateFromRef24, String dateToRef24, String acctschema24, String org24, String agnoRef24, String mProject24, String adOrgClient25, String adUserClient25, String dateFromRef25, String dateToRef25, String acctschema25, String org25, String agnoRef25, String mProject25, String adOrgClient26, String adUserClient26, String dateFromRef26, String dateToRef26, String acctschema26, String org26, String agnoRef26, String mProject26, String adOrgClient27, String adUserClient27, String dateFromRef27, String dateToRef27, String acctschema27, String org27, String agnoRef27, String mProject27, String adOrgClient28, String adUserClient28, String dateFromRef28, String dateToRef28, String acctschema28, String org28, String agnoRef28, String mProject28, String adOrgClient29, String adUserClient29, String dateFromRef29, String dateToRef29, String acctschema29, String org29, String agnoRef29, String mProject29, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ID, round(SUM(QTY),2) AS QTY, round(SUM(QTYCREDIT),2) AS QTYCREDIT, " +
      "        round(SUM(QTY_REF),2) AS QTY_REF, round(SUM(QTYCREDIT_REF),2) AS QTYCREDIT_REF," +
      "        round(SUM(qty_proy),2) AS qty_proy," +
      "        round(SUM(qty_proy1),2) AS qty_proy1, " +
      "        round(SUM(qty_proy2),2) AS qty_proy2, " +
      "        round(SUM(qty_proy3),2) AS qty_proy3, " +
      "        round(SUM(qty_proy4),2) AS qty_proy4, " +
      "        round(SUM(qty_proy5),2) AS qty_proy5, " +
      "        round(SUM(qty_proy6),2) AS qty_proy6, " +
      "        round(SUM(qty_proy7),2) AS qty_proy7, " +
      "        round(SUM(qty_proy8),2) AS qty_proy8, " +
      "        round(SUM(qty_proy9),2) AS qty_proy9," +
      "        round(SUM(qty_proy10),2) AS qty_proy10," +
      "        round(SUM(qty_proy11),2) AS qty_proy11, " +
      "        round(SUM(qty_proy12),2) AS qty_proy12," +
      "        round(SUM(qty_proy13),2) AS qty_proy13, " +
      "        round(SUM(qty_proy14),2) AS qty_proy14, " +
      "        round(SUM(qty_proy15),2) AS qty_proy15, " +
      "        round(SUM(qty_proy16),2) AS qty_proy16, " +
      "        round(SUM(qty_proy17),2) AS qty_proy17, " +
      "        round(SUM(qty_proy18),2) AS qty_proy18, " +
      "        round(SUM(qty_proy19),2) AS qty_proy19," +
      "        round(SUM(qty_proy20),2) AS qty_proy20," +
      "        round(SUM(qty_proy21),2) AS qty_proy21, " +
      "        round(SUM(qty_proy22),2) AS qty_proy22," +
      "        round(SUM(qty_proy23),2) AS qty_proy23, " +
      "        round(SUM(qty_proy24),2) AS qty_proy24, " +
      "        round(SUM(qty_proy25),2) AS qty_proy25, " +
      "        round(SUM(qty_proy26),2) AS qty_proy26, " +
      "        round(SUM(qty_proy27),2) AS qty_proy27, " +
      "        round(SUM(qty_proy28),2) AS qty_proy28, " +
      "        round(SUM(qty_proy29),2) AS qty_proy29," +
      "        round(SUM(qty_proy30),2) AS qty_proy30" +
      "        FROM (" +
      "        SELECT m.C_ElementValue_ID as id, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty," +
      "        (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                        0 as qty_proy," +
      "				        0 as qty_proy1,  0 as qty_proy2, " +
      "				        0 as qty_proy3,  0 as qty_proy4," +
      "				        0 as qty_proy5,  0 as qty_proy6, " +
      "				        0 as qty_proy7,  0 as qty_proy8, " +
      "				        0 as qty_proy9,  0 as qty_proy10, " +
      "				        0 as qty_proy11, 0 as qty_proy12," +
      "				        0 as qty_proy13, 0 as qty_proy14, " +
      "				        0 as qty_proy15, 0 as qty_proy16," +
      "				        0 as qty_proy17, 0 as qty_proy18, " +
      "				        0 as qty_proy19, 0 as qty_proy20, " +
      "				        0 as qty_proy21, 0 as qty_proy22, " +
      "				        0 as qty_proy23, 0 as qty_proy24," +
      "				        0 as qty_proy25, 0 as qty_proy26, " +
      "				        0 as qty_proy27, 0 as qty_proy28, " +
      "				        0 as qty_proy29, 0 as qty_proy30" +
      "            FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "           WHERE m.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "             AND m.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "             AND 1=1 ";
    strSql = strSql + ((dateFrom==null || dateFrom.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateTo==null || dateTo.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "             AND f.FACTACCTTYPE <> 'R'" +
      "             AND f.FACTACCTTYPE <> 'C'" +
      "             AND m.C_ElementValue_ID = f.Account_ID" +
      "         	 AND 0=0 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "         	 AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "        	 AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "        	 AND Y.YEAR IN (";
    strSql = strSql + ((agno==null || agno.equals(""))?"":agno);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS QTY, 0 as qtyCredit, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty_ref, " +
      "                (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1, 0 as qty_proy2," +
      "                0 as qty_proy3, 0 as qty_proy4," +
      "                0 as qty_proy5, 0 as qty_proy6," +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14," +
      "                0 as qty_proy15, 0 as qty_proy16," +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20," +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24," +
      "                0 as qty_proy25, 0 as qty_proy26," +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30 " +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "            AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "            AND 2=2 ";
    strSql = strSql + ((dateFromRef==null || dateFromRef.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef==null || dateToRef.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND 1=1 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (";
    strSql = strSql + ((agnoRef==null || agnoRef.equals(""))?"":agnoRef);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "        SELECT n.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy," +
      "                0 as qty_proy1,0 as qty_proy2, 0 as qty_proy3, 0 as qty_proy4, 0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, 0 as qty_proy9, 0 as qty_proy10, 0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, 0 as qty_proy15, 0 as qty_proy16, 0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, 0 as qty_proy21, 0 as qty_proy22, 0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, 0 as qty_proy27, 0 as qty_proy28, 0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n, Fact_Acct f, C_Period p, C_Year y " +
      "           WHERE n.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient0==null || adOrgClient0.equals(""))?"":adOrgClient0);
    strSql = strSql + 
      ") " +
      "            AND n.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient0==null || adUserClient0.equals(""))?"":adUserClient0);
    strSql = strSql + 
      ") " +
      "            AND 3=3 ";
    strSql = strSql + ((dateFromRef0==null || dateFromRef0.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef0==null || dateToRef0.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema0==null || acctschema0.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n.C_ElementValue_ID = f.Account_ID" +
      "            AND 4=4 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org0==null || org0.equals(""))?"":org0);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND (f.c_project_id = null or f.c_project_id is null)  " +
      "        UNION ALL" +
      "        SELECT n1.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy1," +
      "                0 as qty_proy2, 0 as qty_proy3, 0 as qty_proy4, 0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, 0 as qty_proy9, 0 as qty_proy10, 0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14,0 as qty_proy15, 0 as qty_proy16, 0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, 0 as qty_proy21, 0 as qty_proy22,0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, 0 as qty_proy27, 0 as qty_proy28, 0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n1, Fact_Acct f, C_Period p, C_Year y " +
      "           WHERE n1.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient1==null || adOrgClient1.equals(""))?"":adOrgClient1);
    strSql = strSql + 
      ") " +
      "            AND n1.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient1==null || adUserClient1.equals(""))?"":adUserClient1);
    strSql = strSql + 
      ") " +
      "            AND 5=5 ";
    strSql = strSql + ((dateFromRef1==null || dateFromRef1.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef1==null || dateToRef1.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema1==null || acctschema1.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n1.C_ElementValue_ID = f.Account_ID" +
      "            AND 6=6 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org1==null || org1.equals(""))?"":org1);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND f.c_project_id = ? " +
      "         UNION ALL" +
      "        SELECT o.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy, 0 as qty_proy1," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy2," +
      "                0 as qty_proy3, 0 as qty_proy4, 0 as qty_proy5, 0 as qty_proy6, 0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, 0 as qty_proy11, 0 as qty_proy12, 0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, 0 as qty_proy17, 0 as qty_proy18, 0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22, 0 as qty_proy23, 0 as qty_proy24, 0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, 0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue o, Fact_Acct f, C_Period p, C_Year y " +
      "           WHERE o.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient2==null || adOrgClient2.equals(""))?"":adOrgClient2);
    strSql = strSql + 
      ") " +
      "            AND o.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient2==null || adUserClient2.equals(""))?"":adUserClient2);
    strSql = strSql + 
      ") " +
      "            AND 7=7 ";
    strSql = strSql + ((dateFromRef2==null || dateFromRef2.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef2==null || dateToRef2.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema2==null || acctschema2.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND o.C_ElementValue_ID = f.Account_ID" +
      "            AND 8=8 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org2==null || org2.equals(""))?"":org2);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n2.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "        		0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy3," +
      "                0 as qty_proy4, " +
      "                0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n2, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n2.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient3==null || adOrgClient3.equals(""))?"":adOrgClient3);
    strSql = strSql + 
      ") " +
      "            AND n2.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient3==null || adUserClient3.equals(""))?"":adUserClient3);
    strSql = strSql + 
      ") " +
      "            AND 9=9 ";
    strSql = strSql + ((dateFromRef3==null || dateFromRef3.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef3==null || dateToRef3.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema3==null || acctschema3.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n2.C_ElementValue_ID = f.Account_ID" +
      "            AND 10=10 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org3==null || org3.equals(""))?"":org3);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND f.c_project_id = ? " +
      "        UNION ALL" +
      "        SELECT n4.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy4, " +
      "                0 as qty_proy5, 0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n4, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n4.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient4==null || adOrgClient4.equals(""))?"":adOrgClient4);
    strSql = strSql + 
      ") " +
      "            AND n4.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient4==null || adUserClient4.equals(""))?"":adUserClient4);
    strSql = strSql + 
      ") " +
      "            AND 11=11 ";
    strSql = strSql + ((dateFromRef4==null || dateFromRef4.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef4==null || dateToRef4.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema4==null || acctschema4.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n4.C_ElementValue_ID = f.Account_ID" +
      "            AND 12=12 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org4==null || org4.equals(""))?"":org4);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n5.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy5, " +
      "                0 as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n5, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n5.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient5==null || adOrgClient5.equals(""))?"":adOrgClient5);
    strSql = strSql + 
      ") " +
      "            AND n5.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient5==null || adUserClient5.equals(""))?"":adUserClient5);
    strSql = strSql + 
      ") " +
      "            AND 13=13 ";
    strSql = strSql + ((dateFromRef5==null || dateFromRef5.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef5==null || dateToRef5.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema5==null || acctschema5.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n5.C_ElementValue_ID = f.Account_ID" +
      "            AND 14=14 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org5==null || org5.equals(""))?"":org5);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n6.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy6, " +
      "                0 as qty_proy7, 0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n6, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n6.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient6==null || adOrgClient6.equals(""))?"":adOrgClient6);
    strSql = strSql + 
      ") " +
      "            AND n6.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient6==null || adUserClient6.equals(""))?"":adUserClient6);
    strSql = strSql + 
      ") " +
      "            AND 15=15 ";
    strSql = strSql + ((dateFromRef6==null || dateFromRef6.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef6==null || dateToRef6.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema6==null || acctschema6.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n6.C_ElementValue_ID = f.Account_ID" +
      "            AND 16=16 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org6==null || org6.equals(""))?"":org6);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)  " +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n7.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy7, " +
      "                0 as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n7, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n7.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient7==null || adOrgClient7.equals(""))?"":adOrgClient7);
    strSql = strSql + 
      ") " +
      "            AND n7.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient7==null || adUserClient7.equals(""))?"":adUserClient7);
    strSql = strSql + 
      ") " +
      "            AND 17=17 ";
    strSql = strSql + ((dateFromRef7==null || dateFromRef7.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef7==null || dateToRef7.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema7==null || acctschema7.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n7.C_ElementValue_ID = f.Account_ID" +
      "            AND 18=18 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org7==null || org7.equals(""))?"":org7);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n8.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy8, " +
      "                0 as qty_proy9, 0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n8, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n8.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient8==null || adOrgClient8.equals(""))?"":adOrgClient8);
    strSql = strSql + 
      ") " +
      "            AND n8.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient8==null || adUserClient8.equals(""))?"":adUserClient8);
    strSql = strSql + 
      ") " +
      "            AND 19=19 ";
    strSql = strSql + ((dateFromRef8==null || dateFromRef8.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef8==null || dateToRef8.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema8==null || acctschema8.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n8.C_ElementValue_ID = f.Account_ID" +
      "            AND 20=20 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org8==null || org8.equals(""))?"":org8);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n9.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy9, " +
      "                0 as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n9, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n9.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient9==null || adOrgClient9.equals(""))?"":adOrgClient9);
    strSql = strSql + 
      ") " +
      "            AND n9.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient9==null || adUserClient9.equals(""))?"":adUserClient9);
    strSql = strSql + 
      ") " +
      "            AND 21=21 ";
    strSql = strSql + ((dateFromRef9==null || dateFromRef9.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef9==null || dateToRef9.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema9==null || acctschema9.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n9.C_ElementValue_ID = f.Account_ID" +
      "            AND 22=22 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org9==null || org9.equals(""))?"":org9);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n10.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy10, " +
      "                0 as qty_proy11, 0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n10, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n10.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient10==null || adOrgClient10.equals(""))?"":adOrgClient10);
    strSql = strSql + 
      ") " +
      "            AND n10.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient10==null || adUserClient10.equals(""))?"":adUserClient10);
    strSql = strSql + 
      ") " +
      "            AND 23=23 ";
    strSql = strSql + ((dateFromRef10==null || dateFromRef10.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef10==null || dateToRef10.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema10==null || acctschema10.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n10.C_ElementValue_ID = f.Account_ID" +
      "            AND 24=24 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org10==null || org10.equals(""))?"":org10);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n11.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy11, " +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n11, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n11.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient11==null || adOrgClient11.equals(""))?"":adOrgClient11);
    strSql = strSql + 
      ") " +
      "            AND n11.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient11==null || adUserClient11.equals(""))?"":adUserClient11);
    strSql = strSql + 
      ") " +
      "            AND 25=25 ";
    strSql = strSql + ((dateFromRef11==null || dateFromRef11.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef11==null || dateToRef11.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema11==null || acctschema11.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n11.C_ElementValue_ID = f.Account_ID" +
      "            AND 26=26 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org11==null || org11.equals(""))?"":org11);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n12.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n12, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n12.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient12==null || adOrgClient12.equals(""))?"":adOrgClient12);
    strSql = strSql + 
      ") " +
      "            AND n12.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient12==null || adUserClient12.equals(""))?"":adUserClient12);
    strSql = strSql + 
      ") " +
      "            AND 27=27 ";
    strSql = strSql + ((dateFromRef12==null || dateFromRef12.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef12==null || dateToRef12.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema12==null || acctschema12.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n12.C_ElementValue_ID = f.Account_ID" +
      "            AND 28=28 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org12==null || org12.equals(""))?"":org12);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n13.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy13, " +
      "                0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n13, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n13.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient13==null || adOrgClient13.equals(""))?"":adOrgClient13);
    strSql = strSql + 
      ") " +
      "            AND n13.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient13==null || adUserClient13.equals(""))?"":adUserClient13);
    strSql = strSql + 
      ") " +
      "            AND 29=29 ";
    strSql = strSql + ((dateFromRef13==null || dateFromRef13.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef13==null || dateToRef13.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema13==null || acctschema13.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n13.C_ElementValue_ID = f.Account_ID" +
      "            AND 30=30 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org13==null || org13.equals(""))?"":org13);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n14.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n14, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n14.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient14==null || adOrgClient14.equals(""))?"":adOrgClient14);
    strSql = strSql + 
      ") " +
      "            AND n14.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient14==null || adUserClient14.equals(""))?"":adUserClient14);
    strSql = strSql + 
      ") " +
      "            AND 31=31 ";
    strSql = strSql + ((dateFromRef14==null || dateFromRef14.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef14==null || dateToRef14.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema14==null || acctschema14.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n14.C_ElementValue_ID = f.Account_ID" +
      "            AND 32=32 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org14==null || org14.equals(""))?"":org14);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n15.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy15, " +
      "                0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n15, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n15.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient15==null || adOrgClient15.equals(""))?"":adOrgClient15);
    strSql = strSql + 
      ") " +
      "            AND n15.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient15==null || adUserClient15.equals(""))?"":adUserClient15);
    strSql = strSql + 
      ") " +
      "            AND 33=33 ";
    strSql = strSql + ((dateFromRef15==null || dateFromRef15.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef15==null || dateToRef15.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema15==null || acctschema15.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n15.C_ElementValue_ID = f.Account_ID" +
      "            AND 34=34 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org15==null || org15.equals(""))?"":org15);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n16.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n16, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n16.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient16==null || adOrgClient16.equals(""))?"":adOrgClient16);
    strSql = strSql + 
      ") " +
      "            AND n16.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient16==null || adUserClient16.equals(""))?"":adUserClient16);
    strSql = strSql + 
      ") " +
      "            AND 35=35 ";
    strSql = strSql + ((dateFromRef16==null || dateFromRef16.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef16==null || dateToRef16.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema16==null || acctschema16.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n16.C_ElementValue_ID = f.Account_ID" +
      "            AND 36=36 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org16==null || org16.equals(""))?"":org16);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n17.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy17, " +
      "                0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n17, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n17.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient17==null || adOrgClient17.equals(""))?"":adOrgClient17);
    strSql = strSql + 
      ") " +
      "            AND n17.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient17==null || adUserClient17.equals(""))?"":adUserClient17);
    strSql = strSql + 
      ") " +
      "            AND 37=37 ";
    strSql = strSql + ((dateFromRef17==null || dateFromRef17.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef17==null || dateToRef17.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema17==null || acctschema17.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n17.C_ElementValue_ID = f.Account_ID" +
      "            AND 38=38 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org17==null || org17.equals(""))?"":org17);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n18.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n18, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n18.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient18==null || adOrgClient18.equals(""))?"":adOrgClient18);
    strSql = strSql + 
      ") " +
      "            AND n18.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient18==null || adUserClient18.equals(""))?"":adUserClient18);
    strSql = strSql + 
      ") " +
      "            AND 39=39 ";
    strSql = strSql + ((dateFromRef18==null || dateFromRef18.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef18==null || dateToRef18.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema18==null || acctschema18.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n18.C_ElementValue_ID = f.Account_ID" +
      "            AND 40=40 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org18==null || org18.equals(""))?"":org18);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n19.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy19," +
      "                0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n19, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n19.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient19==null || adOrgClient19.equals(""))?"":adOrgClient19);
    strSql = strSql + 
      ") " +
      "            AND n19.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient19==null || adUserClient19.equals(""))?"":adUserClient19);
    strSql = strSql + 
      ") " +
      "            AND 41=41 ";
    strSql = strSql + ((dateFromRef19==null || dateFromRef19.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef19==null || dateToRef19.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema19==null || acctschema19.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n19.C_ElementValue_ID = f.Account_ID" +
      "            AND 42=42 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org19==null || org19.equals(""))?"":org19);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "         SELECT n20.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n20, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n20.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient20==null || adOrgClient20.equals(""))?"":adOrgClient20);
    strSql = strSql + 
      ") " +
      "            AND n20.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient20==null || adUserClient20.equals(""))?"":adUserClient20);
    strSql = strSql + 
      ") " +
      "            AND 43=43 ";
    strSql = strSql + ((dateFromRef20==null || dateFromRef20.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef20==null || dateToRef20.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema20==null || acctschema20.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n20.C_ElementValue_ID = f.Account_ID" +
      "            AND 44=44 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org20==null || org20.equals(""))?"":org20);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "             UNION ALL" +
      "        SELECT n21.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy21, " +
      "                0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n21, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n21.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient21==null || adOrgClient21.equals(""))?"":adOrgClient21);
    strSql = strSql + 
      ") " +
      "            AND n21.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient21==null || adUserClient21.equals(""))?"":adUserClient21);
    strSql = strSql + 
      ") " +
      "            AND 45=45 ";
    strSql = strSql + ((dateFromRef21==null || dateFromRef21.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef21==null || dateToRef21.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema21==null || acctschema21.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n21.C_ElementValue_ID = f.Account_ID" +
      "            AND 46=46 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org21==null || org21.equals(""))?"":org21);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n22.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n22, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n22.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient22==null || adOrgClient22.equals(""))?"":adOrgClient22);
    strSql = strSql + 
      ") " +
      "            AND n22.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient22==null || adUserClient22.equals(""))?"":adUserClient22);
    strSql = strSql + 
      ") " +
      "            AND 47=47 ";
    strSql = strSql + ((dateFromRef22==null || dateFromRef22.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef22==null || dateToRef22.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema22==null || acctschema22.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n22.C_ElementValue_ID = f.Account_ID" +
      "            AND 48=48 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org22==null || org22.equals(""))?"":org22);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n23.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy23, " +
      "                0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n23, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n23.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient23==null || adOrgClient23.equals(""))?"":adOrgClient23);
    strSql = strSql + 
      ") " +
      "            AND n23.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient23==null || adUserClient23.equals(""))?"":adUserClient23);
    strSql = strSql + 
      ") " +
      "            AND 49=49 ";
    strSql = strSql + ((dateFromRef23==null || dateFromRef23.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef23==null || dateToRef23.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema23==null || acctschema23.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n23.C_ElementValue_ID = f.Account_ID" +
      "            AND 50=50 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org23==null || org23.equals(""))?"":org23);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n24.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n24, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n24.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient24==null || adOrgClient24.equals(""))?"":adOrgClient24);
    strSql = strSql + 
      ") " +
      "            AND n24.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient24==null || adUserClient24.equals(""))?"":adUserClient24);
    strSql = strSql + 
      ") " +
      "            AND 51=51 ";
    strSql = strSql + ((dateFromRef24==null || dateFromRef24.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef24==null || dateToRef24.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema24==null || acctschema24.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n24.C_ElementValue_ID = f.Account_ID" +
      "            AND 52=52 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org24==null || org24.equals(""))?"":org24);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n25.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy25, " +
      "                0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n25, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n25.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient25==null || adOrgClient25.equals(""))?"":adOrgClient25);
    strSql = strSql + 
      ") " +
      "            AND n25.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient25==null || adUserClient25.equals(""))?"":adUserClient25);
    strSql = strSql + 
      ") " +
      "            AND 53=53 ";
    strSql = strSql + ((dateFromRef25==null || dateFromRef25.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef25==null || dateToRef25.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema25==null || acctschema25.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n25.C_ElementValue_ID = f.Account_ID" +
      "            AND 54=54 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org25==null || org25.equals(""))?"":org25);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n26.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n26, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n26.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient26==null || adOrgClient26.equals(""))?"":adOrgClient26);
    strSql = strSql + 
      ") " +
      "            AND n26.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient26==null || adUserClient26.equals(""))?"":adUserClient26);
    strSql = strSql + 
      ") " +
      "            AND 55=55 ";
    strSql = strSql + ((dateFromRef26==null || dateFromRef26.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef26==null || dateToRef26.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema26==null || acctschema26.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n26.C_ElementValue_ID = f.Account_ID" +
      "            AND 56=56 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org26==null || org26.equals(""))?"":org26);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n27.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy27, " +
      "                0 as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n27, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n27.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient27==null || adOrgClient27.equals(""))?"":adOrgClient27);
    strSql = strSql + 
      ") " +
      "            AND n27.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient27==null || adUserClient27.equals(""))?"":adUserClient27);
    strSql = strSql + 
      ") " +
      "            AND 57=57 ";
    strSql = strSql + ((dateFromRef27==null || dateFromRef27.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef27==null || dateToRef27.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema27==null || acctschema27.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n27.C_ElementValue_ID = f.Account_ID" +
      "            AND 58=58 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org27==null || org27.equals(""))?"":org27);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "           UNION ALL" +
      "        SELECT n28.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy28, " +
      "                0 as qty_proy29, 0 as qty_proy30" +
      "           FROM C_ElementValue n28, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n28.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient28==null || adOrgClient28.equals(""))?"":adOrgClient28);
    strSql = strSql + 
      ") " +
      "            AND n28.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient28==null || adUserClient28.equals(""))?"":adUserClient28);
    strSql = strSql + 
      ") " +
      "            AND 59=59 ";
    strSql = strSql + ((dateFromRef28==null || dateFromRef28.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef28==null || dateToRef28.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema28==null || acctschema28.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n28.C_ElementValue_ID = f.Account_ID" +
      "            AND 60=60 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org28==null || org28.equals(""))?"":org28);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?" +
      "        UNION ALL" +
      "        SELECT n29.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_proy," +
      "                0 as qty_proy1," +
      "                0 as qty_proy2," +
      "                0 as qty_proy3," +
      "                0 as qty_proy4," +
      "                0 as qty_proy5," +
      "                0 as qty_proy6," +
      "                0 as qty_proy7," +
      "                0 as qty_proy8," +
      "                0 as qty_proy9," +
      "                0 as qty_proy10," +
      "                0 as qty_proy11," +
      "                0 as qty_proy12," +
      "                0 as qty_proy13, 0 as qty_proy14, " +
      "                0 as qty_proy15, 0 as qty_proy16, " +
      "                0 as qty_proy17, 0 as qty_proy18, " +
      "                0 as qty_proy19, 0 as qty_proy20, " +
      "                0 as qty_proy21, 0 as qty_proy22," +
      "                0 as qty_proy23, 0 as qty_proy24, " +
      "                0 as qty_proy25, 0 as qty_proy26, " +
      "                0 as qty_proy27, 0 as qty_proy28, " +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_proy29, " +
      "                0 as qty_proy30" +
      "           FROM C_ElementValue n29, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE n29.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient29==null || adOrgClient29.equals(""))?"":adOrgClient29);
    strSql = strSql + 
      ") " +
      "            AND n29.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient29==null || adUserClient29.equals(""))?"":adUserClient29);
    strSql = strSql + 
      ") " +
      "            AND 61=61 ";
    strSql = strSql + ((dateFromRef29==null || dateFromRef29.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef29==null || dateToRef29.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema29==null || acctschema29.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND n29.C_ElementValue_ID = f.Account_ID" +
      "            AND 62=62 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org29==null || org29.equals(""))?"":org29);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (?)" +
      "            AND f.c_project_id = ?      " +
      "        ) AA" +
      "        GROUP BY ID";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFrom != null && !(dateFrom.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFrom);
      }
      if (dateTo != null && !(dateTo.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTo);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agno != null && !(agno.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFromRef != null && !(dateFromRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef);
      }
      if (dateToRef != null && !(dateToRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agnoRef != null && !(agnoRef.equals(""))) {
        }
      if (adOrgClient0 != null && !(adOrgClient0.equals(""))) {
        }
      if (adUserClient0 != null && !(adUserClient0.equals(""))) {
        }
      if (dateFromRef0 != null && !(dateFromRef0.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef0);
      }
      if (dateToRef0 != null && !(dateToRef0.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef0);
      }
      if (acctschema0 != null && !(acctschema0.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema0);
      }
      if (org0 != null && !(org0.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef0);
      if (adOrgClient1 != null && !(adOrgClient1.equals(""))) {
        }
      if (adUserClient1 != null && !(adUserClient1.equals(""))) {
        }
      if (dateFromRef1 != null && !(dateFromRef1.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef1);
      }
      if (dateToRef1 != null && !(dateToRef1.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef1);
      }
      if (acctschema1 != null && !(acctschema1.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema1);
      }
      if (org1 != null && !(org1.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject1);
      if (adOrgClient2 != null && !(adOrgClient2.equals(""))) {
        }
      if (adUserClient2 != null && !(adUserClient2.equals(""))) {
        }
      if (dateFromRef2 != null && !(dateFromRef2.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef2);
      }
      if (dateToRef2 != null && !(dateToRef2.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef2);
      }
      if (acctschema2 != null && !(acctschema2.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema2);
      }
      if (org2 != null && !(org2.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject2);
      if (adOrgClient3 != null && !(adOrgClient3.equals(""))) {
        }
      if (adUserClient3 != null && !(adUserClient3.equals(""))) {
        }
      if (dateFromRef3 != null && !(dateFromRef3.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef3);
      }
      if (dateToRef3 != null && !(dateToRef3.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef3);
      }
      if (acctschema3 != null && !(acctschema3.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema3);
      }
      if (org3 != null && !(org3.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject3);
      if (adOrgClient4 != null && !(adOrgClient4.equals(""))) {
        }
      if (adUserClient4 != null && !(adUserClient4.equals(""))) {
        }
      if (dateFromRef4 != null && !(dateFromRef4.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef4);
      }
      if (dateToRef4 != null && !(dateToRef4.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef4);
      }
      if (acctschema4 != null && !(acctschema4.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema4);
      }
      if (org4 != null && !(org4.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject4);
      if (adOrgClient5 != null && !(adOrgClient5.equals(""))) {
        }
      if (adUserClient5 != null && !(adUserClient5.equals(""))) {
        }
      if (dateFromRef5 != null && !(dateFromRef5.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef5);
      }
      if (dateToRef5 != null && !(dateToRef5.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef5);
      }
      if (acctschema5 != null && !(acctschema5.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema5);
      }
      if (org5 != null && !(org5.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject5);
      if (adOrgClient6 != null && !(adOrgClient6.equals(""))) {
        }
      if (adUserClient6 != null && !(adUserClient6.equals(""))) {
        }
      if (dateFromRef6 != null && !(dateFromRef6.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef6);
      }
      if (dateToRef6 != null && !(dateToRef6.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef6);
      }
      if (acctschema6 != null && !(acctschema6.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema6);
      }
      if (org6 != null && !(org6.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject6);
      if (adOrgClient7 != null && !(adOrgClient7.equals(""))) {
        }
      if (adUserClient7 != null && !(adUserClient7.equals(""))) {
        }
      if (dateFromRef7 != null && !(dateFromRef7.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef7);
      }
      if (dateToRef7 != null && !(dateToRef7.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef7);
      }
      if (acctschema7 != null && !(acctschema7.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema7);
      }
      if (org7 != null && !(org7.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject7);
      if (adOrgClient8 != null && !(adOrgClient8.equals(""))) {
        }
      if (adUserClient8 != null && !(adUserClient8.equals(""))) {
        }
      if (dateFromRef8 != null && !(dateFromRef8.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef8);
      }
      if (dateToRef8 != null && !(dateToRef8.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef8);
      }
      if (acctschema8 != null && !(acctschema8.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema8);
      }
      if (org8 != null && !(org8.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject8);
      if (adOrgClient9 != null && !(adOrgClient9.equals(""))) {
        }
      if (adUserClient9 != null && !(adUserClient9.equals(""))) {
        }
      if (dateFromRef9 != null && !(dateFromRef9.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef9);
      }
      if (dateToRef9 != null && !(dateToRef9.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef9);
      }
      if (acctschema9 != null && !(acctschema9.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema9);
      }
      if (org9 != null && !(org9.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject9);
      if (adOrgClient10 != null && !(adOrgClient10.equals(""))) {
        }
      if (adUserClient10 != null && !(adUserClient10.equals(""))) {
        }
      if (dateFromRef10 != null && !(dateFromRef10.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef10);
      }
      if (dateToRef10 != null && !(dateToRef10.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef10);
      }
      if (acctschema10 != null && !(acctschema10.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema10);
      }
      if (org10 != null && !(org10.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject10);
      if (adOrgClient11 != null && !(adOrgClient11.equals(""))) {
        }
      if (adUserClient11 != null && !(adUserClient11.equals(""))) {
        }
      if (dateFromRef11 != null && !(dateFromRef11.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef11);
      }
      if (dateToRef11 != null && !(dateToRef11.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef11);
      }
      if (acctschema11 != null && !(acctschema11.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema11);
      }
      if (org11 != null && !(org11.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject11);
      if (adOrgClient12 != null && !(adOrgClient12.equals(""))) {
        }
      if (adUserClient12 != null && !(adUserClient12.equals(""))) {
        }
      if (dateFromRef12 != null && !(dateFromRef12.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef12);
      }
      if (dateToRef12 != null && !(dateToRef12.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef12);
      }
      if (acctschema12 != null && !(acctschema12.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema12);
      }
      if (org12 != null && !(org12.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject12);
      if (adOrgClient13 != null && !(adOrgClient13.equals(""))) {
        }
      if (adUserClient13 != null && !(adUserClient13.equals(""))) {
        }
      if (dateFromRef13 != null && !(dateFromRef13.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef13);
      }
      if (dateToRef13 != null && !(dateToRef13.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef13);
      }
      if (acctschema13 != null && !(acctschema13.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema13);
      }
      if (org13 != null && !(org13.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef13);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject13);
      if (adOrgClient14 != null && !(adOrgClient14.equals(""))) {
        }
      if (adUserClient14 != null && !(adUserClient14.equals(""))) {
        }
      if (dateFromRef14 != null && !(dateFromRef14.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef14);
      }
      if (dateToRef14 != null && !(dateToRef14.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef14);
      }
      if (acctschema14 != null && !(acctschema14.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema14);
      }
      if (org14 != null && !(org14.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef14);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject14);
      if (adOrgClient15 != null && !(adOrgClient15.equals(""))) {
        }
      if (adUserClient15 != null && !(adUserClient15.equals(""))) {
        }
      if (dateFromRef15 != null && !(dateFromRef15.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef15);
      }
      if (dateToRef15 != null && !(dateToRef15.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef15);
      }
      if (acctschema15 != null && !(acctschema15.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema15);
      }
      if (org15 != null && !(org15.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef15);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject15);
      if (adOrgClient16 != null && !(adOrgClient16.equals(""))) {
        }
      if (adUserClient16 != null && !(adUserClient16.equals(""))) {
        }
      if (dateFromRef16 != null && !(dateFromRef16.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef16);
      }
      if (dateToRef16 != null && !(dateToRef16.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef16);
      }
      if (acctschema16 != null && !(acctschema16.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema16);
      }
      if (org16 != null && !(org16.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef16);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject16);
      if (adOrgClient17 != null && !(adOrgClient17.equals(""))) {
        }
      if (adUserClient17 != null && !(adUserClient17.equals(""))) {
        }
      if (dateFromRef17 != null && !(dateFromRef17.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef17);
      }
      if (dateToRef17 != null && !(dateToRef17.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef17);
      }
      if (acctschema17 != null && !(acctschema17.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema17);
      }
      if (org17 != null && !(org17.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef17);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject17);
      if (adOrgClient18 != null && !(adOrgClient18.equals(""))) {
        }
      if (adUserClient18 != null && !(adUserClient18.equals(""))) {
        }
      if (dateFromRef18 != null && !(dateFromRef18.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef18);
      }
      if (dateToRef18 != null && !(dateToRef18.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef18);
      }
      if (acctschema18 != null && !(acctschema18.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema18);
      }
      if (org18 != null && !(org18.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef18);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject18);
      if (adOrgClient19 != null && !(adOrgClient19.equals(""))) {
        }
      if (adUserClient19 != null && !(adUserClient19.equals(""))) {
        }
      if (dateFromRef19 != null && !(dateFromRef19.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef19);
      }
      if (dateToRef19 != null && !(dateToRef19.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef19);
      }
      if (acctschema19 != null && !(acctschema19.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema19);
      }
      if (org19 != null && !(org19.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef19);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject19);
      if (adOrgClient20 != null && !(adOrgClient20.equals(""))) {
        }
      if (adUserClient20 != null && !(adUserClient20.equals(""))) {
        }
      if (dateFromRef20 != null && !(dateFromRef20.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef20);
      }
      if (dateToRef20 != null && !(dateToRef20.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef20);
      }
      if (acctschema20 != null && !(acctschema20.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema20);
      }
      if (org20 != null && !(org20.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef20);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject20);
      if (adOrgClient21 != null && !(adOrgClient21.equals(""))) {
        }
      if (adUserClient21 != null && !(adUserClient21.equals(""))) {
        }
      if (dateFromRef21 != null && !(dateFromRef21.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef21);
      }
      if (dateToRef21 != null && !(dateToRef21.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef21);
      }
      if (acctschema21 != null && !(acctschema21.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema21);
      }
      if (org21 != null && !(org21.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef21);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject21);
      if (adOrgClient22 != null && !(adOrgClient22.equals(""))) {
        }
      if (adUserClient22 != null && !(adUserClient22.equals(""))) {
        }
      if (dateFromRef22 != null && !(dateFromRef22.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef22);
      }
      if (dateToRef22 != null && !(dateToRef22.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef22);
      }
      if (acctschema22 != null && !(acctschema22.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema22);
      }
      if (org22 != null && !(org22.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef22);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject22);
      if (adOrgClient23 != null && !(adOrgClient23.equals(""))) {
        }
      if (adUserClient23 != null && !(adUserClient23.equals(""))) {
        }
      if (dateFromRef23 != null && !(dateFromRef23.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef23);
      }
      if (dateToRef23 != null && !(dateToRef23.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef23);
      }
      if (acctschema23 != null && !(acctschema23.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema23);
      }
      if (org23 != null && !(org23.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef23);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject23);
      if (adOrgClient24 != null && !(adOrgClient24.equals(""))) {
        }
      if (adUserClient24 != null && !(adUserClient24.equals(""))) {
        }
      if (dateFromRef24 != null && !(dateFromRef24.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef24);
      }
      if (dateToRef24 != null && !(dateToRef24.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef24);
      }
      if (acctschema24 != null && !(acctschema24.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema24);
      }
      if (org24 != null && !(org24.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef24);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject24);
      if (adOrgClient25 != null && !(adOrgClient25.equals(""))) {
        }
      if (adUserClient25 != null && !(adUserClient25.equals(""))) {
        }
      if (dateFromRef25 != null && !(dateFromRef25.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef25);
      }
      if (dateToRef25 != null && !(dateToRef25.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef25);
      }
      if (acctschema25 != null && !(acctschema25.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema25);
      }
      if (org25 != null && !(org25.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef25);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject25);
      if (adOrgClient26 != null && !(adOrgClient26.equals(""))) {
        }
      if (adUserClient26 != null && !(adUserClient26.equals(""))) {
        }
      if (dateFromRef26 != null && !(dateFromRef26.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef26);
      }
      if (dateToRef26 != null && !(dateToRef26.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef26);
      }
      if (acctschema26 != null && !(acctschema26.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema26);
      }
      if (org26 != null && !(org26.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef26);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject26);
      if (adOrgClient27 != null && !(adOrgClient27.equals(""))) {
        }
      if (adUserClient27 != null && !(adUserClient27.equals(""))) {
        }
      if (dateFromRef27 != null && !(dateFromRef27.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef27);
      }
      if (dateToRef27 != null && !(dateToRef27.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef27);
      }
      if (acctschema27 != null && !(acctschema27.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema27);
      }
      if (org27 != null && !(org27.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef27);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject27);
      if (adOrgClient28 != null && !(adOrgClient28.equals(""))) {
        }
      if (adUserClient28 != null && !(adUserClient28.equals(""))) {
        }
      if (dateFromRef28 != null && !(dateFromRef28.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef28);
      }
      if (dateToRef28 != null && !(dateToRef28.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef28);
      }
      if (acctschema28 != null && !(acctschema28.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema28);
      }
      if (org28 != null && !(org28.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef28);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject28);
      if (adOrgClient29 != null && !(adOrgClient29.equals(""))) {
        }
      if (adUserClient29 != null && !(adUserClient29.equals(""))) {
        }
      if (dateFromRef29 != null && !(dateFromRef29.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef29);
      }
      if (dateToRef29 != null && !(dateToRef29.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef29);
      }
      if (acctschema29 != null && !(acctschema29.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema29);
      }
      if (org29 != null && !(org29.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agnoRef29);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProject29);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.id = UtilSql.getValue(result, "id");
        objectAccountTreeProyectoData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeProyectoData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeProyectoData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeProyectoData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeProyectoData.qtyProy = UtilSql.getValue(result, "qty_proy");
        objectAccountTreeProyectoData.qtyProy1 = UtilSql.getValue(result, "qty_proy1");
        objectAccountTreeProyectoData.qtyProy2 = UtilSql.getValue(result, "qty_proy2");
        objectAccountTreeProyectoData.qtyProy3 = UtilSql.getValue(result, "qty_proy3");
        objectAccountTreeProyectoData.qtyProy4 = UtilSql.getValue(result, "qty_proy4");
        objectAccountTreeProyectoData.qtyProy5 = UtilSql.getValue(result, "qty_proy5");
        objectAccountTreeProyectoData.qtyProy6 = UtilSql.getValue(result, "qty_proy6");
        objectAccountTreeProyectoData.qtyProy7 = UtilSql.getValue(result, "qty_proy7");
        objectAccountTreeProyectoData.qtyProy8 = UtilSql.getValue(result, "qty_proy8");
        objectAccountTreeProyectoData.qtyProy9 = UtilSql.getValue(result, "qty_proy9");
        objectAccountTreeProyectoData.qtyProy10 = UtilSql.getValue(result, "qty_proy10");
        objectAccountTreeProyectoData.qtyProy11 = UtilSql.getValue(result, "qty_proy11");
        objectAccountTreeProyectoData.qtyProy12 = UtilSql.getValue(result, "qty_proy12");
        objectAccountTreeProyectoData.qtyProy13 = UtilSql.getValue(result, "qty_proy13");
        objectAccountTreeProyectoData.qtyProy14 = UtilSql.getValue(result, "qty_proy14");
        objectAccountTreeProyectoData.qtyProy15 = UtilSql.getValue(result, "qty_proy15");
        objectAccountTreeProyectoData.qtyProy16 = UtilSql.getValue(result, "qty_proy16");
        objectAccountTreeProyectoData.qtyProy17 = UtilSql.getValue(result, "qty_proy17");
        objectAccountTreeProyectoData.qtyProy18 = UtilSql.getValue(result, "qty_proy18");
        objectAccountTreeProyectoData.qtyProy19 = UtilSql.getValue(result, "qty_proy19");
        objectAccountTreeProyectoData.qtyProy20 = UtilSql.getValue(result, "qty_proy20");
        objectAccountTreeProyectoData.qtyProy21 = UtilSql.getValue(result, "qty_proy21");
        objectAccountTreeProyectoData.qtyProy22 = UtilSql.getValue(result, "qty_proy22");
        objectAccountTreeProyectoData.qtyProy23 = UtilSql.getValue(result, "qty_proy23");
        objectAccountTreeProyectoData.qtyProy24 = UtilSql.getValue(result, "qty_proy24");
        objectAccountTreeProyectoData.qtyProy25 = UtilSql.getValue(result, "qty_proy25");
        objectAccountTreeProyectoData.qtyProy26 = UtilSql.getValue(result, "qty_proy26");
        objectAccountTreeProyectoData.qtyProy27 = UtilSql.getValue(result, "qty_proy27");
        objectAccountTreeProyectoData.qtyProy28 = UtilSql.getValue(result, "qty_proy28");
        objectAccountTreeProyectoData.qtyProy29 = UtilSql.getValue(result, "qty_proy29");
        objectAccountTreeProyectoData.qtyProy30 = UtilSql.getValue(result, "qty_proy30");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectForms(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient)    throws ServletException {
    return selectForms(connectionProvider, adOrgClient, adUserClient, 0, 0);
  }

  public static AccountTreeProyectoData[] selectForms(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT m.C_ElementValue_ID as id, o.account_id as node_id, o.sign as ACCOUNTSIGN" +
      "        FROM C_ElementValue m, C_ELEMENTVALUE_OPERAND o  " +
      "        WHERE m.isActive='Y' " +
      "        AND m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND m.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND o.isactive = 'Y' " +
      "        order by m.C_elementvalue_id, o.seqno";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.id = UtilSql.getValue(result, "id");
        objectAccountTreeProyectoData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeProyectoData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectOperands(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String element)    throws ServletException {
    return selectOperands(connectionProvider, adOrgClient, adUserClient, element, 0, 0);
  }

  public static AccountTreeProyectoData[] selectOperands(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String element, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT m.C_ElementValue_ID as id, o.account_id as node_id, o.sign" +
      "        FROM C_ElementValue m, C_ELEMENTVALUE_OPERAND o, C_ElementValue n" +
      "        WHERE m.isActive='Y' " +
      "        AND m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND m.C_Element_ID = ?" +
      "        AND m.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND n.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND o.isactive = 'Y' " +
      "        order by m.C_elementvalue_id, o.seqno";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, element);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.id = UtilSql.getValue(result, "id");
        objectAccountTreeProyectoData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeProyectoData.sign = UtilSql.getValue(result, "sign");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectRangoFechas(ConnectionProvider connectionProvider, String cYearId)    throws ServletException {
    return selectRangoFechas(connectionProvider, cYearId, 0, 0);
  }

  public static AccountTreeProyectoData[] selectRangoFechas(ConnectionProvider connectionProvider, String cYearId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "           SELECT min(P.STARTDATE) as fecha_inicio," +
      "		          max(P.ENDDATE) as fecha_fin" +
      "		     FROM c_period P" +
      "		    WHERE P.periodtype = 'S'" +
      "		      AND P.c_year_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cYearId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectAccountTreeProyectoData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectFechasAnio(ConnectionProvider connectionProvider, String adClientId, String year)    throws ServletException {
    return selectFechasAnio(connectionProvider, adClientId, year, 0, 0);
  }

  public static AccountTreeProyectoData[] selectFechasAnio(ConnectionProvider connectionProvider, String adClientId, String year, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "           select min(a.startdate) as fecha_inicio," +
      "		          max(a.enddate) as fecha_fin" +
      "		     from c_period a, c_year b" +
      "		    where b.ad_client_id = ?" +
      "		      and a.c_year_id = b.c_year_id" +
      "		      and b.year = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, year);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectAccountTreeProyectoData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }

  public static AccountTreeProyectoData[] selectProyectos(ConnectionProvider connectionProvider, String adClientId, String fechaInicial, String fechaFinal)    throws ServletException {
    return selectProyectos(connectionProvider, adClientId, fechaInicial, fechaFinal, 0, 0);
  }

  public static AccountTreeProyectoData[] selectProyectos(ConnectionProvider connectionProvider, String adClientId, String fechaInicial, String fechaFinal, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "           select distinct(a.c_project_id) as proyecto," +
      "                  b.name " +
      "			 from fact_acct a, c_project b" +
      "			where a.ad_client_id = ?" +
      "			  and date(a.dateacct) between date(?) and date(?)" +
      "			  and a.c_project_id is not null" +
      "			  and a.c_project_id = b.c_project_id" +
      "			order by 2";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicial);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFinal);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeProyectoData objectAccountTreeProyectoData = new AccountTreeProyectoData();
        objectAccountTreeProyectoData.proyecto = UtilSql.getValue(result, "proyecto");
        objectAccountTreeProyectoData.name = UtilSql.getValue(result, "name");
        objectAccountTreeProyectoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeProyectoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeProyectoData objectAccountTreeProyectoData[] = new AccountTreeProyectoData[vector.size()];
    vector.copyInto(objectAccountTreeProyectoData);
    return(objectAccountTreeProyectoData);
  }
}
