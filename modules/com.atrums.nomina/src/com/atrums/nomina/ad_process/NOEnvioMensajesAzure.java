package com.atrums.nomina.ad_process;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;

public class NOEnvioMensajesAzure implements Process {
	static Logger log = Logger.getLogger(NOEnvioMensajesAzure.class);
	final OBError msg = new OBError();

	public static final String connectionString = "DefaultEndpointsProtocol=https;AccountName=almacencolasgesth;AccountKey=yZftjNM9m6LkY/QGX2C06nY6hzGSvENTxBvjSoO3bkN0ju1rhSkC8fxu1HdN9bjzjBgzML0Rx1CbdgOi51g1FA==;EndpointSuffix=core.windows.net";
	//public static final String connectionString = "DefaultEndpointsProtocol=https;AccountName=almacencolas;AccountKey=lMIvzXDvMVhpJNN8DYKGKcdDsrw5tia7yHOi4vtmfRaS4cLKv/lBczfS/uAeLlTekn8V8bZK+XOLfDznLhNHoQ==;EndpointSuffix=core.windows.net";
	
	@Override
	public void execute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub
		ConnectionProvider conn = bundle.getConnection();
		VariablesSecureApp varsAux = bundle.getContext().toVars();

		OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(), varsAux.getOrg());

		NOEnvioMensajesAzureData[] axmldocumentos;

		String msj = null;
		boolean envio = false;

		try {

			//Envio de contratos
			axmldocumentos = null;
			axmldocumentos = NOEnvioMensajesAzureData.methodBuscarContratosNuevos(conn, varsAux.getClient());

			for (int i = 0; i < axmldocumentos.length; i++) {
				if (!axmldocumentos[i].dato1.equals("")) {

					msj = "{\r\n" + 
							"  \"usuarios\": [\r\n" + 
							"    {\r\n" + 
							"      \"tipoAccion\": \""+ axmldocumentos[i].dato1 +"\",\r\n" + 
							"      \"empresa\": \""+ axmldocumentos[i].dato2 +"\",\r\n" +
							"      \"codigoColaborador\": \""+ axmldocumentos[i].dato3 +"\",\r\n" + 
							"      \"nombres\": \""+ axmldocumentos[i].dato4 +"\",\r\n" + 
							"      \"apellidos\": \""+ axmldocumentos[i].dato5 +"\",\r\n" + 
							"      \"tipoIdentificacion\": \""+ axmldocumentos[i].dato6 +"\",\r\n" + 
							"      \"identificacion\": \""+ axmldocumentos[i].dato7 +"\",\r\n" + 
							"      \"correoPersonal\": \""+ axmldocumentos[i].dato8 +"\",\r\n" + 
							"      \"correoEmpresarial\": \""+ axmldocumentos[i].dato9 +"\",\r\n" + 
							"      \"contrato\": {\r\n" + 
							"        \"tipoContrato\": \""+ axmldocumentos[i].dato10 +"\",\r\n" + 
							"        \"nombreContrato\": \""+ axmldocumentos[i].dato11 +"\",\r\n" + 
							"        \"nroContrato\": \""+ axmldocumentos[i].dato12 +"\",\r\n" + 
							"        \"valorContrato\": \""+ 0 +"\",\r\n" + 
							"        \"fechaInicio\": \""+ axmldocumentos[i].dato14 +"\",\r\n" + 
							"        \"fechaFin\": \""+ axmldocumentos[i].dato15 +"\",\r\n" + 
							"        \"idCargo\": \""+ axmldocumentos[i].dato16 +"\",\r\n" + 							
							"        \"cargo\": \""+ axmldocumentos[i].dato17 +"\",\r\n" + 
							"        \"fechaPutCola\": \""+ axmldocumentos[i].dato18 +"\"\r\n" +
							"      }\r\n" + 
							"    }\r\n" + 
							"  ]\r\n" + 
							"}";

					envio = enviarJson(msj);
					
					if (envio == true) {
						NOEnvioMensajesAzureData.methodActualizaEstadoEnvio(conn, axmldocumentos[i].dato19);
					}
				}
			}
			
		} catch (Exception ex) {
			Log.error(ex.getMessage(), ex);
		} finally {
			// TODO: handle finally clause
			OBDal.getInstance().rollbackAndClose();
		}
	}

	public boolean enviarJson(String msj) {
		boolean resp = false;
		try {
			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(connectionString);

			// Create the queue client.
			CloudQueueClient queueClient = storageAccount.createCloudQueueClient();

			// Retrieve a reference to a queue.
			CloudQueue queue = queueClient.getQueueReference("atrumqueue");
			//CloudQueue queue = queueClient.getQueueReference("colatest1");

			// Create the queue if it doesn't already exist.
			// queue.createIfNotExists();

			CloudQueueMessage message = new CloudQueueMessage(msj);
			queue.addMessage(message);

			resp = true;

		} catch (Exception ex) {
			Log.error("Error, no se puede conectar a la cola: " + ex.getMessage(), ex);
			resp = false;
		}

		return resp;
	}

}
