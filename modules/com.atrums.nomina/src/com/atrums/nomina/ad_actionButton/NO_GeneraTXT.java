package com.atrums.nomina.ad_actionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

public class NO_GeneraTXT extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String strPafoCabeceraId = vars.getStringParameter("inpnoPagoCabeceraId");

    if (!strPafoCabeceraId.equals("") && strPafoCabeceraId != null
        && !strPafoCabeceraId.equals("null")) {
      imprTXT(response, strPafoCabeceraId);
    } else {
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println("");
      out.close();
    }
  }

  protected void imprTXT(HttpServletResponse response, String strPafoCabeceraId)
      throws IOException, ServletException {

    Date date = new Date();
    SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
    String strFecha = ft.format(date);
    String strNombreArch = "Cash-" + strFecha;

    FileWriter fichero = null;
    PrintWriter pw = null;

    File flDocTXT = null;

    try {
      flDocTXT = File.createTempFile(strNombreArch, ".txt", null);
      fichero = new FileWriter(flDocTXT);
      pw = new PrintWriter(fichero);

      NOGeneraTXTData[] textData = NOGeneraTXTData.methodSeleccionarDatosTexto(this,
          strPafoCabeceraId);

      if (textData.length > 0) {
        for (int i = 0; i < textData.length; i++) {
          pw.println(textData[i].dato1);
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (null != fichero)
          fichero.close();
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }

    FileInputStream insStream = new FileInputStream(flDocTXT);

    response.setContentType("text/plain");
    response.setHeader("Content-Disposition",
        "attachment; filename=Documento_" + strNombreArch + ".txt");
    IOUtils.copy(insStream, response.getOutputStream());
    flDocTXT.exists();
    insStream.close();
    response.flushBuffer();

    /*
     * byte[] btyDoc = null;
     * 
     * if (ateData.length > 0) { btyDoc = ateData[0].dato1.getBytes(); }
     * 
     * File flDocXML = null;
     * 
     * if (btyDoc != null) {
     * 
     * flDocXML = opeaux.bytetofile(btyDoc); if (flDocXML != null) { FileInputStream insStream = new
     * FileInputStream(flDocXML);
     * 
     * response.setContentType("application/xml"); response.setHeader("Content-Disposition",
     * "attachment; filename=Documento_" + ateData[0].dato2 + ".xml"); IOUtils.copy(insStream,
     * response.getOutputStream()); flDocXML.exists(); insStream.close(); response.flushBuffer(); }
     * }
     */
  }

}
