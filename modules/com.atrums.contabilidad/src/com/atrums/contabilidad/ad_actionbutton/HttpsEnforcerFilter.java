package com.atrums.contabilidad.ad_actionbutton;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpsEnforcerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // No es necesario implementar esta función
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        // Si la petición no es HTTPS, redirigir a HTTPS
        if (!req.isSecure()) {
            String url = "https://" + req.getServerName() + req.getRequestURI();
            res.sendRedirect(url);
        } else {
            // Continuar con la cadena de filtros
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // No es necesario implementar esta función
    }
}
