<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_Factura" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="b5a72514-937c-4dc2-ae06-04eef4324507">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="10"/>
	<parameter name="DOCUMENT_ID" class="java.lang.String"/>
	<parameter name="BASE_DESIGN" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:/_Desarrollo/OpenBravo/munditransport/OpenBravoERP/modules/com.atrums.felectronica/src"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT cl.name as empresa, i.ad_client_id,  coalesce(oi.taxid,'') AS cedruc,
       coalesce((CASE WHEN (dt.em_co_tp_comp_autorizador_sri = '1') THEN 'FACTURA'
            WHEN (dt.em_co_tp_comp_autorizador_sri = '4') THEN 'NOTA DE CREDITO'
            WHEN (dt.em_co_tp_comp_autorizador_sri = '5') THEN 'NOTA DE DEBITO'
       END),'') AS tipoDoc,
       'No. ' || lpad(coalesce(i.em_co_nro_estab,''),3,'0') || '-' ||
       lpad(coalesce(i.em_co_punto_emision,''),3,'0') || '-' ||
       lpad(coalesce(i.documentno,''),9,'0') AS numero,
       coalesce(i.em_co_nro_aut_sri,'Pendiente por el SRI...') as autoriza,
       coalesce(to_char(i.em_atecfe_fecha_autori::timestamp, 'DD/MM/YYYY HH24:MI:SS'),'Pendiente por el SRI...') as fecha,
       coalesce((CASE WHEN (cl.em_atecfe_tipoambiente = '1') THEN 'PRUEBA'
            WHEN (cl.em_atecfe_tipoambiente = '2') THEN 'PRODUCCION'
       END),'') AS ambiente,
       coalesce((CASE WHEN (cl.em_atecfe_tipoemisi = '1') THEN 'NORMAL'
            WHEN (cl.em_atecfe_tipoemisi = '2') THEN 'INDISPONIBILIDAD DEL SRI'
       END),'') AS emision,
       coalesce(i.em_atecfe_codigo_acc,'Pendiente por el SRI...') AS acceso,
       upper(o.name) AS razonSocial,
       coalesce(l.address1,coalesce(l.address2,'')) AS direccion,
       coalesce(cl.em_atecfe_numresolsri,'') AS contriEspe,
       coalesce((CASE WHEN (cl.em_atecfe_obligcontabi = 'Y') THEN 'SI'
            WHEN (cl.em_atecfe_obligcontabi = 'N') THEN 'NO'
       END),'') AS obligCont,
       coalesce(bp.name2,'') AS clieRazon,
       coalesce(bp.taxid,'') AS cliciruc,
       upper(bp.name) AS ncomercial,
       (select name from c_bpartner_location where c_bpartner_location_id=i.c_bpartner_location_id) as direccioncl,
       (select city from c_location where c_location_id=(select c_location_id from c_bpartner_location where c_bpartner_location_id=i.c_bpartner_location_id)) as ciudad,
       (select phone from c_bpartner_location where c_bpartner_location_id=i.c_bpartner_location_id) as telefono,
       (select name from ad_user where ad_user_id=i.salesrep_id) as vendedor,
        to_char(i.dateinvoiced,'dd/MM/YYYY') AS fechaFac,
	   --
		coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 12),0,00) AS subtotal_imp,
		coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 14),0,00) AS subtotal_imp14,
		--
       coalesce((SELECT sum(ilt.taxbaseamt)
       FROM c_invoicelinetax AS ilt
       INNER JOIN c_tax AS t ON (ilt.c_tax_id = t.c_tax_id)
       WHERE ilt.c_invoice_id = i.c_invoice_id AND t.em_atecfe_tartax = '0' AND ilt.taxamt=0), 0.00) AS subtotalSin_imp,
       coalesce((SELECT sum(ilt.taxbaseamt)
       FROM c_invoicelinetax AS ilt
       INNER JOIN c_tax AS t ON (ilt.c_tax_id = t.c_tax_id)
       WHERE ilt.c_invoice_id = i.c_invoice_id AND t.em_atecfe_tartax = '7' AND ilt.taxamt=0), 0.00) AS noSujeto,
       coalesce(i.totallines, 0) AS subtotal,
       coalesce(i.grandtotal, 0) AS total,
		(coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 12),0,00) * 12/100) AS iva12,
		(coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 14),0,00) * 14/100) AS iva14,
       0.00 AS descuento,
       0.00 AS ice,
       0.00 AS propina,
       coalesce((SELECT sum(ilt.taxamt)
       FROM c_invoicelinetax AS ilt
       WHERE ilt.c_invoice_id = i.c_invoice_id AND ilt.taxamt<>0),0.00) AS subtotaliva,
       coalesce(cs.direc_serv_consul,'') as consul,
       coalesce((SELECT array_to_string(array_agg(u.email), ';') FROM ad_user as u WHERE u.c_bpartner_id = i.c_bpartner_id AND u.em_atecfe_check_email = 'Y'),'') as emailcli,
       coalesce(bl.phone, coalesce(bl.phone2,'')) as telef,
       coalesce(i.description,'') as descrio,
       (select  coalesce(c.address1,coalesce(c.address2,''))
          from ad_orginfo ad, ad_org og, c_location c
         where ad.ad_org_id = og.ad_org_id
           and og.issummary = 'Y'
           and og.ad_orgtype_id = '1'
           and c.c_location_id = ad.c_location_id
           and og.ad_client_id = i.ad_client_id
         limit 1) as dir_matriz,
      coalesce((select im.documentno
         from m_inout im, m_inoutline m
        where im.m_inout_id = m.m_inout_id
          and m.m_inoutline_id = (select il.m_inoutline_id
                                    from c_invoiceline il
                                   where il.m_inoutline_id is not null
                                     and il.c_invoice_id = i.c_invoice_id
                                   limit 1)),' ')  as  guia,
        i.ad_org_id as organizationid,
(select split_part(cli.value,'*',2)
          from ad_client cli
         where cli.ad_client_id = i.ad_client_id) as var,
(select split_part(cli.value,'*',3)
          from ad_client cli
         where cli.ad_client_id = i.ad_client_id) as var1,
coalesce((case when i.ad_client_id = '3648B85F8B5E44279C11F8BC34536BF8' then
       					     (case when (select split_part(org.value,'*',4)
       					            from ad_org org
       					           where org.ad_org_id = i.ad_org_id) = '3' then
       					                 'CONTRIBUYENTE RÉGIMEN RIMPE'
       					            else
       					                 ''
       					            end)
       					else
       					    (case when (select split_part(cli.value,'*',4)
       					       from ad_client cli
       					      where cli.ad_client_id = i.ad_client_id) = '3' then
       					            'CONTRIBUYENTE RÉGIMEN RIMPE'
       					       else
       					            ''
       					       end)
       					end),'') as var4
FROM c_invoice i
   INNER JOIN ad_org o ON (i.ad_org_id = o.ad_org_id)
   INNER JOIN ad_orginfo oi ON (i.ad_org_id = oi.ad_org_id)
   INNER JOIN ad_client cl ON(i.ad_client_id = cl.ad_client_id)
   INNER JOIN c_location l ON (oi.c_location_id = l.c_location_id)
   INNER JOIN c_country c ON (l.c_country_id = c.c_country_id)
   INNER JOIN c_bpartner bp ON (i.c_bpartner_id = bp.c_bpartner_id)
   INNER JOIN c_doctype dt ON (i.c_doctypetarget_id = dt.c_doctype_id)
   LEFT JOIN atecfe_conf_servidor cs on (i.ad_client_id = cs.ad_client_id)
   LEFT JOIN c_bpartner_location bl on (bp.c_bpartner_id = bl.c_bpartner_id and bl.c_bpartner_location_id = i.c_bpartner_location_id)
WHERE i.c_invoice_id = $P{DOCUMENT_ID}]]>
	</queryString>
	<field name="empresa" class="java.lang.String"/>
	<field name="ad_client_id" class="java.lang.String"/>
	<field name="cedruc" class="java.lang.String"/>
	<field name="tipodoc" class="java.lang.String"/>
	<field name="numero" class="java.lang.String"/>
	<field name="autoriza" class="java.lang.String"/>
	<field name="fecha" class="java.lang.String"/>
	<field name="ambiente" class="java.lang.String"/>
	<field name="emision" class="java.lang.String"/>
	<field name="acceso" class="java.lang.String"/>
	<field name="razonsocial" class="java.lang.String"/>
	<field name="direccion" class="java.lang.String"/>
	<field name="contriespe" class="java.lang.String"/>
	<field name="obligcont" class="java.lang.String"/>
	<field name="clierazon" class="java.lang.String"/>
	<field name="cliciruc" class="java.lang.String"/>
	<field name="ncomercial" class="java.lang.String"/>
	<field name="direccioncl" class="java.lang.String"/>
	<field name="ciudad" class="java.lang.String"/>
	<field name="telefono" class="java.lang.String"/>
	<field name="vendedor" class="java.lang.String"/>
	<field name="fechafac" class="java.lang.String"/>
	<field name="subtotal_imp" class="java.math.BigDecimal"/>
	<field name="subtotal_imp14" class="java.math.BigDecimal"/>
	<field name="subtotalsin_imp" class="java.math.BigDecimal"/>
	<field name="nosujeto" class="java.math.BigDecimal"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="iva12" class="java.math.BigDecimal"/>
	<field name="iva14" class="java.math.BigDecimal"/>
	<field name="descuento" class="java.math.BigDecimal"/>
	<field name="ice" class="java.math.BigDecimal"/>
	<field name="propina" class="java.math.BigDecimal"/>
	<field name="subtotaliva" class="java.math.BigDecimal"/>
	<field name="consul" class="java.lang.String"/>
	<field name="emailcli" class="java.lang.String"/>
	<field name="telef" class="java.lang.String"/>
	<field name="descrio" class="java.lang.String"/>
	<field name="dir_matriz" class="java.lang.String"/>
	<field name="guia" class="java.lang.String"/>
	<field name="organizationid" class="java.lang.String"/>
	<field name="var" class="java.lang.String"/>
	<field name="var1" class="java.lang.String"/>
	<field name="var4" class="java.lang.String"/>
	<title>
		<band height="214">
			<rectangle radius="10">
				<reportElement x="1" y="66" width="276" height="142" uuid="8ed44ba1-85f5-4fb2-af11-bb6d25170de4"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="10">
				<reportElement x="287" y="-1" width="268" height="209" uuid="256297b1-4cf0-4ce5-9d3b-10805d630de6"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement x="297" y="3" width="35" height="16" uuid="da74364d-cdca-4918-863b-5dba1ecf62dd"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[R.U.C.:]]></text>
			</staticText>
			<textField>
				<reportElement x="332" y="3" width="213" height="16" uuid="56644f54-01b0-4c48-b5d6-bdcaa4478a65"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cedruc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="297" y="18" width="103" height="16" uuid="a41a481e-d81f-4dda-8a46-89223524e209"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tipodoc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="401" y="18" width="144" height="16" uuid="cb973d39-645d-487c-8d90-d88f6d0b9a64"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{numero}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="297" y="33" width="248" height="16" uuid="9a3e1634-7be6-4827-966e-fd16712988e2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[NÚMERO DE AUTORIZACIÓN]]></text>
			</staticText>
			<textField>
				<reportElement x="297" y="48" width="248" height="16" uuid="4de7d321-367c-46f4-8597-78288ed7c11f"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{autoriza}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="297" y="63" width="103" height="22" uuid="690cf114-1be9-4383-96e0-f2f673d7addb"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[FECHA Y HORA DE AUTORIZACIÓN]]></text>
			</staticText>
			<textField>
				<reportElement x="401" y="63" width="144" height="22" uuid="40bd5767-91d4-4c7d-9900-2b54b88a004d"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fecha}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="297" y="84" width="103" height="20" uuid="a0e14d6a-f634-4f90-af2e-6205b0cb816f"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[AMBIENTE:]]></text>
			</staticText>
			<textField>
				<reportElement x="401" y="84" width="144" height="20" uuid="51c301a8-2af1-48bb-8645-9e8729fe8306"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ambiente}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="297" y="103" width="103" height="20" uuid="e9b5f9c3-c44c-4c41-a525-f28f4889b39c"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[EMISIÓN:]]></text>
			</staticText>
			<textField>
				<reportElement x="401" y="103" width="144" height="20" uuid="17c7ee4d-b2ad-491f-9e52-159e66ad4ac6"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{emision}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="297" y="133" width="248" height="12" uuid="1e1829c8-6fa3-47b3-b10d-126a9840944c"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[CLAVE DE ACCESO]]></text>
			</staticText>
			<componentElement>
				<reportElement x="297" y="148" width="248" height="38" uuid="e2c5026a-8921-4f14-951f-5c56706cef26"/>
				<jr:barbecue xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" type="Code128" drawText="false" checksumRequired="false" barWidth="0" barHeight="0">
					<jr:codeExpression><![CDATA[$F{acceso}]]></jr:codeExpression>
				</jr:barbecue>
			</componentElement>
			<textField>
				<reportElement x="297" y="190" width="248" height="20" uuid="3ddf0ad7-4d8e-4332-a878-3f638b78b55d"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{acceso}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="5" y="69" width="268" height="16" uuid="07de3d80-2b24-4047-a45d-41be1b49ea3c"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{empresa}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="174" y="144" width="99" height="15" uuid="5ca19192-82e7-4725-8cad-698983cfd0e7"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{obligcont}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="65" y="108" width="208" height="20" uuid="1be7e8dd-0e87-427a-b8ed-bf2cf5d290fa"/>
				<textElement verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{direccion}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="87" width="60" height="20" uuid="c5b6c4ec-ac7f-4519-a838-17bbec615811"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dir. Matriz:]]></text>
			</staticText>
			<staticText>
				<reportElement x="5" y="108" width="60" height="20" uuid="c00068a3-11b7-4160-91d0-d07c5aa4de77"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dir. Sucursal:]]></text>
			</staticText>
			<textField>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="65" y="87" width="208" height="20" uuid="82bb37b1-837c-4565-95ac-ebdf99810969"/>
				<textElement verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{direccion}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="144" width="169" height="15" uuid="8ec36da1-3e52-41c9-b4b8-8f3c327d9d17"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[OBLIGADO A LLEVAR CONTABILIDAD]]></text>
			</staticText>
			<staticText>
				<reportElement x="5" y="174" width="268" height="15" uuid="2ca77c2f-a3ca-481c-ada1-acaf8f9ea88a">
					<printWhenExpression><![CDATA[$F{var1}.equals("2")]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="false"/>
				</textElement>
				<text><![CDATA[Agente de Retención  Resolución Nro. NAC-DNCRASC20-00000001]]></text>
			</staticText>
			<staticText>
				<reportElement x="5" y="159" width="268" height="15" uuid="e5777348-7c36-4bb4-9f04-ece167defdd4">
					<printWhenExpression><![CDATA[$F{var}.equals("1")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="false"/>
				</textElement>
				<text><![CDATA[CONTRIBUYENTE REGIMEN MICROEMPRESAS]]></text>
			</staticText>
			<textField>
				<reportElement x="5" y="190" width="268" height="15" uuid="bba53989-3e3a-45d0-9622-8386045f9e8f">
					<printWhenExpression><![CDATA[!$F{var4}.equals("") && !$F{var4}.equals(null)]]></printWhenExpression>
				</reportElement>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{var4}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="129" width="169" height="15" uuid="a06e98bb-2a76-45f6-b3ec-8032c9861cbc">
					<printWhenExpression><![CDATA[!$F{contriespe}.equals("")&&!$F{contriespe}.equals(null)]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[CONTRIBUYENTE ESPECIAL No.]]></text>
			</staticText>
			<textField>
				<reportElement x="174" y="129" width="99" height="15" uuid="68d9089e-b955-4eba-b6f5-c2028687d43f">
					<printWhenExpression><![CDATA[!$F{contriespe}.equals("")&&!$F{contriespe}.equals(null)]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{contriespe}]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Center" vAlign="Top" isUsingCache="true">
				<reportElement key="image-1" x="1" y="2" width="173" height="47" uuid="8d5b9cd7-b963-4a0e-b3ba-9abb6896dcfb"/>
				<imageExpression><![CDATA[org.openbravo.erpCommon.utility.Utility.showImageLogo("yourcompanylegal", $F{organizationid})]]></imageExpression>
			</image>
			<staticText>
				<reportElement x="5" y="190" width="268" height="15" uuid="1db023ce-544a-498e-acf3-6e3313616623">
					<printWhenExpression><![CDATA[$F{ad_client_id}.equals("F3A1DD78970A49029C55D19F3728A8E4")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[CONTRIBUYENTE REGIMEN GENERAL]]></text>
			</staticText>
		</band>
	</title>
	<columnHeader>
		<band height="76">
			<rectangle>
				<reportElement x="0" y="1" width="555" height="75" uuid="9db47b6c-9115-4e2d-a9ec-2f599126bd61"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement x="441" y="4" width="101" height="11" uuid="c529755e-63bd-442b-935b-ae953731e93a"/>
				<textElement verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cliciruc}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="29" width="95" height="11" uuid="dcca6c86-4ff0-4c13-8453-7327ea401a1c"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Emisión:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="110" y="29" width="136" height="11" uuid="b6f930d4-0e9f-4976-bb64-5063188aa8cf"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fechafac}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="376" y="4" width="65" height="11" uuid="b29127c6-f026-4038-bcce-856efd4a0154"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[RUC/Cl:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="110" y="4" width="262" height="11" uuid="1ff8fa56-c409-4a38-b831-99b1ffd0d7e8"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{clierazon}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="4" width="95" height="11" uuid="47cacf92-514b-42da-b227-e97df78cf05e"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Razón Social:]]></text>
			</staticText>
			<staticText>
				<reportElement x="376" y="16" width="65" height="11" uuid="478d12b0-8e2b-4454-bc36-ae9818233153"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Teléfono:]]></text>
			</staticText>
			<staticText>
				<reportElement x="15" y="16" width="95" height="11" uuid="f87b7859-ad77-4a1d-805a-243ad926d8e0"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Nombre Comercial:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="110" y="16" width="262" height="11" uuid="dee36198-b4d8-4c1c-9dc2-ce8a53187896"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ncomercial}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="41" width="95" height="11" uuid="50aa2f37-8b9b-4e5e-969a-994a7ba5d47b"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dirección:]]></text>
			</staticText>
			<staticText>
				<reportElement x="376" y="41" width="65" height="11" uuid="42c15133-94ed-4ddb-835e-776ecbd69166"/>
				<textElement verticalAlignment="Top">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Ciudad:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="111" y="41" width="262" height="11" uuid="a1937f8d-8fed-4027-9b06-824e7b0bdaf4"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{direccioncl}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="441" y="41" width="100" height="11" uuid="f9647319-7283-46b9-9f25-ca3d4141dad3"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ciudad}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="441" y="16" width="100" height="11" uuid="19606ce9-7dcb-4331-986d-5f7e942f158d"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{telefono}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="23">
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="555" height="23" uuid="10f5c233-567c-4564-8266-9dc29eaec8c2">
					<printWhenExpression><![CDATA[$F{ad_client_id}=="CF9DE066854D4BE3B9B44EE9DA921059"]]></printWhenExpression>
				</reportElement>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{BASE_DESIGN} + "/com/atrums/felectronica/erpReport/Rpt_Factura_lineas.jasper"]]></subreportExpression>
			</subreport>
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="555" height="23" uuid="2ce0fafe-43a0-4fb1-8a52-cc2bc324d83a">
					<printWhenExpression><![CDATA[$F{ad_client_id}!="CF9DE066854D4BE3B9B44EE9DA921059"]]></printWhenExpression>
				</reportElement>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{BASE_DESIGN} + "/com/atrums/felectronica/erpReport/Rpt_Factura_lineasLogic.jasper"]]></subreportExpression>
			</subreport>
		</band>
		<band height="18">
			<subreport>
				<reportElement x="0" y="0" width="555" height="18" uuid="209565fd-acab-432e-85a0-b277d47d0a1f"/>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{BASE_DESIGN} + "/com/atrums/felectronica/erpReport/Rpt_Factura_Iva12.jasper"]]></subreportExpression>
			</subreport>
		</band>
		<band height="24">
			<subreport>
				<reportElement x="1" y="10" width="555" height="13" uuid="faf1d5fc-be4f-4b36-9650-f92bb08b93fa"/>
				<subreportParameter name="USER_CLIENT"/>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="BASE_DESIGN">
					<subreportParameterExpression><![CDATA[$P{BASE_DESIGN}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{BASE_DESIGN} + "/com/atrums/felectronica/erpReport/Reembolso_Ventas.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
