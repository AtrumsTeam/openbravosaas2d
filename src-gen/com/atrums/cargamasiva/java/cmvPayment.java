/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.cargamasiva.java;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity cmv_payment (stored in table cmv_payment).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class cmvPayment extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "cmv_payment";
    public static final String ENTITY_NAME = "cmv_payment";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_DOCUMENTNO = "documentNo";
    public static final String PROPERTY_REFERENCENO = "referenceNo";
    public static final String PROPERTY_DESCRIPTION = "description";
    public static final String PROPERTY_ACCOUNTINGDATE = "accountingDate";
    public static final String PROPERTY_AMOUNT = "amount";
    public static final String PROPERTY_DEPOSITDATE = "depositDate";
    public static final String PROPERTY_NUMESTAB = "nUMEstab";
    public static final String PROPERTY_PTOEMISION = "pTOEmision";
    public static final String PROPERTY_CBPARTNERNAME = "cbpartnerName";
    public static final String PROPERTY_FINACCOUNTNAME = "fINAccountName";
    public static final String PROPERTY_GLITEMNAME = "glitemName";
    public static final String PROPERTY_DOCTYPENAME = "doctypeName";
    public static final String PROPERTY_FINMETHODNAME = "fINMethodName";
    public static final String PROPERTY_LOGMESSAGE = "logmessage";
    public static final String PROPERTY_PROCESAR = "procesar";

    public cmvPayment() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESAR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getDocumentNo() {
        return (String) get(PROPERTY_DOCUMENTNO);
    }

    public void setDocumentNo(String documentNo) {
        set(PROPERTY_DOCUMENTNO, documentNo);
    }

    public String getReferenceNo() {
        return (String) get(PROPERTY_REFERENCENO);
    }

    public void setReferenceNo(String referenceNo) {
        set(PROPERTY_REFERENCENO, referenceNo);
    }

    public String getDescription() {
        return (String) get(PROPERTY_DESCRIPTION);
    }

    public void setDescription(String description) {
        set(PROPERTY_DESCRIPTION, description);
    }

    public Date getAccountingDate() {
        return (Date) get(PROPERTY_ACCOUNTINGDATE);
    }

    public void setAccountingDate(Date accountingDate) {
        set(PROPERTY_ACCOUNTINGDATE, accountingDate);
    }

    public BigDecimal getAmount() {
        return (BigDecimal) get(PROPERTY_AMOUNT);
    }

    public void setAmount(BigDecimal amount) {
        set(PROPERTY_AMOUNT, amount);
    }

    public Date getDepositDate() {
        return (Date) get(PROPERTY_DEPOSITDATE);
    }

    public void setDepositDate(Date depositDate) {
        set(PROPERTY_DEPOSITDATE, depositDate);
    }

    public String getNUMEstab() {
        return (String) get(PROPERTY_NUMESTAB);
    }

    public void setNUMEstab(String nUMEstab) {
        set(PROPERTY_NUMESTAB, nUMEstab);
    }

    public String getPTOEmision() {
        return (String) get(PROPERTY_PTOEMISION);
    }

    public void setPTOEmision(String pTOEmision) {
        set(PROPERTY_PTOEMISION, pTOEmision);
    }

    public String getCbpartnerName() {
        return (String) get(PROPERTY_CBPARTNERNAME);
    }

    public void setCbpartnerName(String cbpartnerName) {
        set(PROPERTY_CBPARTNERNAME, cbpartnerName);
    }

    public String getFINAccountName() {
        return (String) get(PROPERTY_FINACCOUNTNAME);
    }

    public void setFINAccountName(String fINAccountName) {
        set(PROPERTY_FINACCOUNTNAME, fINAccountName);
    }

    public String getGlitemName() {
        return (String) get(PROPERTY_GLITEMNAME);
    }

    public void setGlitemName(String glitemName) {
        set(PROPERTY_GLITEMNAME, glitemName);
    }

    public String getDoctypeName() {
        return (String) get(PROPERTY_DOCTYPENAME);
    }

    public void setDoctypeName(String doctypeName) {
        set(PROPERTY_DOCTYPENAME, doctypeName);
    }

    public String getFINMethodName() {
        return (String) get(PROPERTY_FINMETHODNAME);
    }

    public void setFINMethodName(String fINMethodName) {
        set(PROPERTY_FINMETHODNAME, fINMethodName);
    }

    public String getLogmessage() {
        return (String) get(PROPERTY_LOGMESSAGE);
    }

    public void setLogmessage(String logmessage) {
        set(PROPERTY_LOGMESSAGE, logmessage);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

}
