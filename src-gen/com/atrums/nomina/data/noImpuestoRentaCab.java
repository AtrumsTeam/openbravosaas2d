/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.nomina.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.calendar.Year;
/**
 * Entity class for entity no_impuesto_renta_cab (stored in table no_impuesto_renta_cab).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class noImpuestoRentaCab extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "no_impuesto_renta_cab";
    public static final String ENTITY_NAME = "no_impuesto_renta_cab";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NOMBRE = "nombre";
    public static final String PROPERTY_FRACCIONBASICA = "fraccionBasica";
    public static final String PROPERTY_FACTORLIMREBAJA = "factorLimRebaja";
    public static final String PROPERTY_LIMREBAJA = "limRebaja";
    public static final String PROPERTY_PORCLIMREBMENOR = "porcLimRebMenor";
    public static final String PROPERTY_PORCLIMREBMAYOR = "porcLimRebMayor";
    public static final String PROPERTY_CANASTABASICA = "canastaBasica";
    public static final String PROPERTY_FACTORREBAJAGP = "factorRebajaGp";
    public static final String PROPERTY_REBAJAMAXIMA = "rebajaMaxima";
    public static final String PROPERTY_REBAJAMAXIMA10POR = "rebajaMaxima10por";
    public static final String PROPERTY_REBAJAMAXIMA20POR = "rebajaMaxima20por";
    public static final String PROPERTY_NOCONFIGGDEDUCIBLELIST = "noConfigGdeducibleList";
    public static final String PROPERTY_NOIMPUESTORENTALIST = "noImpuestoRentaList";

    public noImpuestoRentaCab() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_FRACCIONBASICA, new BigDecimal(0));
        setDefaultValue(PROPERTY_FACTORLIMREBAJA, new BigDecimal(0));
        setDefaultValue(PROPERTY_LIMREBAJA, new BigDecimal(0));
        setDefaultValue(PROPERTY_PORCLIMREBMENOR, (long) 0);
        setDefaultValue(PROPERTY_PORCLIMREBMAYOR, (long) 0);
        setDefaultValue(PROPERTY_CANASTABASICA, new BigDecimal(0));
        setDefaultValue(PROPERTY_FACTORREBAJAGP, new BigDecimal(0));
        setDefaultValue(PROPERTY_REBAJAMAXIMA, new BigDecimal(0));
        setDefaultValue(PROPERTY_REBAJAMAXIMA10POR, new BigDecimal(0));
        setDefaultValue(PROPERTY_REBAJAMAXIMA20POR, new BigDecimal(0));
        setDefaultValue(PROPERTY_NOCONFIGGDEDUCIBLELIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_NOIMPUESTORENTALIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Year getNombre() {
        return (Year) get(PROPERTY_NOMBRE);
    }

    public void setNombre(Year nombre) {
        set(PROPERTY_NOMBRE, nombre);
    }

    public BigDecimal getFraccionBasica() {
        return (BigDecimal) get(PROPERTY_FRACCIONBASICA);
    }

    public void setFraccionBasica(BigDecimal fraccionBasica) {
        set(PROPERTY_FRACCIONBASICA, fraccionBasica);
    }

    public BigDecimal getFactorLimRebaja() {
        return (BigDecimal) get(PROPERTY_FACTORLIMREBAJA);
    }

    public void setFactorLimRebaja(BigDecimal factorLimRebaja) {
        set(PROPERTY_FACTORLIMREBAJA, factorLimRebaja);
    }

    public BigDecimal getLimRebaja() {
        return (BigDecimal) get(PROPERTY_LIMREBAJA);
    }

    public void setLimRebaja(BigDecimal limRebaja) {
        set(PROPERTY_LIMREBAJA, limRebaja);
    }

    public Long getPorcLimRebMenor() {
        return (Long) get(PROPERTY_PORCLIMREBMENOR);
    }

    public void setPorcLimRebMenor(Long porcLimRebMenor) {
        set(PROPERTY_PORCLIMREBMENOR, porcLimRebMenor);
    }

    public Long getPorcLimRebMayor() {
        return (Long) get(PROPERTY_PORCLIMREBMAYOR);
    }

    public void setPorcLimRebMayor(Long porcLimRebMayor) {
        set(PROPERTY_PORCLIMREBMAYOR, porcLimRebMayor);
    }

    public BigDecimal getCanastaBasica() {
        return (BigDecimal) get(PROPERTY_CANASTABASICA);
    }

    public void setCanastaBasica(BigDecimal canastaBasica) {
        set(PROPERTY_CANASTABASICA, canastaBasica);
    }

    public BigDecimal getFactorRebajaGp() {
        return (BigDecimal) get(PROPERTY_FACTORREBAJAGP);
    }

    public void setFactorRebajaGp(BigDecimal factorRebajaGp) {
        set(PROPERTY_FACTORREBAJAGP, factorRebajaGp);
    }

    public BigDecimal getRebajaMaxima() {
        return (BigDecimal) get(PROPERTY_REBAJAMAXIMA);
    }

    public void setRebajaMaxima(BigDecimal rebajaMaxima) {
        set(PROPERTY_REBAJAMAXIMA, rebajaMaxima);
    }

    public BigDecimal getRebajaMaxima10por() {
        return (BigDecimal) get(PROPERTY_REBAJAMAXIMA10POR);
    }

    public void setRebajaMaxima10por(BigDecimal rebajaMaxima10por) {
        set(PROPERTY_REBAJAMAXIMA10POR, rebajaMaxima10por);
    }

    public BigDecimal getRebajaMaxima20por() {
        return (BigDecimal) get(PROPERTY_REBAJAMAXIMA20POR);
    }

    public void setRebajaMaxima20por(BigDecimal rebajaMaxima20por) {
        set(PROPERTY_REBAJAMAXIMA20POR, rebajaMaxima20por);
    }

    @SuppressWarnings("unchecked")
    public List<noConfigGdeducible> getNoConfigGdeducibleList() {
      return (List<noConfigGdeducible>) get(PROPERTY_NOCONFIGGDEDUCIBLELIST);
    }

    public void setNoConfigGdeducibleList(List<noConfigGdeducible> noConfigGdeducibleList) {
        set(PROPERTY_NOCONFIGGDEDUCIBLELIST, noConfigGdeducibleList);
    }

    @SuppressWarnings("unchecked")
    public List<noImpuestoRenta> getNoImpuestoRentaList() {
      return (List<noImpuestoRenta>) get(PROPERTY_NOIMPUESTORENTALIST);
    }

    public void setNoImpuestoRentaList(List<noImpuestoRenta> noImpuestoRentaList) {
        set(PROPERTY_NOIMPUESTORENTALIST, noImpuestoRentaList);
    }

}
